<?php

namespace App\Models;

class Casos extends BaseModel
{

    


    //Metodo para obtener todos los casos 
    public function obtenerCasos()
    {
        $db      = \Config\Database::connect();
        $strQuery = "SELECT distinct a.idcaso, a.tipo_beneficiario,a.id_tipo_atencion,a.tipo_atend_id,a.casotel,TRIM(a.casoced) AS casoced,a.casonom,a.casoape,a.casodesc";
        $strQuery .= ",a.edad,to_char(a.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimiento,a.fecha_nacimiento as fecha_nacimiento_normal , a.casofec as casofec_normal,a.caso_nacionalidad,a.idrrss,a.ofiid,a.estadoid,a.id_tipo_atencion";
        $strQuery .= ",a.municipioid,a.parroquiaid,a.direccion,a.correo,a.ente_adscrito_id";
        $strQuery .= ",CONCAT(a.caso_nacionalidad,a.casoced) AS cedula";
        $strQuery .= ",t_bene.tipo_beneficiario_nombre as tipo_beneficiario_nombre";
        $strQuery .= ",cgr.competencia_cgr,cgr.asume_cgr";
        $strQuery .= ",denu.denu_afecta_persona,denu.denu_afecta_comunidad,denu.denu_afecta_terceros";
        $strQuery .= ",denu.denu_involucrados,denu.denu_fecha_hechos,denu.denu_instancia_popular";
        $strQuery .= ",denu.denu_rif_instancia,denu.denu_ente_financiador,denu.denu_nombre_proyecto,denu.denu_monto_aprovado";
        $strQuery .= ",CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
        $strQuery .= ",CONCAT(u_ope.usuopnom, ' ',' ', u_ope.usuopape) AS user_name";
        $strQuery .= ",case when sexo='1'then 'M' else 'F' end as sexo ";
        $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
        $strQuery .= ",t_antusu.tipo_aten_nombre ";
        $strQuery .= "FROM sgc_casos a ";
        $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
        $strQuery .= " join sgc_tipo_beneficiarios as t_bene on a.tipo_beneficiario=t_bene.tipo_beneficiario_id";
        $strQuery .= " join sgc_usuario_operador u_ope on a.idusuopr = u_ope.idusuopr  ";
        $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id  ";
        $strQuery .= " left join sgc_registro_cgr cgr on a.idcaso=cgr.id_caso  ";
        $strQuery .= " left join sgc_casos_denuncias denu on a.idcaso=denu_id_caso ";
        $strQuery .= " where a.borrado='false'  ";
        $strQuery .= " ORDER BY a.idcaso  desc";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }

  //Metodo para obtener todos los casos por usuario
  public function obtenerCasos_filtrados_por_usuario($idusur)
  {
      $db      = \Config\Database::connect();
      $strQuery = "SELECT distinct a.idcaso, a.tipo_beneficiario,a.id_tipo_atencion,a.tipo_atend_id,a.casotel,TRIM(a.casoced) AS casoced,a.casonom,a.casoape,a.casodesc";
      $strQuery .= ",a.edad,to_char(a.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimiento,a.fecha_nacimiento as fecha_nacimiento_normal , a.casofec as casofec_normal,a.caso_nacionalidad,a.idrrss,a.ofiid,a.estadoid,a.id_tipo_atencion";
      $strQuery .= ",a.municipioid,a.parroquiaid,a.direccion,a.correo,a.ente_adscrito_id";
      $strQuery .= ",CONCAT(a.caso_nacionalidad,a.casoced) AS cedula";
      $strQuery .= ",t_bene.tipo_beneficiario_nombre as tipo_beneficiario_nombre";
      $strQuery .= ",cgr.competencia_cgr,cgr.asume_cgr";
      $strQuery .= ",denu.denu_afecta_persona,denu.denu_afecta_comunidad,denu.denu_afecta_terceros";
      $strQuery .= ",denu.denu_involucrados,denu.denu_fecha_hechos,denu.denu_instancia_popular";
      $strQuery .= ",denu.denu_rif_instancia,denu.denu_ente_financiador,denu.denu_nombre_proyecto,denu.denu_monto_aprovado";
      $strQuery .= ",CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
      $strQuery .= ",CONCAT(u_ope.usuopnom, ' ',' ', u_ope.usuopape) AS user_name";
      $strQuery .= ",case when sexo='1'then 'M' else 'F' end as sexo ";
      $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
      $strQuery .= ",t_antusu.tipo_aten_nombre ";
      $strQuery .= "FROM sgc_casos a ";
      $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
      $strQuery .= " join sgc_tipo_beneficiarios as t_bene on a.tipo_beneficiario=t_bene.tipo_beneficiario_id";
      $strQuery .= " join sgc_usuario_operador u_ope on a.idusuopr = u_ope.idusuopr  ";
      $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id  ";
      $strQuery .= " left join sgc_registro_cgr cgr on a.idcaso=cgr.id_caso  ";
      $strQuery .= " left join sgc_casos_denuncias denu on a.idcaso=denu_id_caso ";
      $strQuery .= " where a.borrado='false'  ";
      $strQuery .= " and a.idusuopr=$idusur ";
      $strQuery .= " ORDER BY a.idcaso  desc";
     $query = $db->query($strQuery);
     $resultado = $query->getResult();
      return $resultado;
  }


//Metodo para obtener toda la informacion del caso para la web 
public function Informacion_Usuarios($casoced)
{
   
    $db      = \Config\Database::connect();
    $strQuery = "SELECT a.tipo_beneficiario,a.idcaso,a.casotel,TRIM(a.casoced) AS casoced,a.casonom,a.casoape,a.casodesc";
    $strQuery .= ",a.caso_nacionalidad,a.idrrss,a.ofiid,a.estadoid,a.id_tipo_atencion";
    $strQuery .= ",a.municipioid,a.parroquiaid,a.direccion,a.correo,a.ente_adscrito_id";
    $strQuery .= ",CONCAT(a.caso_nacionalidad,a.casoced) AS cedula";
    $strQuery .= ",cgr.competencia_cgr,cgr.asume_cgr";
    $strQuery .= ",denu.denu_afecta_persona,denu.denu_afecta_comunidad,denu.denu_afecta_terceros";
    $strQuery .= ",denu.denu_involucrados,denu.denu_fecha_hechos,denu.denu_instancia_popular";
    $strQuery .= ",denu.denu_rif_instancia,denu.denu_ente_financiador,denu.denu_nombre_proyecto,denu.denu_monto_aprovado";
    $strQuery .= ",CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
    $strQuery .= ",CONCAT(u_ope.usuopnom, ' ',' ', u_ope.usuopape) AS user_name";
    $strQuery .= ",case when sexo='1'then 'M' else 'F' end as sexo ";
    $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
    $strQuery .= ",tpinte.tipo_prop_nombre ";
    $strQuery .= ",tpinte.tipo_prop_id ";
    $strQuery .= ",t_antusu.tipo_aten_nombre ";
    $strQuery .= "FROM sgc_casos a ";
    $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
    $strQuery .= " join sgc_usuario_operador u_ope on a.idusuopr = u_ope.idusuopr  ";
    $strQuery .= " join sgc_tipo_prop_caso as tpc on a.idcaso=tpc.idcaso  ";
    $strQuery .= " join sgc_tipo_prop_intelec as tpinte on tpc.idtippropint=tpinte.tipo_prop_id  ";
    $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id  ";
    $strQuery .= " left join sgc_registro_cgr cgr on a.idcaso=cgr.id_caso  ";
    $strQuery .= " left join sgc_casos_denuncias denu on a.idcaso=denu_id_caso ";
    $strQuery .= " where a.borrado='false'  ";
    $strQuery .= " and a.casoced='$casoced' ";
    $strQuery .= " ORDER BY a.idcaso  desc";
    $query = $db->query($strQuery);
    $resultado = $query->getResult();
    return $resultado;
}



    //Metodo para obtener el caso en funsion del id 
    public function obtenerCaso_id()
    {
        $db      = \Config\Database::connect();
        $strQuery = "SELECT a.tipo_beneficiario,a.idcaso,a.casotel,TRIM(a.casoced) AS casoced,a.casonom,a.casoape,a.casodesc";
        $strQuery .= ",a.caso_nacionalidad,a.idrrss,a.ofiid,a.estadoid,a.id_tipo_atencion";
        $strQuery .= ",a.municipioid,a.parroquiaid,a.direccion,a.correo,a.ente_adscrito_id";
        $strQuery .= ",CONCAT(a.caso_nacionalidad,a.casoced) AS cedula";
        $strQuery .= ",cgr.competencia_cgr,cgr.asume_cgr";
        $strQuery .= ",denu.denu_afecta_persona,denu.denu_afecta_comunidad,denu.denu_afecta_terceros";
        $strQuery .= ",denu.denu_involucrados,denu.denu_fecha_hechos,denu.denu_instancia_popular";
        $strQuery .= ",denu.denu_rif_instancia,denu.denu_ente_financiador,denu.denu_nombre_proyecto,denu.denu_monto_aprovado";
        $strQuery .= ",CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
        $strQuery .= ",CONCAT(u_ope.usuopnom, ' ',' ', u_ope.usuopape) AS user_name";
        $strQuery .= ",case when sexo='1'then 'M' else 'F' end as sexo ";
        $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
        $strQuery .= ",tpinte.tipo_prop_nombre ";
        $strQuery .= ",tpinte.tipo_prop_id ";
        $strQuery .= ",t_antusu.tipo_aten_nombre ";
        $strQuery .= "FROM sgc_casos a ";
        $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
        $strQuery .= " join sgc_usuario_operador u_ope on a.idusuopr = u_ope.idusuopr  ";
        $strQuery .= " join sgc_tipo_prop_caso as tpc on a.idcaso=tpc.idcaso  ";
        $strQuery .= " join sgc_tipo_prop_intelec as tpinte on tpc.idtippropint=tpinte.tipo_prop_id  ";
        $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id  ";
        $strQuery .= " left join sgc_registro_cgr cgr on a.idcaso=cgr.id_caso  ";
        $strQuery .= " left join sgc_casos_denuncias denu on a.idcaso=denu_id_caso ";
        $strQuery .= " where a.borrado='false'  ";
        $strQuery .= " ORDER BY a.idcaso  desc";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }
    //Metodo para obtener EL ULTIMO ID INSERTADO
    public function obtener_utimo_id()
    {
        $builder = $this->dbconn('public.sgc_casos');
        $builder->select(
            " MAX(idcaso) as ultimo_id"
        );
        $query = $builder->get();
        return $query;
    }
    //Metodo para consultar los ultimos veinte casos por usuario
    public function obtener_ultimos_casos(String $iduser)
    {
        $db      = \Config\Database::connect();
        $strQuery = "SELECT a.idcaso,a.casotel,a.casoced,";
        $strQuery .= " CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
        $strQuery .= " ,case when sexo='1'then 'M' else 'F' end as sexo ";
        $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
        $strQuery .= ",tpinte.tipo_prop_nombre ";
        $strQuery .= ",t_antusu.tipo_aten_nombre ";
        $strQuery .= "FROM sgc_casos a ";
        $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
        $strQuery .= " join sgc_usuario_operador c on a.idusuopr = c.idusuopr  ";
        $strQuery .= " join sgc_tipo_prop_caso as tpc on a.idcaso=tpc.idcaso  ";
        $strQuery .= " join sgc_tipo_prop_intelec as tpinte on tpc.idtippropint=tpinte.tipo_prop_id  ";
        $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id  ";
        $strQuery .= "where a.idusuopr= $iduser ";
        $strQuery .= " ORDER BY a.idcaso  DESC";
        $strQuery .= " limit(20)";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }
    //Metodo para insertar un nuevo caso en la BD
    public function insertarNuevoCaso(array $datos)
    {
        $builder = $this->dbconn('sgc_casos');
        date_default_timezone_set('America/Caracas');
        $hora = date("h:i:s A"); // Utiliza "h" en lugar de "H" para obtener la hora en formato 12 horas
        $datos['caso_hora'] = $hora;
        $query = $builder->insert($datos);
        return $query;
    }
    //Metodo para   actualizar  us Caso en la BD
    public function actualizarCaso(array $datos)
    {
        $builder = $this->dbconn('sgc_casos');
        $query = $builder->update($datos, 'idcaso = ' . $datos["idcaso"]);
        return $query;
    }
    //Metodo para obtener el detalle de un solo caso
    public function detalleCaso(String $idcaso)
    {
        $db      = \Config\Database::connect();
        $strQuery = "SELECT  a.idcaso,a.casotel,TRIM(a.casoced) AS casoced,a.casonom,a.casoape,a.casodesc";
        $strQuery .= ",a.caso_nacionalidad,a.idrrss,a.ofiid,a.estadoid,a.id_tipo_atencion,a.ente_adscrito_id";
        $strQuery .= ",a.municipioid,a.parroquiaid,a.direccion,a.correo";
        $strQuery .= ",CASE WHEN direc.descripcion IS null then 'No Aplica' Else  direc.descripcion  end as unidad_administrativa ";
        $strQuery .= ",est.estadonom";
        $strQuery .= ",mun.municipionom";
        $strQuery .= ",p.parroquianom";
        $strQuery .= ",u_ope.usuopemail";
        $strQuery .= ",CONCAT(a.caso_nacionalidad,a.casoced) AS cedula";
        $strQuery .= ",CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
        $strQuery .= ",CONCAT(u_ope.usuopnom, ' ',' ', u_ope.usuopape) AS user_name";
        $strQuery .= ",case when sexo='1'then 'M' else 'F' end as sexo ";
        $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
        $strQuery .= ",t_antusu.tipo_aten_nombre ";
        $strQuery .= "FROM sgc_casos a ";
        $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
        $strQuery .= " join sgc_estados est on est.estadoid = a.estadoid  ";
        $strQuery .= " join sgc_municipio mun on mun.municipioid = a.municipioid ";
        $strQuery .= " join sgc_parroquias p on p.parroquiaid = a.parroquiaid";
        $strQuery .= " join sgc_usuario_operador u_ope on a.idusuopr = u_ope.idusuopr  ";
        $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id  ";
        $strQuery .= " left join sgc_casos_remitidos as casos_remi on a.idcaso=casos_remi.casos_id  ";
        $strQuery .= " left join sgc_direcciones_administrativas as direc on casos_remi.direccion_id=direc.id  ";
        $strQuery .= " WHERE a.idcaso= $idcaso";
        $strQuery .= " ORDER BY casofec_normal  ASC";
       $query = $db->query($strQuery);
       $resultado = $query->getResult();
       return $resultado;
    }



    //Metodo para obtener todos los casos para el reporte consolidado 
    public function reporte_consolidado($desde = null, $hasta = null, $tipo_atencion_usu = null, $sexo = null, $via_atencion = null,  $tipo_beneficiario = 0,$atencion_cuidadano = 0,$estatus = 0,$detalle_atencion = 0,$tipo_de_persona = 0,$id_estado = 0,$edad_min=null,$edad_max=null)
    {
       
        $db      = \Config\Database::connect();
        $strQuery = "SELECT a.idcaso,a.casotel,TRIM(a.casoced) AS casoced,a.tipo_atend_id,a.casonom,a.casoape,a.casodesc";
        $strQuery .= ",a.caso_nacionalidad,a.tipo_beneficiario,a.idrrss,a.ofiid,a.estadoid,a.id_tipo_atencion";
        $strQuery .= ",t_bene.tipo_beneficiario_nombre as tipo_beneficiario_nombre";
        $strQuery .= ",a.municipioid,a.parroquiaid,a.edad";
        $strQuery .= ",CONCAT(a.caso_nacionalidad,a.casoced) AS cedula";
        $strQuery .= ",CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
        $strQuery .= ",CONCAT(u_ope.usuopnom, ' ',' ', u_ope.usuopape) AS user_name";
        $strQuery .= ",case when sexo='1'then 'M' else 'F' end as sexo ";
        $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
        $strQuery .= ",t_antusu.tipo_aten_nombre ";
        $strQuery .= "FROM sgc_casos a ";
        $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
        $strQuery .= " join sgc_tipo_beneficiarios as t_bene on a.tipo_beneficiario=t_bene.tipo_beneficiario_id";
        $strQuery .= " join sgc_usuario_operador u_ope on a.idusuopr = u_ope.idusuopr  ";
        $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id   ";
        $strQuery .= " where a.borrado='false'  "; 
        $strWhere = "";
        if ($desde != 'null' and $hasta != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND casofec BETWEEN '$desde'AND '$hasta'";
            } else {
                $strWhere .= " AND casofec BETWEEN '$desde'AND '$hasta'";
            }
        }
       
        if ($edad_min != 'null' and $edad_max != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND edad BETWEEN '$edad_min'AND '$edad_max'";
            } else {
                $strWhere .= " AND edad BETWEEN '$edad_min'AND '$edad_max'";
            }
        }

        if ($tipo_atencion_usu != 0) {
            if (trim($strWhere) == "") {
                $strWhere .= " AND t_antusu.tipo_aten_id=$tipo_atencion_usu";
            } else {
                $strWhere .= " AND t_antusu.tipo_aten_id=$tipo_atencion_usu";
            }
        }
        if ($sexo != 0) {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.sexo=$sexo";
            } else {
                $strWhere .= " AND a.sexo=$sexo";
            }
        }
        if ($via_atencion != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.idrrss=$via_atencion";
            } else {
                $strWhere .= " AND a.idrrss=$via_atencion";
            }
        }
       


        if ($tipo_beneficiario != '0' && $tipo_beneficiario != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.tipo_beneficiario=$tipo_beneficiario";
            } else {
                $strWhere .= " AND a.tipo_beneficiario=$tipo_beneficiario";
            }
        }

        if ($atencion_cuidadano != '0' && $atencion_cuidadano != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.ofiid=$atencion_cuidadano";
            } else {
                $strWhere .= " AND a.ofiid=$atencion_cuidadano";
            }
        }

        if ($estatus != '0' && $estatus != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.idest=$estatus";
            } else {
                $strWhere .= " AND a.idest=$estatus";
            }
        }


        if ($detalle_atencion != '0' && $detalle_atencion != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.tipo_atend_id=$detalle_atencion";
            } else {
                $strWhere .= " AND a.tipo_atend_id=$detalle_atencion";
            }
        }

        if ($tipo_de_persona != '0' && $tipo_de_persona != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.caso_nacionalidad='$tipo_de_persona'";
            } else {
                $strWhere .= " AND a.caso_nacionalidad='$tipo_de_persona'";
            }
        }

        if ($id_estado != '0' && $id_estado != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.estadoid='$id_estado'";
            } else {
                $strWhere .= " AND a.estadoid='$id_estado'";
            }
        }

       $strQuery = $strQuery . $strWhere;
       $strQuery .= " ORDER BY a.idcaso  desc";
       $query = $db->query($strQuery);
       $resultado = $query->getResult();
       return $resultado;
    }

    //Metodo para obtener todos los casos para el reporte POR OPERADOR
    public function reporte_operador($desde = null, $hasta = null, $tipo_atencion_usu = null, $sexo = null, $via_atencion = null,  $tipo_beneficiario = 0,$atencion_cuidadano = 0,$estatus = 0,$detalle_atencion = 0,$tipo_de_persona = 0,$usuario=null,$id_estado=null,$edad_min=null,$edad_max=null)
    {
        $db      = \Config\Database::connect();
        $strQuery = "SELECT a.idcaso,a.casotel,TRIM(a.casoced) AS casoced,a.tipo_atend_id,a.casonom,a.casoape,a.casodesc";
        $strQuery .= ",a.caso_nacionalidad,a.tipo_beneficiario,a.idrrss,a.ofiid,a.estadoid,a.id_tipo_atencion";
        $strQuery .= ",t_bene.tipo_beneficiario_nombre as tipo_beneficiario_nombre";
        $strQuery .= ",a.municipioid,a.parroquiaid,a.edad";
        $strQuery .= ",CONCAT(a.caso_nacionalidad,a.casoced) AS cedula";
        $strQuery .= ",CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
        $strQuery .= ",CONCAT(u_ope.usuopnom, ' ',' ', u_ope.usuopape) AS user_name";
        $strQuery .= ",case when sexo='1'then 'M' else 'F' end as sexo ";
        $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
        $strQuery .= ",t_antusu.tipo_aten_nombre ";
        $strQuery .= "FROM sgc_casos a ";
        $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
        $strQuery .= " join sgc_tipo_beneficiarios as t_bene on a.tipo_beneficiario=t_bene.tipo_beneficiario_id";
        $strQuery .= " join sgc_usuario_operador u_ope on a.idusuopr = u_ope.idusuopr  ";
        $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id   ";
        $strQuery .= " where a.borrado='false'  ";
        $strWhere = "";
        if ($usuario != 'null'&&$usuario != '0') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND u_ope.idusuopr =$usuario";
            } else {
                $strWhere .= " AND u_ope.idusuopr =$usuario";
            }
        }

        if ($desde != 'null' and $hasta != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND casofec BETWEEN '$desde'AND '$hasta'";
            } else {
                $strWhere .= " AND casofec BETWEEN '$desde'AND '$hasta'";
            }
        }
       
        if ($edad_min != 'null' and $edad_max != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND edad BETWEEN '$edad_min'AND '$edad_max'";
            } else {
                $strWhere .= " AND edad BETWEEN '$edad_min'AND '$edad_max'";
            }
        }
        if ($tipo_atencion_usu != 0) {
            if (trim($strWhere) == "") {
                $strWhere .= " AND t_antusu.tipo_aten_id=$tipo_atencion_usu";
            } else {
                $strWhere .= " AND t_antusu.tipo_aten_id=$tipo_atencion_usu";
            }
        }
        if ($sexo != 0) {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.sexo=$sexo";
            } else {
                $strWhere .= " AND a.sexo=$sexo";
            }
        }
        if ($via_atencion != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.idrrss=$via_atencion";
            } else {
                $strWhere .= " AND a.idrrss=$via_atencion";
            }
        }
       

        if ($tipo_beneficiario != '0' && $tipo_beneficiario != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.tipo_beneficiario=$tipo_beneficiario";
            } else {
                $strWhere .= " AND a.tipo_beneficiario=$tipo_beneficiario";
            }
        }

        if ($atencion_cuidadano != '0' && $atencion_cuidadano != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.ofiid=$atencion_cuidadano";
            } else {
                $strWhere .= " AND a.ofiid=$atencion_cuidadano";
            }
        }

        if ($estatus != '0' && $estatus != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.idest=$estatus";
            } else {
                $strWhere .= " AND a.idest=$estatus";
            }
        }


        if ($detalle_atencion != '0' && $detalle_atencion != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.tipo_atend_id=$detalle_atencion";
            } else {
                $strWhere .= " AND a.tipo_atend_id=$detalle_atencion";
            }
        }

        if ($tipo_de_persona != '0' && $tipo_de_persona != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.caso_nacionalidad='$tipo_de_persona'";
            } else {
                $strWhere .= " AND a.caso_nacionalidad='$tipo_de_persona'";
            }
        }
        if ($id_estado != '0' && $id_estado != 'null') {
            if (trim($strWhere) == "") {
                $strWhere .= " AND a.estadoid='$id_estado'";
            } else {
                $strWhere .= " AND a.estadoid='$id_estado'";
            }
        }

        $strQuery = $strQuery . $strWhere;
        // return $strQuery;
        $strQuery .= " ORDER BY a.idcaso  desc";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }

    //Metodo para obtener los casos por estatus
    public function obtenerCasosPorEstatus(String $estatus)
    {
        $builder = $this->dbconn('sgc_casos a');
        $builder->join("sgc_estatus b", "b.idest = a.idest");
        $builder->join('sgc_usuario_operador c', "a.idusuopr = c.idusuopr");
        $builder->where("a.idest", $estatus);
        $builder->where("a.borrado", false);
        $query = $builder->get();
        return $query;
    }
    //Metodo para obtener los casos para los reportes
    public function obtenerCasosConsolidados(String $endDate, String $initDate)
    {
        $builder = $this->dbconn('sgc_casos a');
        $builder->select("a.idcaso, a.casofec, a.casoced, a.casonom, a.casoape, a.casotel, a.casonumsol, a.casoavz, b.estnom, c.rsnom, d.usuopnom, d.usuopape, e.estadonom, f.paisnom, g.municipionom, h.parroquianom");
        $builder->join("sgc_estatus b", "a.idest = b.idest");
        $builder->join('sgc_red_social c', "a.idrrss = c.idrrss");
        $builder->join('sgc_usuario_operador d', "a.idusuopr = d.idusuopr");
        $builder->join("sgc_estados e", "a.estadoid = e.estadoid");
        $builder->join("sgc_paises f", "e.paisid = f.paisid");
        $builder->join("sgc_municipio g", "a.municipioid = g.municipioid");
        $builder->join("sgc_parroquias h", "a.parroquiaid = h.parroquiaid");
        $builder->where("a.casofec BETWEEN '" . $initDate . "' AND '" . $endDate . "'");
        $builder->where("a.borrado", false);
        $builder->orderBy('a.idcaso', "ASC");
        $query = $builder->get();
        return $query;
    }

    //Metodo para obtener los casos por fecha
    public function contarCasosPorFecha(String $startDate, String $endDate)
    {
        $builder = $this->dbconn('sgc_casos');
        $builder->select('casofec');
        $builder->selectCount('casofec', 'cantCases');
        $builder->where("casofec BETWEEN '$startDate' AND '$endDate'");
        $builder->groupBy('casofec');
        $result = $builder->get();
        return $result;
    }


    public function consultar_estados($desde=null,$hasta=null)
    { 
       $db      = \Config\Database::connect();
       $strQuery = " select casos.estadoid,casos.estadonom,casos.count,casos.casofec";
       $strQuery .= " from ";
       $strQuery .= " (";
       $strQuery .= " SELECT ";
       $strQuery .= "  estados.estadoid,";
       $strQuery .= "  COUNT(c.estadoid) AS count,";
       $strQuery .= "  estados.estadonom,c.casofec";
       $strQuery .= " FROM";
       $strQuery .= " public.sgc_estados AS estados";
       $strQuery .= " LEFT JOIN sgc_casos AS c ON estados.estadoid = c.estadoid ";
       if ($desde != 'null' and $hasta != 'null')
        {
        $strQuery .= " WHERE c.borrado = false AND c.casofec BETWEEN '$desde' AND '$hasta' ";  # code...
        }
       $strQuery .= " GROUP BY";
       $strQuery .= " estados.estadoid, estados.estadonom,c.casofec";
       $strQuery .= " )as casos"; 
       $strQuery .= " ORDER BY casos.estadonom";
       $query = $db->query($strQuery);
       $resultado = $query->getResult();
       return $resultado;   
    }
    public function consultar_estatus_caso_estados($desde=null,$hasta=null)
    { 


       $db      = \Config\Database::connect();
       $strQuery = "  SELECT coalesce(estatus.estnom,'No Aplica')as estnom,estados.estadonom ,estados.estadoid,casos.casofec, ";
       $strQuery .= " COALESCE (casos.veces,0)as count ";
       $strQuery .= " FROM ";
       $strQuery .= "  ( SELECT e.estnom,e.idest ";
       $strQuery .= "   FROM public.sgc_estatus as e ";
       $strQuery .= "  WHERE e.borrado='false' ORDER BY idest ASC ";
       $strQuery .= "   ) as estatus ";
       $strQuery .= "   LEFT JOIN ";
       $strQuery .= "  ( ";
       $strQuery .= "  SELECT count(c.idcaso) as veces ,c.idest,c.estadoid ,c.casofec";
       $strQuery .= "  FROM sgc_casos c GROUP BY c.idest,c.estadoid ,c.casofec ORDER BY veces";
       $strQuery .= "  ) AS casos ON casos.idest=estatus.idest";
       $strQuery .= "  right  JOIN public.sgc_estados AS estados on casos.estadoid=estados.estadoid ";
       if ($desde != 'null' and $hasta != 'null') {
        $strQuery .= "where casos.casofec BETWEEN '$desde' AND '$hasta' ";  # code...
  }
       $strQuery .= " ORDER BY estados.estadonom";
       $query = $db->query($strQuery);
       $resultado = $query->getResult();
       return $resultado;


 }




    //Metodo que cuenta los Casos Atendididos
    public function contarCasosAtendidos()
    {
        $db      = \Config\Database::connect();
        $strQuery = "  SELECT COALESCE(COUNT(c.idrrss), 0) AS count,rs.red_s_nom FROM public.sgc_red_social rs ";
        $strQuery .= " LEFT JOIN public.sgc_casos c ON rs.red_s_id = c.idrrss AND c.borrado = 'false' ";
        $strQuery .= " And  rs.red_s_borrado = 'false' ";
        $strQuery .= " GROUP BY  rs.red_s_id, rs.red_s_nom ";
        $strQuery .= " order by  rs.red_s_nom desc";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }

    //Metodo que cuenta los Casos Atendididos por fecha
    public function contarCasosAtendidos_Fecha($desde, $hasta)
    {
        $db      = \Config\Database::connect();
        $strQuery = "  SELECT COALESCE(COUNT(c.idrrss), 0) AS count,rs.red_s_nom FROM public.sgc_red_social rs ";
        $strQuery .= " LEFT JOIN public.sgc_casos c ON rs.red_s_id = c.idrrss AND c.borrado = 'false' ";
        if ($desde != 'null' and $hasta != 'null') {
            $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
        }
        $strQuery .= " GROUP BY  rs.red_s_id, rs.red_s_nom ";
        $strQuery .= " order by  rs.red_s_nom desc";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }



    //Metodo que cuenta los casos ATENDIDOS GENERO MASCULINO
    public function contarCasosAtendidos_MASCULINO($desde = null, $hasta = null)
    {
        $db      = \Config\Database::connect();
        $strQuery = "  SELECT COALESCE(COUNT(c.idrrss), 0) AS count,rs.red_s_nom FROM public.sgc_red_social rs ";
        $strQuery .= " LEFT JOIN public.sgc_casos c ON rs.red_s_id = c.idrrss AND c.borrado = 'false' ";
        if ($desde != 'null' and $hasta != 'null') {
            $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
        }
        $strQuery .= " and c.sexo='1'";
        $strQuery .= " GROUP BY  rs.red_s_id, rs.red_s_nom";
        $strQuery .= " order by  rs.red_s_nom desc";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }
    //Metodo que cuenta los casos ATENDIDOS GENERO FEMENINO
    public function contarCasosAtendidos_FEMENINO($desde = null, $hasta = null)
    {
        $db      = \Config\Database::connect();
        $strQuery = "  SELECT COALESCE(COUNT(c.idrrss), 0) AS count,rs.red_s_nom FROM public.sgc_red_social rs ";
        $strQuery .= " LEFT JOIN public.sgc_casos c ON rs.red_s_id = c.idrrss AND c.borrado = 'false' ";
        $strQuery .= " and c.sexo='2'";
        if ($desde != 'null' and $hasta != 'null') {
            $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
        }
        $strQuery .= " GROUP BY  rs.red_s_id, rs.red_s_nom";
        $strQuery .= " order by  rs.red_s_nom desc";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }


    //Metodo que cuenta los Casos Atendididos por tipo de SOlicitud
    public function contarCasosTipoSolicitud()
    {
        
        $db      = \Config\Database::connect();
        $strQuery = " SELECT tipoaten.tipo_aten_nombre, COALESCE (casos.veces,0)as count ";
        $strQuery .= "FROM ";
        $strQuery .= "( ";
        $strQuery .= " SELECT ";
        $strQuery .= "ta.tipo_aten_nombre";
        $strQuery .= ",ta.tipo_aten_id ";
        $strQuery .= "FROM ";
        $strQuery .= "public.sgc_tipoatencion_usu ta ";
        $strQuery .= "WHERE ta.tipo_aten_borrado='false' ";
        $strQuery .= "ORDER BY ";
        $strQuery .= "tipo_aten_id ASC ";
        $strQuery .= ") as tipoaten ";
        $strQuery .= "LEFT JOIN";
        $strQuery .= "( ";
        $strQuery .= "SELECT ";
        $strQuery .= "count(c.idcaso) as veces ";
        $strQuery .= ",c.id_tipo_atencion ";
        $strQuery .= "FROM ";
        $strQuery .= "sgc_casos c ";
        $strQuery .= "GROUP BY c.id_tipo_atencion ";
        $strQuery .= "ORDER BY veces";
        $strQuery .= " ) AS casos ON casos.id_tipo_atencion=tipoaten.tipo_aten_id ";
        $strQuery .= " ORDER BY tipoaten.tipo_aten_nombre";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado; 
  
    }




//Metodo que cuenta los Casos Atendididos por tipo de SOlicitud Masculino
public function contarCasosTipoSolicitudMasculino($desde = null, $hasta = null)
{

    $db      = \Config\Database::connect();
    $strQuery = " SELECT tipoaten.tipo_aten_nombre, COALESCE (casos.veces,0)as count ";
    $strQuery .= "FROM  ";
    $strQuery .= "(  ";
    $strQuery .= "SELECT ta.tipo_aten_nombre,ta.tipo_aten_id ";
    $strQuery .= "FROM public.sgc_tipoatencion_usu ta  ";
    $strQuery .= "WHERE ta.tipo_aten_borrado='false' ORDER BY tipo_aten_id ASC ";
    $strQuery .= ") as tipoaten  ";
    $strQuery .= "LEFT JOIN ";
    $strQuery .= "(  ";
    $strQuery .= "SELECT count(c.idcaso) as veces ,c.id_tipo_atencion  ";
    $strQuery .= "FROM sgc_casos c ";
    $strQuery .= "where  c.sexo='1'";
    if ($desde != 'null' and $hasta != 'null') {
        $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
    }
    $strQuery .= "GROUP BY c.id_tipo_atencion ORDER BY veces ";
    $strQuery .= ") AS casos ON casos.id_tipo_atencion=tipoaten.tipo_aten_id ";
    $strQuery .= "ORDER BY tipoaten.tipo_aten_nombre";
    $query = $db->query($strQuery);
    $resultado = $query->getResult();
    return $resultado; 
}

//Metodo que cuenta los Casos Atendididos por tipo de SOlicitud Femenino
public function contarCasosTipoSolicitudFemenino($desde = null, $hasta = null)
{
    
    $db      = \Config\Database::connect();
    $strQuery = " SELECT tipoaten.tipo_aten_nombre, COALESCE (casos.veces,0)as count ";
    $strQuery .= "FROM  ";
    $strQuery .= "(  ";
    $strQuery .= "SELECT ta.tipo_aten_nombre,ta.tipo_aten_id ";
    $strQuery .= "FROM public.sgc_tipoatencion_usu ta  ";
    $strQuery .= "WHERE ta.tipo_aten_borrado='false' ORDER BY tipo_aten_id ASC ";
    $strQuery .= ") as tipoaten  ";
    $strQuery .= "LEFT JOIN ";
    $strQuery .= "(  ";
    $strQuery .= "SELECT count(c.idcaso) as veces ,c.id_tipo_atencion  ";
    $strQuery .= "FROM sgc_casos c ";
    $strQuery .= "where  c.sexo='2'";
    if ($desde != 'null' and $hasta != 'null') {
        $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
    }
    $strQuery .= "GROUP BY c.id_tipo_atencion ORDER BY veces ";
    $strQuery .= ") AS casos ON casos.id_tipo_atencion=tipoaten.tipo_aten_id ";
    $strQuery .= "ORDER BY tipoaten.tipo_aten_nombre";
    $query = $db->query($strQuery);
    $resultado = $query->getResult();
    return $resultado; 
}








//Metodo que cuenta los Casos Atendididos por tipo de SOLICITUDES ESTADALES
public function ContarasosTipoSolicitud_Estadal($desde, $hasta)
{
    
    $db      = \Config\Database::connect();
    $strQuery = " SELECT COALESCE(COUNT(c.id_tipo_atencion), 0) AS count, COALESCE(aten.tipo_aten_nombre,'No Aplica')as tipo_aten_nombre,";
    $strQuery .= "estados.estadonom ";
    $strQuery .= "FROM public.sgc_casos AS c ";
    $strQuery .= " RIGHT JOIN sgc_tipoatencion_usu AS aten ON c.id_tipo_atencion = aten.tipo_aten_id ";
    $strQuery .= " RIGHT JOIN public.sgc_estados AS estados ON c.estadoid = estados.estadoid ";
    $strQuery .= " WHERE COALESCE(c.borrado, FALSE) IS FALSE AND COALESCE(aten.tipo_aten_borrado, FALSE) IS FALSE ";
    if ($desde != 'null' and $hasta != 'null') {
        $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
    }
    $strQuery .= " GROUP BY aten.tipo_aten_nombre ,estados.estadonom";
    $strQuery .= " ORDER BY estados.estadonom ";
    $query = $db->query($strQuery);
    $resultado = $query->getResult();
    return $resultado; 

}


    //Metodo que cuenta los Casos Atendididos por tipo de SOlicitud por Fecha
    public function contarCasosTipoSolicitudFecha($desde = null, $hasta = null)
    {
        
        $db      = \Config\Database::connect();
        $strQuery = " SELECT COALESCE(COUNT(c.id_tipo_atencion), 0) AS count, tip_ate.tipo_aten_nombre ";
        $strQuery .= "  FROM (";
        $strQuery .= "SELECT * FROM sgc_tipoatencion_usu WHERE tipo_aten_borrado = 'false'";
        $strQuery .= " ) AS tip_ate ";
        $strQuery .= "LEFT OUTER JOIN public.sgc_casos AS c ON c.id_tipo_atencion = tip_ate.tipo_aten_id ";
        $strQuery .= "AND c.borrado = 'false'";
        if ($desde != 'null' and $hasta != 'null') {
            $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
        }
        $strQuery .= "GROUP BY tip_ate.tipo_aten_nombre ";
        $strQuery .= "ORDER BY tip_ate.tipo_aten_nombre ASC";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado; 
    }


    
//Metodo que cuenta los Casos Atendididos por tipo de SOlicitud Masculino
public function contarCasosTipoSolicitudMasculinoFecha($desde = null, $hasta = null)
{
    
        $db      = \Config\Database::connect();
        $strQuery = " SELECT COALESCE(COUNT(c.id_tipo_atencion), 0) AS count, tip_ate.tipo_aten_nombre ";
        $strQuery .= "  FROM (";
        $strQuery .= "SELECT * FROM sgc_tipoatencion_usu WHERE tipo_aten_borrado = 'false'";
        $strQuery .= " ) AS tip_ate ";
        $strQuery .= "LEFT OUTER JOIN public.sgc_casos AS c ON c.id_tipo_atencion = tip_ate.tipo_aten_id ";
        $strQuery .= "AND c.borrado = 'false'";
        if ($desde != 'null' and $hasta != 'null') {
            $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
        }
        $strQuery .= " and c.sexo='1'";
        $strQuery .= "GROUP BY tip_ate.tipo_aten_nombre ";
        $strQuery .= "ORDER BY tip_ate.tipo_aten_nombre ASC";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado; 
      
        
}




//Metodo que cuenta los Casos Atendididos por tipo de SOlicitud Femenino por fecha
public function contarCasosTipoSolicitudFemeninoFecha($desde = null, $hasta = null)
{
    
    $db      = \Config\Database::connect();
    $strQuery = " SELECT COALESCE(COUNT(c.id_tipo_atencion), 0) AS count, tip_ate.tipo_aten_nombre ";
    $strQuery .= "  FROM (";
    $strQuery .= "SELECT * FROM sgc_tipoatencion_usu WHERE tipo_aten_borrado = 'false'";
    $strQuery .= " ) AS tip_ate ";
    $strQuery .= "LEFT OUTER JOIN public.sgc_casos AS c ON c.id_tipo_atencion = tip_ate.tipo_aten_id ";
    $strQuery .= "AND c.borrado = 'false'";
    if ($desde != 'null' and $hasta != 'null') {
        $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
    }
    $strQuery .= " and c.sexo='2'";
    $strQuery .= "GROUP BY tip_ate.tipo_aten_nombre ";
    $strQuery .= "ORDER BY tip_ate.tipo_aten_nombre ASC";
    $query = $db->query($strQuery);
    $resultado = $query->getResult();
    return $resultado; 

}


    //	Metodo que cuenta los Casos POR ESTATUS
     public function contarCasosEstatus()
     {
        $db      = \Config\Database::connect();
        $strQuery = " SELECT estatus.estnom, COALESCE (casos.veces,0)as count ";
        $strQuery .= "FROM ";
        $strQuery .= " ( ";
        $strQuery .= " SELECT e.estnom,e.idest ";
        $strQuery .= " FROM ";
        $strQuery .= " public.sgc_estatus as e ";
        $strQuery .= " WHERE e.borrado='false' ";
        $strQuery .= " ORDER BY ";
        $strQuery .= "  idest ASC ";
        $strQuery .= " ) as estatus";
        $strQuery .= " LEFT JOIN ";
        $strQuery .= " ( ";
        $strQuery .= "  SELECT ";
        $strQuery .= " count(c.idcaso) as veces ";
        $strQuery .= "  ,c.idest ";
        $strQuery .= " FROM ";
        $strQuery .= " sgc_casos c ";
        $strQuery .= " GROUP BY ";
        $strQuery .= " c.idest ";
        $strQuery .= " ORDER BY  ";
        $strQuery .= " veces ";
        $strQuery .= " ) AS casos ON casos.idest=estatus.idest";
        $strQuery .= " ORDER BY estatus.estnom";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
     }
    
     //Metodo que cuenta los Casos POR ESTATUS POR FECHA 
     public function contarCasosEstatusFecha($desde = null, $hasta = null)
     {
        $db      = \Config\Database::connect();
        $strQuery = " SELECT COALESCE(COUNT(c.idest), 0) AS count,est.estnom FROM sgc_estatus AS est ";
        $strQuery .= "LEFT OUTER JOIN public.sgc_casos AS c ON c.idest = est.idest AND c.borrado = 'false' ";
        if ($desde != 'null' and $hasta != 'null') {
            $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
        }
        $strQuery .= "   GROUP BY  est.estnom"; 
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;

     }
        
     //Metodo que cuenta los Casos POR ESTATUS POR FECHA 
     public function Estatus_casos_estadal($desde = null, $hasta = null)
     {
        $db      = \Config\Database::connect();
        $strQuery = " SELECT COALESCE(COUNT(c.idest), 0) AS count,est.estnom, COALESCE(c.estadoid,0) AS estadoid ";
        $strQuery .= "FROM sgc_estatus AS est ";
        $strQuery .= "LEFT OUTER JOIN public.sgc_casos AS c ON c.idest = est.idest ";
        $strQuery .= " AND c.borrado = 'false'  ";
        if ($desde != 'null' and $hasta != 'null') {
            $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  # code...
        }
        $strQuery .= " GROUP BY est.estnom,c.estadoid"; 
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;

     }

     





    //Metodo que cuenta los Casos Atendididos
    public function contarCasosAtendidos_filtros($desde = null, $hasta = null)
    {
        $db      = \Config\Database::connect();
        $strQuery = " SELECT count  (red_s.red_s_nom),red_s.red_s_nom FROM public.sgc_casos as c ";
        $strQuery .= "join sgc_red_social red_s on c.idrrss  = red_s.red_s_id ";
        $strQuery .= "where c.casofec BETWEEN '$desde' AND '$hasta'";
        $strQuery .= " and c.borrado='false' ";
        $strQuery .= "group by (red_s.red_s_nom,red_s.red_s_nom) ";
        //return  $strQuery;
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }

  

    //Metodo que cuenta los casos por tipo de beneficiario 
    public function contarCasos_Tipo_Beneficiario($desde = null, $hasta = null)
    {  
        $db      = \Config\Database::connect();
        $strQuery = " SELECT  COALESCE(COUNT(c.tipo_beneficiario), 0) AS count,";
        $strQuery .= " tb.tipo_beneficiario_nombre";
        $strQuery .= "  FROM";
        $strQuery .= "  public.sgc_casos AS c";
        $strQuery .= "  RIGHT JOIN sgc_tipo_beneficiarios AS tb ON c.tipo_beneficiario = tb.tipo_beneficiario_id";
        $strQuery .= "  WHERE";
        $strQuery .= "  c.borrado = 'false' OR c.tipo_beneficiario IS NULL ";
        $strQuery .= "  and tb.tipo_beneficiario_borrado='false' ";
        if ($desde != 'null' and $hasta != 'null') {
            $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  
        }
        $strQuery .= " group by (tb.tipo_beneficiario_nombre)  ";
        $strQuery .= " order by tb.tipo_beneficiario_nombre asc  ";

        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
        
    }


  //METODO QUE CUENTA LOS CASOS ESTADALES POR TIPO DE BENEFICIAIRIO
  public function ContarCasos_Estadal_Tipo_Beneficiario($desde = null, $hasta = null)
  { 
    $db      = \Config\Database::connect();
    $strQuery = " SELECT  COALESCE(COUNT(c.tipo_beneficiario), 0) AS count, ";
    $strQuery .= " COALESCE(tb.tipo_beneficiario_nombre,'No Aplica')as tipo_beneficiario_nombre,estados.estadonom ";
    $strQuery .= "  FROM";
    $strQuery .= "  public.sgc_casos AS c";
    $strQuery .= "   RIGHT JOIN sgc_tipo_beneficiarios AS tb ON c.tipo_beneficiario = tb.tipo_beneficiario_id ";
    $strQuery .= "   RIGHT JOIN public.sgc_estados AS estados ON c.estadoid = estados.estadoid  ";
    $strQuery .= "  WHERE";
    $strQuery .= "   COALESCE(c.borrado, FALSE) IS FALSE  ";
    $strQuery .= "  AND COALESCE(tb.tipo_beneficiario_borrado, FALSE) IS FALSE  ";
    if ($desde != 'null' and $hasta != 'null') {
        $strQuery .= "and c.casofec BETWEEN '$desde' AND '$hasta'";  
    }
    $strQuery .= " GROUP BY tb.tipo_beneficiario_nombre ,estados.estadonom   ";
    $strQuery .= " ORDER BY estados.estadonom";
    $query = $db->query($strQuery);
    $resultado = $query->getResult();
    return $resultado;

}


//Metodo que cuenta los casos por tipo de beneficiario  en funcion de la fecha 
public function contarCasos_Tipo_Beneficiario_fecha($desde = null, $hasta = null)
{
    $db      = \Config\Database::connect();
    $strQuery = " SELECT  ";
    $strQuery .= "  tb.tipo_beneficiario_id";
    $strQuery .= " ,tb.tipo_beneficiario_nombre";
    $strQuery .= " ,COALESCE(casos_fecha.count,0) as total_fecha_tipo,";
    $strQuery .= "  COALESCE(sum(casos_fecha.count),0) as count ";
    $strQuery .= " FROM";
    $strQuery .= " sgc_tipo_beneficiarios AS tb";
    $strQuery .= " left join ";
    $strQuery .= " (";
    $strQuery .= " select c.casofec,c.tipo_beneficiario,count(c.tipo_beneficiario)";
    $strQuery .= " from sgc_casos as c ";
    if ($desde != 'null' and $hasta != 'null') {
        $strQuery .= "WHERE c.casofec BETWEEN '$desde' AND '$hasta'";  
    }
    $strQuery .= "  group by (c.tipo_beneficiario,c.casofec)";
    $strQuery .= "  ) as casos_fecha ON tb.tipo_beneficiario_id=casos_fecha.tipo_beneficiario";
    $strQuery .= "  group by (tb.tipo_beneficiario_id , tb.tipo_beneficiario_nombre,casos_fecha.count ) ";
    $strQuery .= "  order by tb.tipo_beneficiario_nombre asc";
    $query = $db->query($strQuery);
    $resultado = $query->getResult();
    return $resultado;
    
}



    //Metodo para agregar  el nombre de los cocumentos de asosciados a los casos
    public function agregar_docu_casos(array $documentos_casos)

    {
        $builder = $this->dbconn('sgc_documentos_casos');
        $query = $builder->insert($documentos_casos);
        return $query;
    }

    //Metodo que busca el correo del usuario en funcion del caso Y la descripcion del caso 
    public function buscar_correo($caseid)
	{
		$builder = $this->dbconn('public.sgc_casos as c');
		$builder->select(
			"c.correo,c.casonom,c.casoape ,c.casodesc"
		);
		$builder->where(['c.idcaso' => $caseid]);
		$query = $builder->get();
		return $query;
	}

    //Metodo que busca el correo del usuario en funcion del caso Y la descripcion del caso 
    public function buscar_token($token)
	{
	
        $db      = \Config\Database::connect();
        $strQuery = " SELECT t.id_usuario FROM  sgc_usuario_token as t   ";
        $strQuery .= " where t.token='$token'";
        //return $strQuery;
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        
        return $resultado;
	}

//   //Metodo que busca el correo del usuario en funcion del caso Y la descripcion del caso 
  public function buscar_usuario($datos)
  {

  
    $builder = $this->dbconn('public.sgc_casos as c');
    $builder->select(
        "*"
    );
    $builder->where(['c.casoced' => $datos]);
    $query = $builder->get();
    return $query;
    

  }


//Metodo para obtener todas las atenciones de una caso 
public function reporte_atencion($desde = null, $hasta = null, $tipo_pi = null, $tipo_atencion_usu = null, $sexo = null, $via_atencion = null, $direcciones_caso = null, $tipo_beneficiario = 0,$atencion_cuidadano = 0,$estatus = 0)
{
    $db      = \Config\Database::connect();
    $strQuery = " SELECT c.idcaso,CONCAT(c.caso_nacionalidad,c.casoced) AS cedula,CONCAT(c.casonom, ' ',' ', c.casoape) AS nombre,";
    $strQuery .= " c.casotel,c.casodesc,c.estnom,sc.idestllam,sc.segcoment,case when sexo='1'then 'M' else 'F' end as sexo,";
    $strQuery .= " to_char(sc.segfec,'dd/mm/yyyy') as fecha_seguimiento,sc.segfec as fecha_seg_normal,";
    $strQuery .= " CONCAT(usu.usuopnom, ' ',' ', usu.usuopape) AS nombre_usuario";
    $strQuery .= ",case when c.tipo_beneficiario='1'then 'Usuario' else 'Emprendedor' end as tipo_beneficiario";
    $strQuery .= " FROM public.sgc_seguimiento_caso as sc";
    $strQuery .= " join vista_casos_atendidos as c on sc.idcaso=c.idcaso";
    $strQuery .= " join sgc_usuario_operador as usu on sc.idusuopr=usu.idusuopr";
    $strQuery .= " where sc.borrado='false'  ";
    $strWhere = "";
    $strQuery = $strQuery . $strWhere;
    $strQuery .= " ORDER BY c.idcaso  desc";
    $query = $db->query($strQuery);
    $resultado = $query->getResult();
    return $resultado;
  
  
}




}
