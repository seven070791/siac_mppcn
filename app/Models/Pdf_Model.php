<?php

namespace App\Models;

class Pdf_Model extends BaseModel
{

    //Metodo para obtener todas las direcciones
    public function obtenerCasos($idcaso = null)
    {

        $db      = \Config\Database::connect();
        $strQuery = "SELECT distinct a.caso_hora,a.idcaso, a.tipo_beneficiario,a.id_tipo_atencion,a.tipo_atend_id,a.casotel,TRIM(a.casoced) AS casoced,a.casonom,a.casoape,a.casodesc";
        $strQuery .= ",a.edad,to_char(a.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimiento,a.fecha_nacimiento as fecha_nacimiento_normal , a.casofec as casofec_normal,a.caso_nacionalidad,a.idrrss,a.ofiid,a.estadoid,a.id_tipo_atencion";
        $strQuery .= ",a.municipioid,a.parroquiaid,a.direccion,a.correo,a.ente_adscrito_id";
        $strQuery .= ",CONCAT(a.caso_nacionalidad,a.casoced) AS cedula";
        $strQuery .= ",t_bene.tipo_beneficiario_nombre as tipo_beneficiario_nombre";
        $strQuery .= ",cgr.competencia_cgr,cgr.asume_cgr";
        $strQuery .= ",est.estadonom";
        $strQuery .= ",mun.municipionom";
        $strQuery .= ",p.parroquianom";
        $strQuery .= ",denu.denu_afecta_persona,denu.denu_afecta_comunidad,denu.denu_afecta_terceros";
        $strQuery .= ",denu.denu_involucrados,denu.denu_fecha_hechos,denu.denu_instancia_popular";
        $strQuery .= ",denu.denu_rif_instancia,denu.denu_ente_financiador,denu.denu_nombre_proyecto,denu.denu_monto_aprovado";
        $strQuery .= ",CONCAT(a.casonom, ' ',' ', a.casoape) AS nombre";
        $strQuery .= ",CONCAT(u_ope.usuopnom, ' ',' ', u_ope.usuopape) AS user_name";
        $strQuery .= ",u_ope.usercargo";
        $strQuery .= ",case when sexo='1'then 'M' else 'F' end as sexo ";
        $strQuery .= ",to_char(a.casofec,'dd/mm/yyyy') as casofec,a.casofec as casofec_normal,b.estnom ";
        $strQuery .= ",t_antusu.tipo_aten_nombre ";
        $strQuery .= "FROM sgc_casos a ";
        $strQuery .= " join sgc_estatus b on b.idest = a.idest  ";
        $strQuery .= " join sgc_tipo_beneficiarios as t_bene on a.tipo_beneficiario=t_bene.tipo_beneficiario_id";
        $strQuery .= " join sgc_usuario_operador u_ope on a.idusuopr = u_ope.idusuopr  ";
        $strQuery .= " join sgc_tipoatencion_usu as t_antusu on a.id_tipo_atencion=t_antusu.tipo_aten_id  ";
        $strQuery .= " left join sgc_registro_cgr cgr on a.idcaso=cgr.id_caso  ";
        $strQuery .= " left join sgc_casos_denuncias denu on a.idcaso=denu_id_caso ";
        $strQuery .= " join sgc_estados est on est.estadoid = a.estadoid  ";
        $strQuery .= " join sgc_municipio mun on mun.municipioid = a.municipioid ";
        $strQuery .= " join sgc_parroquias p on p.parroquiaid = a.parroquiaid";
        $strQuery .= " where a.borrado='false'  ";
        $strQuery .= " AND a.idcaso='$idcaso'  ";
        $strQuery .= " ORDER BY a.idcaso  desc";
        $query = $db->query($strQuery);
        $resultado = $query->getResult();
        return $resultado;
    }
}
