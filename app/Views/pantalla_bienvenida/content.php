<!-- Content Wrapper. Contains page content -->
<?php
$session = session();
?>

<link rel="stylesheet" href="<?php echo base_url(); ?>/css_paginas/botones_datatable.css">
<style>
  table.dataTable thead,
  table.dataTable tfoot {
    background: linear-gradient(to right, #a9b6c2, #a9b6c2, #a9b6c2);
  }
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid"> 
      <div class="row justify-content-center align-items-center" style="height: 70vh;"> 
        <div class="col-lg-10 col-sm-10 col-md-10 "> <!-- Reducir el tamaño de la columna para que la card se centre -->
          <div class="card mt-5"> 
            <div class="card-body imagen">
              <section class="form-login_imagen">
                <div class="imagencentral">
                  <img class="img-fluid mx-auto d-block" src="<?= base_url() ?>/img/Logo-MinComercio-Sin-fondo.png" id="imagencentral">
                </div>
                <br>
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
