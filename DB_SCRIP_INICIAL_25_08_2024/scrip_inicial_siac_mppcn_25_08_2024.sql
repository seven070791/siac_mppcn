--
-- PostgreSQL database dump
--

-- Dumped from database version 14.13 (Ubuntu 14.13-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 16.1 (Ubuntu 16.1-1.pgdg22.04+1)

-- Started on 2024-08-25 20:01:58 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 246 (class 1259 OID 74812)
-- Name: sgc_auditoria_sistema; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_auditoria_sistema (
    audi_id integer NOT NULL,
    audi_user_id integer NOT NULL,
    audi_accion text NOT NULL,
    audi_fecha date DEFAULT CURRENT_DATE NOT NULL,
    audi_hora character(11)
);


ALTER TABLE public.sgc_auditoria_sistema OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 74811)
-- Name: sgc_auditoria_sistema_audi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_auditoria_sistema_audi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_auditoria_sistema_audi_id_seq OWNER TO postgres;

--
-- TOC entry 3674 (class 0 OID 0)
-- Dependencies: 245
-- Name: sgc_auditoria_sistema_audi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_auditoria_sistema_audi_id_seq OWNED BY public.sgc_auditoria_sistema.audi_id;


--
-- TOC entry 248 (class 1259 OID 74822)
-- Name: sgc_casos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_casos (
    idcaso integer NOT NULL,
    casofec date NOT NULL,
    casoced character varying(48) NOT NULL,
    casonom character varying(48) NOT NULL,
    casoape character varying(48) NOT NULL,
    casotel character varying(48) NOT NULL,
    casonumsol character varying(48) NOT NULL,
    idest integer NOT NULL,
    idrrss integer NOT NULL,
    idusuopr integer NOT NULL,
    estadoid integer NOT NULL,
    municipioid integer NOT NULL,
    parroquiaid integer NOT NULL,
    ofiid integer NOT NULL,
    casodesc character varying(4096) NOT NULL,
    id_tipo_atencion integer DEFAULT 0,
    sexo integer,
    caso_nacionalidad character varying(1),
    borrado boolean DEFAULT false,
    tipo_beneficiario integer DEFAULT 1,
    direccion text,
    correo text,
    ente_adscrito_id integer,
    caso_hora text,
    tipo_atend_id integer,
    edad integer,
    fecha_nacimiento date
);


ALTER TABLE public.sgc_casos OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 74864)
-- Name: sgc_casos_denuncias; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_casos_denuncias (
    denu_id integer NOT NULL,
    denu_afecta_persona boolean DEFAULT false NOT NULL,
    denu_afecta_comunidad boolean DEFAULT false NOT NULL,
    denu_afecta_terceros boolean DEFAULT false NOT NULL,
    denu_involucrados text,
    denu_fecha_hechos date DEFAULT CURRENT_DATE NOT NULL,
    denu_instancia_popular text,
    denu_rif_instancia text,
    denu_ente_financiador text,
    denu_nombre_proyecto text,
    denu_monto_aprovado text,
    denu_id_caso integer,
    denu_borrado boolean DEFAULT false
);


ALTER TABLE public.sgc_casos_denuncias OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 74863)
-- Name: sgc_casos_denuncias_denu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_casos_denuncias_denu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_casos_denuncias_denu_id_seq OWNER TO postgres;

--
-- TOC entry 3675 (class 0 OID 0)
-- Dependencies: 249
-- Name: sgc_casos_denuncias_denu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_casos_denuncias_denu_id_seq OWNED BY public.sgc_casos_denuncias.denu_id;


--
-- TOC entry 247 (class 1259 OID 74821)
-- Name: sgc_casos_idcaso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_casos_idcaso_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_casos_idcaso_seq OWNER TO postgres;

--
-- TOC entry 3676 (class 0 OID 0)
-- Dependencies: 247
-- Name: sgc_casos_idcaso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_casos_idcaso_seq OWNED BY public.sgc_casos.idcaso;


--
-- TOC entry 252 (class 1259 OID 74878)
-- Name: sgc_casos_remitidos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_casos_remitidos (
    casos_re_id integer NOT NULL,
    casos_id integer NOT NULL,
    direccion_id integer NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    idusuop integer,
    vigencia boolean DEFAULT true,
    fecha date DEFAULT CURRENT_DATE
);


ALTER TABLE public.sgc_casos_remitidos OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 74877)
-- Name: sgc_casos_remitidos_casos_re_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_casos_remitidos_casos_re_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_casos_remitidos_casos_re_id_seq OWNER TO postgres;

--
-- TOC entry 3677 (class 0 OID 0)
-- Dependencies: 251
-- Name: sgc_casos_remitidos_casos_re_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_casos_remitidos_casos_re_id_seq OWNED BY public.sgc_casos_remitidos.casos_re_id;


--
-- TOC entry 258 (class 1259 OID 74923)
-- Name: sgc_direcciones_administrativas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_direcciones_administrativas (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    correo text
);


ALTER TABLE public.sgc_direcciones_administrativas OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 74922)
-- Name: sgc_direcciones_administrativas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_direcciones_administrativas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_direcciones_administrativas_id_seq OWNER TO postgres;

--
-- TOC entry 3678 (class 0 OID 0)
-- Dependencies: 257
-- Name: sgc_direcciones_administrativas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_direcciones_administrativas_id_seq OWNED BY public.sgc_direcciones_administrativas.id;


--
-- TOC entry 254 (class 1259 OID 74888)
-- Name: sgc_documentos_casos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_documentos_casos (
    docu_id integer NOT NULL,
    docu_id_caso integer NOT NULL,
    docu_ruta text NOT NULL,
    docu_descripcion text
);


ALTER TABLE public.sgc_documentos_casos OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 74887)
-- Name: sgc_documentos_casos_docu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_documentos_casos_docu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_documentos_casos_docu_id_seq OWNER TO postgres;

--
-- TOC entry 3679 (class 0 OID 0)
-- Dependencies: 253
-- Name: sgc_documentos_casos_docu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_documentos_casos_docu_id_seq OWNED BY public.sgc_documentos_casos.docu_id;


--
-- TOC entry 209 (class 1259 OID 56035)
-- Name: sgc_ente_asdcrito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_ente_asdcrito (
    ente_id integer NOT NULL,
    ente_nombre text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sgc_ente_asdcrito OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 56041)
-- Name: sgc_ente_asdcrito_ente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_ente_asdcrito_ente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_ente_asdcrito_ente_id_seq OWNER TO postgres;

--
-- TOC entry 3680 (class 0 OID 0)
-- Dependencies: 210
-- Name: sgc_ente_asdcrito_ente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_ente_asdcrito_ente_id_seq OWNED BY public.sgc_ente_asdcrito.ente_id;


--
-- TOC entry 211 (class 1259 OID 56042)
-- Name: sgc_estados; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_estados (
    estadoid integer NOT NULL,
    estadonom character varying(48) NOT NULL,
    paisid integer NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.sgc_estados OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 56046)
-- Name: sgc_estados_estadoid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_estados_estadoid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_estados_estadoid_seq OWNER TO postgres;

--
-- TOC entry 3681 (class 0 OID 0)
-- Dependencies: 212
-- Name: sgc_estados_estadoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_estados_estadoid_seq OWNED BY public.sgc_estados.estadoid;


--
-- TOC entry 244 (class 1259 OID 74735)
-- Name: sgc_estatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_estatus (
    idest integer NOT NULL,
    estnom character varying(48) NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.sgc_estatus OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 74734)
-- Name: sgc_estatus_idest_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_estatus_idest_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_estatus_idest_seq OWNER TO postgres;

--
-- TOC entry 3682 (class 0 OID 0)
-- Dependencies: 243
-- Name: sgc_estatus_idest_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_estatus_idest_seq OWNED BY public.sgc_estatus.idest;


--
-- TOC entry 213 (class 1259 OID 56051)
-- Name: sgc_estatus_llamadas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_estatus_llamadas (
    idestllam integer NOT NULL,
    estllamnom character varying(48) NOT NULL
);


ALTER TABLE public.sgc_estatus_llamadas OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 56054)
-- Name: sgc_estatus_llamadas_idestllam_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_estatus_llamadas_idestllam_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_estatus_llamadas_idestllam_seq OWNER TO postgres;

--
-- TOC entry 3683 (class 0 OID 0)
-- Dependencies: 214
-- Name: sgc_estatus_llamadas_idestllam_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_estatus_llamadas_idestllam_seq OWNED BY public.sgc_estatus_llamadas.idestllam;


--
-- TOC entry 215 (class 1259 OID 56055)
-- Name: sgc_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_migrations (
    id bigint NOT NULL,
    version character varying(255) NOT NULL,
    class character varying(255) NOT NULL,
    "group" character varying(255) NOT NULL,
    namespace character varying(255) NOT NULL,
    "time" integer NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.sgc_migrations OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 56060)
-- Name: sgc_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_migrations_id_seq OWNER TO postgres;

--
-- TOC entry 3684 (class 0 OID 0)
-- Dependencies: 216
-- Name: sgc_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_migrations_id_seq OWNED BY public.sgc_migrations.id;


--
-- TOC entry 217 (class 1259 OID 56061)
-- Name: sgc_municipio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_municipio (
    municipioid integer NOT NULL,
    municipionom character varying(48) NOT NULL,
    estadoid integer NOT NULL
);


ALTER TABLE public.sgc_municipio OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 56064)
-- Name: sgc_municipio_municipioid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_municipio_municipioid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_municipio_municipioid_seq OWNER TO postgres;

--
-- TOC entry 3685 (class 0 OID 0)
-- Dependencies: 218
-- Name: sgc_municipio_municipioid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_municipio_municipioid_seq OWNED BY public.sgc_municipio.municipioid;


--
-- TOC entry 219 (class 1259 OID 56065)
-- Name: sgc_oficinas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_oficinas (
    idofi integer NOT NULL,
    ofinom character varying(48) NOT NULL
);


ALTER TABLE public.sgc_oficinas OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 56068)
-- Name: sgc_oficinas_idofi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_oficinas_idofi_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_oficinas_idofi_seq OWNER TO postgres;

--
-- TOC entry 3686 (class 0 OID 0)
-- Dependencies: 220
-- Name: sgc_oficinas_idofi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_oficinas_idofi_seq OWNED BY public.sgc_oficinas.idofi;


--
-- TOC entry 221 (class 1259 OID 56069)
-- Name: sgc_paises; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_paises (
    paisid integer NOT NULL,
    paisnom character varying(48) NOT NULL
);


ALTER TABLE public.sgc_paises OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 56072)
-- Name: sgc_paises_paisid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_paises_paisid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_paises_paisid_seq OWNER TO postgres;

--
-- TOC entry 3687 (class 0 OID 0)
-- Dependencies: 222
-- Name: sgc_paises_paisid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_paises_paisid_seq OWNED BY public.sgc_paises.paisid;


--
-- TOC entry 223 (class 1259 OID 56073)
-- Name: sgc_parroquias; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_parroquias (
    parroquiaid integer NOT NULL,
    parroquianom character varying(48) NOT NULL,
    municipioid integer NOT NULL
);


ALTER TABLE public.sgc_parroquias OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 56076)
-- Name: sgc_parroquias_parroquiaid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_parroquias_parroquiaid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_parroquias_parroquiaid_seq OWNER TO postgres;

--
-- TOC entry 3688 (class 0 OID 0)
-- Dependencies: 224
-- Name: sgc_parroquias_parroquiaid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_parroquias_parroquiaid_seq OWNED BY public.sgc_parroquias.parroquiaid;


--
-- TOC entry 225 (class 1259 OID 56077)
-- Name: sgc_red_social; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_red_social (
    red_s_id integer NOT NULL,
    red_s_nom character varying(48) NOT NULL,
    red_s_borrado boolean DEFAULT false
);


ALTER TABLE public.sgc_red_social OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 56081)
-- Name: sgc_red_social_idrrss_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_red_social_idrrss_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_red_social_idrrss_seq OWNER TO postgres;

--
-- TOC entry 3689 (class 0 OID 0)
-- Dependencies: 226
-- Name: sgc_red_social_idrrss_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_red_social_idrrss_seq OWNED BY public.sgc_red_social.red_s_id;


--
-- TOC entry 227 (class 1259 OID 56082)
-- Name: sgc_registro_cgr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_registro_cgr (
    id_cgr integer NOT NULL,
    competencia_cgr integer NOT NULL,
    asume_cgr integer NOT NULL,
    borrado_cgr boolean DEFAULT false NOT NULL,
    id_caso integer
);


ALTER TABLE public.sgc_registro_cgr OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 56086)
-- Name: sgc_registro_cgr_id_cgr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_registro_cgr_id_cgr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_registro_cgr_id_cgr_seq OWNER TO postgres;

--
-- TOC entry 3690 (class 0 OID 0)
-- Dependencies: 228
-- Name: sgc_registro_cgr_id_cgr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_registro_cgr_id_cgr_seq OWNED BY public.sgc_registro_cgr.id_cgr;


--
-- TOC entry 229 (class 1259 OID 56087)
-- Name: sgc_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_roles (
    idrol integer NOT NULL,
    rolnom character varying(48) NOT NULL
);


ALTER TABLE public.sgc_roles OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 56090)
-- Name: sgc_roles_idrol_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_roles_idrol_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_roles_idrol_seq OWNER TO postgres;

--
-- TOC entry 3691 (class 0 OID 0)
-- Dependencies: 230
-- Name: sgc_roles_idrol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_roles_idrol_seq OWNED BY public.sgc_roles.idrol;


--
-- TOC entry 256 (class 1259 OID 74897)
-- Name: sgc_seguimiento_caso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_seguimiento_caso (
    idsegcas integer NOT NULL,
    idcaso integer NOT NULL,
    idestllam integer NOT NULL,
    segcoment character varying(512) NOT NULL,
    segfec date NOT NULL,
    idusuopr integer NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.sgc_seguimiento_caso OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 74896)
-- Name: sgc_seguimiento_caso_idsegcas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_seguimiento_caso_idsegcas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_seguimiento_caso_idsegcas_seq OWNER TO postgres;

--
-- TOC entry 3692 (class 0 OID 0)
-- Dependencies: 255
-- Name: sgc_seguimiento_caso_idsegcas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_seguimiento_caso_idsegcas_seq OWNED BY public.sgc_seguimiento_caso.idsegcas;


--
-- TOC entry 240 (class 1259 OID 74659)
-- Name: sgc_tipo_beneficiarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_tipo_beneficiarios (
    tipo_beneficiario_id integer NOT NULL,
    tipo_beneficiario_nombre text NOT NULL,
    tipo_beneficiario_borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sgc_tipo_beneficiarios OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 74658)
-- Name: sgc_tipo_beneficiarios_tipo_beneficiario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_tipo_beneficiarios_tipo_beneficiario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_tipo_beneficiarios_tipo_beneficiario_id_seq OWNER TO postgres;

--
-- TOC entry 3693 (class 0 OID 0)
-- Dependencies: 239
-- Name: sgc_tipo_beneficiarios_tipo_beneficiario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_tipo_beneficiarios_tipo_beneficiario_id_seq OWNED BY public.sgc_tipo_beneficiarios.tipo_beneficiario_id;


--
-- TOC entry 242 (class 1259 OID 74689)
-- Name: sgc_tipoatenciondetalle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_tipoatenciondetalle (
    tipo_atend_id integer NOT NULL,
    tipo_atend_nombre text NOT NULL,
    tipo_aten_id integer NOT NULL,
    tipo_atend_borrado boolean DEFAULT false
);


ALTER TABLE public.sgc_tipoatenciondetalle OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 74688)
-- Name: sgc_tipoatencioindetalle_tipo_atend_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_tipoatencioindetalle_tipo_atend_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_tipoatencioindetalle_tipo_atend_id_seq OWNER TO postgres;

--
-- TOC entry 3694 (class 0 OID 0)
-- Dependencies: 241
-- Name: sgc_tipoatencioindetalle_tipo_atend_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_tipoatencioindetalle_tipo_atend_id_seq OWNED BY public.sgc_tipoatenciondetalle.tipo_atend_id;


--
-- TOC entry 231 (class 1259 OID 56107)
-- Name: sgc_tipoatencion_usu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_tipoatencion_usu (
    tipo_aten_id integer NOT NULL,
    tipo_aten_nombre text NOT NULL,
    tipo_aten_borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.sgc_tipoatencion_usu OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 56113)
-- Name: sgc_tipoatencion_usu_tipo_anten_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_tipoatencion_usu_tipo_anten_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_tipoatencion_usu_tipo_anten_id_seq OWNER TO postgres;

--
-- TOC entry 3695 (class 0 OID 0)
-- Dependencies: 232
-- Name: sgc_tipoatencion_usu_tipo_anten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_tipoatencion_usu_tipo_anten_id_seq OWNED BY public.sgc_tipoatencion_usu.tipo_aten_id;


--
-- TOC entry 233 (class 1259 OID 56121)
-- Name: sgc_usuario_operador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_usuario_operador (
    idusuopr integer NOT NULL,
    usuopnom character varying(48) NOT NULL,
    usuopape character varying(48) NOT NULL,
    usuoppass character varying(255) NOT NULL,
    usuopemail character varying(48) NOT NULL,
    idrol integer NOT NULL,
    usuopborrado boolean DEFAULT false,
    usercargo text
);


ALTER TABLE public.sgc_usuario_operador OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 56127)
-- Name: sgc_usuario_operador_idusuopr_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_usuario_operador_idusuopr_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_usuario_operador_idusuopr_seq OWNER TO postgres;

--
-- TOC entry 3696 (class 0 OID 0)
-- Dependencies: 234
-- Name: sgc_usuario_operador_idusuopr_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_usuario_operador_idusuopr_seq OWNED BY public.sgc_usuario_operador.idusuopr;


--
-- TOC entry 235 (class 1259 OID 56128)
-- Name: sgc_usuario_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sgc_usuario_token (
    id integer NOT NULL,
    id_usuario integer NOT NULL,
    token text
);


ALTER TABLE public.sgc_usuario_token OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 56133)
-- Name: sgc_usuario_token_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sgc_usuario_token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sgc_usuario_token_id_seq OWNER TO postgres;

--
-- TOC entry 3697 (class 0 OID 0)
-- Dependencies: 236
-- Name: sgc_usuario_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sgc_usuario_token_id_seq OWNED BY public.sgc_usuario_token.id;


--
-- TOC entry 237 (class 1259 OID 56134)
-- Name: sta_usuarios_visitas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sta_usuarios_visitas (
    id integer NOT NULL,
    user_requests_ip character varying(150),
    fecha date DEFAULT CURRENT_DATE,
    hora character(11)
);


ALTER TABLE public.sta_usuarios_visitas OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 56138)
-- Name: sta_usuarios_visitas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sta_usuarios_visitas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.sta_usuarios_visitas_id_seq OWNER TO postgres;

--
-- TOC entry 3698 (class 0 OID 0)
-- Dependencies: 238
-- Name: sta_usuarios_visitas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sta_usuarios_visitas_id_seq OWNED BY public.sta_usuarios_visitas.id;


--
-- TOC entry 3395 (class 2604 OID 74815)
-- Name: sgc_auditoria_sistema audi_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_auditoria_sistema ALTER COLUMN audi_id SET DEFAULT nextval('public.sgc_auditoria_sistema_audi_id_seq'::regclass);


--
-- TOC entry 3397 (class 2604 OID 74825)
-- Name: sgc_casos idcaso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos ALTER COLUMN idcaso SET DEFAULT nextval('public.sgc_casos_idcaso_seq'::regclass);


--
-- TOC entry 3401 (class 2604 OID 74867)
-- Name: sgc_casos_denuncias denu_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos_denuncias ALTER COLUMN denu_id SET DEFAULT nextval('public.sgc_casos_denuncias_denu_id_seq'::regclass);


--
-- TOC entry 3407 (class 2604 OID 74881)
-- Name: sgc_casos_remitidos casos_re_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos_remitidos ALTER COLUMN casos_re_id SET DEFAULT nextval('public.sgc_casos_remitidos_casos_re_id_seq'::regclass);


--
-- TOC entry 3414 (class 2604 OID 74926)
-- Name: sgc_direcciones_administrativas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_direcciones_administrativas ALTER COLUMN id SET DEFAULT nextval('public.sgc_direcciones_administrativas_id_seq'::regclass);


--
-- TOC entry 3411 (class 2604 OID 74891)
-- Name: sgc_documentos_casos docu_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_documentos_casos ALTER COLUMN docu_id SET DEFAULT nextval('public.sgc_documentos_casos_docu_id_seq'::regclass);


--
-- TOC entry 3367 (class 2604 OID 56145)
-- Name: sgc_ente_asdcrito ente_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_ente_asdcrito ALTER COLUMN ente_id SET DEFAULT nextval('public.sgc_ente_asdcrito_ente_id_seq'::regclass);


--
-- TOC entry 3369 (class 2604 OID 56146)
-- Name: sgc_estados estadoid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_estados ALTER COLUMN estadoid SET DEFAULT nextval('public.sgc_estados_estadoid_seq'::regclass);


--
-- TOC entry 3393 (class 2604 OID 74738)
-- Name: sgc_estatus idest; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_estatus ALTER COLUMN idest SET DEFAULT nextval('public.sgc_estatus_idest_seq'::regclass);


--
-- TOC entry 3371 (class 2604 OID 56148)
-- Name: sgc_estatus_llamadas idestllam; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_estatus_llamadas ALTER COLUMN idestllam SET DEFAULT nextval('public.sgc_estatus_llamadas_idestllam_seq'::regclass);


--
-- TOC entry 3372 (class 2604 OID 56149)
-- Name: sgc_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_migrations ALTER COLUMN id SET DEFAULT nextval('public.sgc_migrations_id_seq'::regclass);


--
-- TOC entry 3373 (class 2604 OID 56150)
-- Name: sgc_municipio municipioid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_municipio ALTER COLUMN municipioid SET DEFAULT nextval('public.sgc_municipio_municipioid_seq'::regclass);


--
-- TOC entry 3374 (class 2604 OID 56151)
-- Name: sgc_oficinas idofi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_oficinas ALTER COLUMN idofi SET DEFAULT nextval('public.sgc_oficinas_idofi_seq'::regclass);


--
-- TOC entry 3375 (class 2604 OID 56152)
-- Name: sgc_paises paisid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_paises ALTER COLUMN paisid SET DEFAULT nextval('public.sgc_paises_paisid_seq'::regclass);


--
-- TOC entry 3376 (class 2604 OID 56153)
-- Name: sgc_parroquias parroquiaid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_parroquias ALTER COLUMN parroquiaid SET DEFAULT nextval('public.sgc_parroquias_parroquiaid_seq'::regclass);


--
-- TOC entry 3377 (class 2604 OID 56154)
-- Name: sgc_red_social red_s_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_red_social ALTER COLUMN red_s_id SET DEFAULT nextval('public.sgc_red_social_idrrss_seq'::regclass);


--
-- TOC entry 3379 (class 2604 OID 56155)
-- Name: sgc_registro_cgr id_cgr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_registro_cgr ALTER COLUMN id_cgr SET DEFAULT nextval('public.sgc_registro_cgr_id_cgr_seq'::regclass);


--
-- TOC entry 3381 (class 2604 OID 56156)
-- Name: sgc_roles idrol; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_roles ALTER COLUMN idrol SET DEFAULT nextval('public.sgc_roles_idrol_seq'::regclass);


--
-- TOC entry 3412 (class 2604 OID 74900)
-- Name: sgc_seguimiento_caso idsegcas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_seguimiento_caso ALTER COLUMN idsegcas SET DEFAULT nextval('public.sgc_seguimiento_caso_idsegcas_seq'::regclass);


--
-- TOC entry 3389 (class 2604 OID 74662)
-- Name: sgc_tipo_beneficiarios tipo_beneficiario_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_tipo_beneficiarios ALTER COLUMN tipo_beneficiario_id SET DEFAULT nextval('public.sgc_tipo_beneficiarios_tipo_beneficiario_id_seq'::regclass);


--
-- TOC entry 3382 (class 2604 OID 56160)
-- Name: sgc_tipoatencion_usu tipo_aten_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_tipoatencion_usu ALTER COLUMN tipo_aten_id SET DEFAULT nextval('public.sgc_tipoatencion_usu_tipo_anten_id_seq'::regclass);


--
-- TOC entry 3391 (class 2604 OID 74692)
-- Name: sgc_tipoatenciondetalle tipo_atend_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_tipoatenciondetalle ALTER COLUMN tipo_atend_id SET DEFAULT nextval('public.sgc_tipoatencioindetalle_tipo_atend_id_seq'::regclass);


--
-- TOC entry 3384 (class 2604 OID 56162)
-- Name: sgc_usuario_operador idusuopr; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_usuario_operador ALTER COLUMN idusuopr SET DEFAULT nextval('public.sgc_usuario_operador_idusuopr_seq'::regclass);


--
-- TOC entry 3386 (class 2604 OID 56163)
-- Name: sgc_usuario_token id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_usuario_token ALTER COLUMN id SET DEFAULT nextval('public.sgc_usuario_token_id_seq'::regclass);


--
-- TOC entry 3387 (class 2604 OID 56164)
-- Name: sta_usuarios_visitas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sta_usuarios_visitas ALTER COLUMN id SET DEFAULT nextval('public.sta_usuarios_visitas_id_seq'::regclass);


--
-- TOC entry 3655 (class 0 OID 74812)
-- Dependencies: 246
-- Data for Name: sgc_auditoria_sistema; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3657 (class 0 OID 74822)
-- Dependencies: 248
-- Data for Name: sgc_casos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3659 (class 0 OID 74864)
-- Dependencies: 250
-- Data for Name: sgc_casos_denuncias; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3661 (class 0 OID 74878)
-- Dependencies: 252
-- Data for Name: sgc_casos_remitidos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3667 (class 0 OID 74923)
-- Dependencies: 258
-- Data for Name: sgc_direcciones_administrativas; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3663 (class 0 OID 74888)
-- Dependencies: 254
-- Data for Name: sgc_documentos_casos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3618 (class 0 OID 56035)
-- Dependencies: 209
-- Data for Name: sgc_ente_asdcrito; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_ente_asdcrito VALUES (1, 'COMERCIO', false);
INSERT INTO public.sgc_ente_asdcrito VALUES (2, 'ALMACENAMIENTO CARACAS', false);


--
-- TOC entry 3620 (class 0 OID 56042)
-- Dependencies: 211
-- Data for Name: sgc_estados; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_estados VALUES (1, 'Amazonas', 1, false);
INSERT INTO public.sgc_estados VALUES (2, 'Anzoátegui', 1, false);
INSERT INTO public.sgc_estados VALUES (3, 'Apure', 1, false);
INSERT INTO public.sgc_estados VALUES (4, 'Aragua', 1, false);
INSERT INTO public.sgc_estados VALUES (5, 'Barinas', 1, false);
INSERT INTO public.sgc_estados VALUES (6, 'Bolívar', 1, false);
INSERT INTO public.sgc_estados VALUES (7, 'Carabobo', 1, false);
INSERT INTO public.sgc_estados VALUES (8, 'Cojedes', 1, false);
INSERT INTO public.sgc_estados VALUES (9, 'Delta Amacuro', 1, false);
INSERT INTO public.sgc_estados VALUES (10, 'Falcón', 1, false);
INSERT INTO public.sgc_estados VALUES (11, 'Guárico', 1, false);
INSERT INTO public.sgc_estados VALUES (12, 'Lara', 1, false);
INSERT INTO public.sgc_estados VALUES (13, 'Mérida', 1, false);
INSERT INTO public.sgc_estados VALUES (14, 'Miranda', 1, false);
INSERT INTO public.sgc_estados VALUES (15, 'Monagas', 1, false);
INSERT INTO public.sgc_estados VALUES (16, 'Nueva Esparta', 1, false);
INSERT INTO public.sgc_estados VALUES (17, 'Portuguesa', 1, false);
INSERT INTO public.sgc_estados VALUES (18, 'Sucre', 1, false);
INSERT INTO public.sgc_estados VALUES (19, 'Táchira', 1, false);
INSERT INTO public.sgc_estados VALUES (20, 'Trujillo', 1, false);
INSERT INTO public.sgc_estados VALUES (21, 'La Guaira', 1, false);
INSERT INTO public.sgc_estados VALUES (22, 'Yaracuy', 1, false);
INSERT INTO public.sgc_estados VALUES (23, 'Zulia', 1, false);
INSERT INTO public.sgc_estados VALUES (24, 'Distrito Capital', 1, false);
INSERT INTO public.sgc_estados VALUES (25, 'Dependencias Federales', 1, false);


--
-- TOC entry 3653 (class 0 OID 74735)
-- Dependencies: 244
-- Data for Name: sgc_estatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_estatus VALUES (3, 'Aprobado', false);
INSERT INTO public.sgc_estatus VALUES (5, 'Cerrado', false);
INSERT INTO public.sgc_estatus VALUES (4, 'No Aprobado', false);
INSERT INTO public.sgc_estatus VALUES (2, 'En espera', false);
INSERT INTO public.sgc_estatus VALUES (1, 'Abierto', false);


--
-- TOC entry 3622 (class 0 OID 56051)
-- Dependencies: 213
-- Data for Name: sgc_estatus_llamadas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_estatus_llamadas VALUES (1, 'Por Llamar');
INSERT INTO public.sgc_estatus_llamadas VALUES (2, 'Atendido');
INSERT INTO public.sgc_estatus_llamadas VALUES (3, 'No Contesta');
INSERT INTO public.sgc_estatus_llamadas VALUES (4, 'Otro');


--
-- TOC entry 3624 (class 0 OID 56055)
-- Dependencies: 215
-- Data for Name: sgc_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_migrations VALUES (1, '2022-06-01-181158', 'App\Database\Migrations\TipoRequerimiento', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (2, '2022-06-01-182126', 'App\Database\Migrations\RedSocial', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (3, '2022-06-01-182332', 'App\Database\Migrations\EstatusLlamadas', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (4, '2022-06-01-182650', 'App\Database\Migrations\Paises', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (5, '2022-06-01-183401', 'App\Database\Migrations\Roles', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (6, '2022-06-01-184314', 'App\Database\Migrations\DireccionCorrespondiente', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (7, '2022-06-01-184552', 'App\Database\Migrations\Estatus', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (8, '2022-06-01-184938', 'App\Database\Migrations\TipoPropIntelec', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (9, '2022-06-01-185211', 'App\Database\Migrations\Estados', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (10, '2022-06-01-185450', 'App\Database\Migrations\Municipio', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (11, '2022-06-01-185804', 'App\Database\Migrations\Parroquias', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (12, '2022-06-01-190326', 'App\Database\Migrations\UsuarioOperador', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (13, '2022-06-01-190856', 'App\Database\Migrations\Casos', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (14, '2022-06-02-134416', 'App\Database\Migrations\SeguimientoCaso', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (15, '2022-06-02-140622', 'App\Database\Migrations\TipoPropCaso', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (16, '2022-06-20-195739', 'App\Database\Migrations\TipoRequerimientoUsuario', 'default', 'App', 1678123675, 1);
INSERT INTO public.sgc_migrations VALUES (17, '2022-10-29-131039', 'App\Database\Migrations\Oficinas', 'default', 'App', 1678123676, 1);


--
-- TOC entry 3626 (class 0 OID 56061)
-- Dependencies: 217
-- Data for Name: sgc_municipio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_municipio VALUES (1, 'Libertador', 24);
INSERT INTO public.sgc_municipio VALUES (2, 'Anaco', 2);
INSERT INTO public.sgc_municipio VALUES (3, 'Aragua', 2);
INSERT INTO public.sgc_municipio VALUES (4, 'Bolivar', 2);
INSERT INTO public.sgc_municipio VALUES (5, 'Bruzual', 2);
INSERT INTO public.sgc_municipio VALUES (6, 'Cajigal', 2);
INSERT INTO public.sgc_municipio VALUES (7, 'Freites', 2);
INSERT INTO public.sgc_municipio VALUES (8, 'Independencia', 2);
INSERT INTO public.sgc_municipio VALUES (9, 'Libertad', 2);
INSERT INTO public.sgc_municipio VALUES (10, 'Miranda', 2);
INSERT INTO public.sgc_municipio VALUES (11, 'Monagas', 2);
INSERT INTO public.sgc_municipio VALUES (12, 'Peñalver', 2);
INSERT INTO public.sgc_municipio VALUES (13, 'Simon Rodriguez', 2);
INSERT INTO public.sgc_municipio VALUES (14, 'Sotillo', 2);
INSERT INTO public.sgc_municipio VALUES (15, 'Guanipa', 2);
INSERT INTO public.sgc_municipio VALUES (16, 'Guanta', 2);
INSERT INTO public.sgc_municipio VALUES (17, 'Piritu', 2);
INSERT INTO public.sgc_municipio VALUES (18, 'M.L/Diego Bautista U', 2);
INSERT INTO public.sgc_municipio VALUES (19, 'Carvajal', 2);
INSERT INTO public.sgc_municipio VALUES (20, 'Santa Ana', 2);
INSERT INTO public.sgc_municipio VALUES (21, 'Mc Gregor', 2);
INSERT INTO public.sgc_municipio VALUES (22, 'S Juan Capistrano', 2);
INSERT INTO public.sgc_municipio VALUES (23, 'Achaguas', 3);
INSERT INTO public.sgc_municipio VALUES (24, 'Muñoz', 3);
INSERT INTO public.sgc_municipio VALUES (25, 'Paez', 3);
INSERT INTO public.sgc_municipio VALUES (26, 'Pedro Camejo', 3);
INSERT INTO public.sgc_municipio VALUES (27, 'Romulo Gallegos', 3);
INSERT INTO public.sgc_municipio VALUES (28, 'San Fernando', 3);
INSERT INTO public.sgc_municipio VALUES (29, 'Biruaca', 3);
INSERT INTO public.sgc_municipio VALUES (30, 'Girardot', 4);
INSERT INTO public.sgc_municipio VALUES (31, 'Santiago Mariño', 4);
INSERT INTO public.sgc_municipio VALUES (32, 'Jose Felix Rivas', 4);
INSERT INTO public.sgc_municipio VALUES (33, 'San Casimiro', 4);
INSERT INTO public.sgc_municipio VALUES (34, 'San Sebastian', 4);
INSERT INTO public.sgc_municipio VALUES (35, 'Sucre', 4);
INSERT INTO public.sgc_municipio VALUES (36, 'Urdaneta', 4);
INSERT INTO public.sgc_municipio VALUES (37, 'Zamora', 4);
INSERT INTO public.sgc_municipio VALUES (38, 'Libertador', 4);
INSERT INTO public.sgc_municipio VALUES (39, 'Jose Angel Lamas', 4);
INSERT INTO public.sgc_municipio VALUES (40, 'Bolivar', 4);
INSERT INTO public.sgc_municipio VALUES (41, 'Santos Michelena', 4);
INSERT INTO public.sgc_municipio VALUES (42, 'Mario B Iragorry', 4);
INSERT INTO public.sgc_municipio VALUES (43, 'Tovar', 4);
INSERT INTO public.sgc_municipio VALUES (44, 'Camatagua', 4);
INSERT INTO public.sgc_municipio VALUES (45, 'Jose R Revenga', 4);
INSERT INTO public.sgc_municipio VALUES (46, 'Francisco Linares A.', 4);
INSERT INTO public.sgc_municipio VALUES (47, 'M.Ocumare D La Costa', 4);
INSERT INTO public.sgc_municipio VALUES (48, 'Arismendi', 5);
INSERT INTO public.sgc_municipio VALUES (49, 'Barinas', 5);
INSERT INTO public.sgc_municipio VALUES (50, 'Bolivar', 5);
INSERT INTO public.sgc_municipio VALUES (51, 'Ezequiel Zamora', 5);
INSERT INTO public.sgc_municipio VALUES (52, 'Obispos', 5);
INSERT INTO public.sgc_municipio VALUES (53, 'Pedraza', 5);
INSERT INTO public.sgc_municipio VALUES (54, 'Rojas', 5);
INSERT INTO public.sgc_municipio VALUES (55, 'Sosa', 5);
INSERT INTO public.sgc_municipio VALUES (56, 'Alberto Arvelo T', 5);
INSERT INTO public.sgc_municipio VALUES (57, 'A Jose De Sucre', 5);
INSERT INTO public.sgc_municipio VALUES (58, 'Cruz Paredes', 5);
INSERT INTO public.sgc_municipio VALUES (59, 'Andres E. Blanco', 5);
INSERT INTO public.sgc_municipio VALUES (60, 'Caroni', 6);
INSERT INTO public.sgc_municipio VALUES (61, 'Cedeño', 6);
INSERT INTO public.sgc_municipio VALUES (62, 'Heres', 6);
INSERT INTO public.sgc_municipio VALUES (63, 'Piar', 6);
INSERT INTO public.sgc_municipio VALUES (64, 'Roscio', 6);
INSERT INTO public.sgc_municipio VALUES (65, 'Sucre', 6);
INSERT INTO public.sgc_municipio VALUES (66, 'Sifontes', 6);
INSERT INTO public.sgc_municipio VALUES (67, 'Raul Leoni', 6);
INSERT INTO public.sgc_municipio VALUES (68, 'Gran Sabana', 6);
INSERT INTO public.sgc_municipio VALUES (69, 'El Callao', 6);
INSERT INTO public.sgc_municipio VALUES (70, 'Padre Pedro Chien', 6);
INSERT INTO public.sgc_municipio VALUES (71, 'Bejuma', 7);
INSERT INTO public.sgc_municipio VALUES (72, 'Carlos Arvelo', 7);
INSERT INTO public.sgc_municipio VALUES (73, 'Diego Ibarra', 7);
INSERT INTO public.sgc_municipio VALUES (74, 'Guacara', 7);
INSERT INTO public.sgc_municipio VALUES (75, 'Montalban', 7);
INSERT INTO public.sgc_municipio VALUES (76, 'Juan Jose Mora', 7);
INSERT INTO public.sgc_municipio VALUES (77, 'Puerto Cabello', 7);
INSERT INTO public.sgc_municipio VALUES (78, 'San Joaquin', 7);
INSERT INTO public.sgc_municipio VALUES (79, 'Valencia', 7);
INSERT INTO public.sgc_municipio VALUES (80, 'Miranda', 7);
INSERT INTO public.sgc_municipio VALUES (81, 'Los Guayos', 7);
INSERT INTO public.sgc_municipio VALUES (82, 'Naguanagua', 7);
INSERT INTO public.sgc_municipio VALUES (83, 'San Diego', 7);
INSERT INTO public.sgc_municipio VALUES (84, 'Libertador', 7);
INSERT INTO public.sgc_municipio VALUES (85, 'Anzoategui', 8);
INSERT INTO public.sgc_municipio VALUES (86, 'Falcon', 8);
INSERT INTO public.sgc_municipio VALUES (87, 'Girardot', 8);
INSERT INTO public.sgc_municipio VALUES (88, 'Mp Pao Sn J Bautista', 8);
INSERT INTO public.sgc_municipio VALUES (89, 'Ricaurte', 8);
INSERT INTO public.sgc_municipio VALUES (90, 'San Carlos', 8);
INSERT INTO public.sgc_municipio VALUES (91, 'Tinaco', 8);
INSERT INTO public.sgc_municipio VALUES (92, 'Lima Blanco', 8);
INSERT INTO public.sgc_municipio VALUES (93, 'Romulo Gallegos', 8);
INSERT INTO public.sgc_municipio VALUES (94, 'Acosta', 10);
INSERT INTO public.sgc_municipio VALUES (95, 'Bolivar', 10);
INSERT INTO public.sgc_municipio VALUES (96, 'Buchivacoa', 10);
INSERT INTO public.sgc_municipio VALUES (97, 'Carirubana', 10);
INSERT INTO public.sgc_municipio VALUES (98, 'Colina', 10);
INSERT INTO public.sgc_municipio VALUES (99, 'Democracia', 10);
INSERT INTO public.sgc_municipio VALUES (100, 'Falcon', 10);
INSERT INTO public.sgc_municipio VALUES (101, 'Federacion', 10);
INSERT INTO public.sgc_municipio VALUES (102, 'Mauroa', 10);
INSERT INTO public.sgc_municipio VALUES (103, 'Miranda', 10);
INSERT INTO public.sgc_municipio VALUES (104, 'Petit', 10);
INSERT INTO public.sgc_municipio VALUES (105, 'Silva', 10);
INSERT INTO public.sgc_municipio VALUES (106, 'Zamora', 10);
INSERT INTO public.sgc_municipio VALUES (107, 'Dabajuro', 10);
INSERT INTO public.sgc_municipio VALUES (108, 'Mons. Iturriza', 10);
INSERT INTO public.sgc_municipio VALUES (109, 'Los Taques', 10);
INSERT INTO public.sgc_municipio VALUES (110, 'Piritu', 10);
INSERT INTO public.sgc_municipio VALUES (111, 'Union', 10);
INSERT INTO public.sgc_municipio VALUES (112, 'San Francisco', 10);
INSERT INTO public.sgc_municipio VALUES (113, 'Jacura', 10);
INSERT INTO public.sgc_municipio VALUES (114, 'Cacique Manaure', 10);
INSERT INTO public.sgc_municipio VALUES (115, 'Palma Sola', 10);
INSERT INTO public.sgc_municipio VALUES (116, 'Sucre', 10);
INSERT INTO public.sgc_municipio VALUES (117, 'Urumaco', 10);
INSERT INTO public.sgc_municipio VALUES (118, 'Tocopero', 10);
INSERT INTO public.sgc_municipio VALUES (119, 'Infante', 11);
INSERT INTO public.sgc_municipio VALUES (120, 'Mellado', 11);
INSERT INTO public.sgc_municipio VALUES (121, 'Miranda', 11);
INSERT INTO public.sgc_municipio VALUES (122, 'Monagas', 11);
INSERT INTO public.sgc_municipio VALUES (123, 'Ribas', 11);
INSERT INTO public.sgc_municipio VALUES (124, 'Roscio', 11);
INSERT INTO public.sgc_municipio VALUES (125, 'Zaraza', 11);
INSERT INTO public.sgc_municipio VALUES (126, 'Camaguan', 11);
INSERT INTO public.sgc_municipio VALUES (127, 'S Jose De Guaribe', 11);
INSERT INTO public.sgc_municipio VALUES (128, 'Las Mercedes', 11);
INSERT INTO public.sgc_municipio VALUES (129, 'El Socorro', 11);
INSERT INTO public.sgc_municipio VALUES (130, 'Ortiz', 11);
INSERT INTO public.sgc_municipio VALUES (131, 'S Maria De Ipire', 11);
INSERT INTO public.sgc_municipio VALUES (132, 'Chaguaramas', 11);
INSERT INTO public.sgc_municipio VALUES (133, 'San Geronimo De G', 11);
INSERT INTO public.sgc_municipio VALUES (134, 'Crespo', 12);
INSERT INTO public.sgc_municipio VALUES (135, 'Iribarren', 12);
INSERT INTO public.sgc_municipio VALUES (136, 'Jimenez', 12);
INSERT INTO public.sgc_municipio VALUES (137, 'Moran', 12);
INSERT INTO public.sgc_municipio VALUES (138, 'Palavecino', 12);
INSERT INTO public.sgc_municipio VALUES (139, 'Torres', 12);
INSERT INTO public.sgc_municipio VALUES (140, 'Urdaneta', 12);
INSERT INTO public.sgc_municipio VALUES (141, 'Andres E Blanco', 12);
INSERT INTO public.sgc_municipio VALUES (142, 'Simon Planas', 12);
INSERT INTO public.sgc_municipio VALUES (143, 'Alberto Adriani', 13);
INSERT INTO public.sgc_municipio VALUES (144, 'Andres Bello', 13);
INSERT INTO public.sgc_municipio VALUES (145, 'Arzobispo Chacon', 13);
INSERT INTO public.sgc_municipio VALUES (146, 'Campo Elias', 13);
INSERT INTO public.sgc_municipio VALUES (147, 'Guaraque', 13);
INSERT INTO public.sgc_municipio VALUES (148, 'Julio Cesar Salas', 13);
INSERT INTO public.sgc_municipio VALUES (149, 'Justo Briceño', 13);
INSERT INTO public.sgc_municipio VALUES (150, 'Libertador', 13);
INSERT INTO public.sgc_municipio VALUES (151, 'Santos Marquina', 13);
INSERT INTO public.sgc_municipio VALUES (152, 'Miranda', 13);
INSERT INTO public.sgc_municipio VALUES (153, 'Antonio Pinto S.', 13);
INSERT INTO public.sgc_municipio VALUES (154, 'Ob. Ramos De Lora', 13);
INSERT INTO public.sgc_municipio VALUES (155, 'Caracciolo Parra', 13);
INSERT INTO public.sgc_municipio VALUES (156, 'Cardenal Quintero', 13);
INSERT INTO public.sgc_municipio VALUES (157, 'Pueblo Llano', 13);
INSERT INTO public.sgc_municipio VALUES (158, 'Rangel', 13);
INSERT INTO public.sgc_municipio VALUES (159, 'Rivas Davila', 13);
INSERT INTO public.sgc_municipio VALUES (160, 'Sucre', 13);
INSERT INTO public.sgc_municipio VALUES (161, 'Tovar', 13);
INSERT INTO public.sgc_municipio VALUES (162, 'Tulio F Cordero', 13);
INSERT INTO public.sgc_municipio VALUES (163, 'Padre Noguera', 13);
INSERT INTO public.sgc_municipio VALUES (164, 'Aricagua', 13);
INSERT INTO public.sgc_municipio VALUES (165, 'Zea', 13);
INSERT INTO public.sgc_municipio VALUES (166, 'Acevedo', 14);
INSERT INTO public.sgc_municipio VALUES (167, 'Brion', 14);
INSERT INTO public.sgc_municipio VALUES (168, 'Guaicaipuro', 14);
INSERT INTO public.sgc_municipio VALUES (169, 'Independencia', 14);
INSERT INTO public.sgc_municipio VALUES (170, 'Lander', 14);
INSERT INTO public.sgc_municipio VALUES (171, 'Paez', 14);
INSERT INTO public.sgc_municipio VALUES (172, 'Paz Castillo', 14);
INSERT INTO public.sgc_municipio VALUES (173, 'Plaza', 14);
INSERT INTO public.sgc_municipio VALUES (174, 'Sucre', 14);
INSERT INTO public.sgc_municipio VALUES (175, 'Urdaneta', 14);
INSERT INTO public.sgc_municipio VALUES (176, 'Zamora', 14);
INSERT INTO public.sgc_municipio VALUES (177, 'Cristobal Rojas', 14);
INSERT INTO public.sgc_municipio VALUES (178, 'Los Salias', 14);
INSERT INTO public.sgc_municipio VALUES (179, 'Andres Bello', 14);
INSERT INTO public.sgc_municipio VALUES (180, 'Simon Bolivar', 14);
INSERT INTO public.sgc_municipio VALUES (181, 'Baruta', 14);
INSERT INTO public.sgc_municipio VALUES (182, 'Carrizal', 14);
INSERT INTO public.sgc_municipio VALUES (183, 'Chacao', 14);
INSERT INTO public.sgc_municipio VALUES (184, 'El Hatillo', 14);
INSERT INTO public.sgc_municipio VALUES (185, 'Buroz', 14);
INSERT INTO public.sgc_municipio VALUES (186, 'Pedro Gual', 14);
INSERT INTO public.sgc_municipio VALUES (187, 'Acosta', 15);
INSERT INTO public.sgc_municipio VALUES (188, 'Bolivar', 15);
INSERT INTO public.sgc_municipio VALUES (189, 'Caripe', 15);
INSERT INTO public.sgc_municipio VALUES (190, 'Cedeño', 15);
INSERT INTO public.sgc_municipio VALUES (191, 'Ezequiel Zamora', 15);
INSERT INTO public.sgc_municipio VALUES (192, 'Libertador', 15);
INSERT INTO public.sgc_municipio VALUES (193, 'Maturin', 15);
INSERT INTO public.sgc_municipio VALUES (194, 'Piar', 15);
INSERT INTO public.sgc_municipio VALUES (195, 'Punceres', 15);
INSERT INTO public.sgc_municipio VALUES (196, 'Sotillo', 15);
INSERT INTO public.sgc_municipio VALUES (197, 'Aguasay', 15);
INSERT INTO public.sgc_municipio VALUES (198, 'Santa Barbara', 15);
INSERT INTO public.sgc_municipio VALUES (199, 'Uracoa', 15);
INSERT INTO public.sgc_municipio VALUES (200, 'Arismendi', 16);
INSERT INTO public.sgc_municipio VALUES (201, 'Diaz', 16);
INSERT INTO public.sgc_municipio VALUES (202, 'Gomez', 16);
INSERT INTO public.sgc_municipio VALUES (203, 'Maneiro', 16);
INSERT INTO public.sgc_municipio VALUES (204, 'Marcano', 16);
INSERT INTO public.sgc_municipio VALUES (205, 'Mariño', 16);
INSERT INTO public.sgc_municipio VALUES (206, 'Penin. De Macanao', 16);
INSERT INTO public.sgc_municipio VALUES (207, 'Villalba(I.Coche)', 16);
INSERT INTO public.sgc_municipio VALUES (208, 'Tubores', 16);
INSERT INTO public.sgc_municipio VALUES (209, 'Antolin Del Campo', 16);
INSERT INTO public.sgc_municipio VALUES (210, 'Garcia', 16);
INSERT INTO public.sgc_municipio VALUES (211, 'Araure', 17);
INSERT INTO public.sgc_municipio VALUES (212, 'Esteller', 17);
INSERT INTO public.sgc_municipio VALUES (213, 'Guanare', 17);
INSERT INTO public.sgc_municipio VALUES (214, 'Guanarito', 17);
INSERT INTO public.sgc_municipio VALUES (215, 'Ospino', 17);
INSERT INTO public.sgc_municipio VALUES (216, 'Paez', 17);
INSERT INTO public.sgc_municipio VALUES (217, 'Sucre', 17);
INSERT INTO public.sgc_municipio VALUES (218, 'Turen', 17);
INSERT INTO public.sgc_municipio VALUES (219, 'M.Jose V De Unda', 17);
INSERT INTO public.sgc_municipio VALUES (220, 'Agua Blanca', 17);
INSERT INTO public.sgc_municipio VALUES (221, 'Papelon', 17);
INSERT INTO public.sgc_municipio VALUES (222, 'Genaro Boconoito', 17);
INSERT INTO public.sgc_municipio VALUES (223, 'S Rafael De Onoto', 17);
INSERT INTO public.sgc_municipio VALUES (224, 'Santa Rosalia', 17);
INSERT INTO public.sgc_municipio VALUES (225, 'Arismendi', 18);
INSERT INTO public.sgc_municipio VALUES (226, 'Benitez', 18);
INSERT INTO public.sgc_municipio VALUES (227, 'Bermudez', 18);
INSERT INTO public.sgc_municipio VALUES (228, 'Cajigal', 18);
INSERT INTO public.sgc_municipio VALUES (229, 'Mariño', 18);
INSERT INTO public.sgc_municipio VALUES (230, 'Mejia', 18);
INSERT INTO public.sgc_municipio VALUES (231, 'Montes', 18);
INSERT INTO public.sgc_municipio VALUES (232, 'Ribero', 18);
INSERT INTO public.sgc_municipio VALUES (233, 'Sucre', 18);
INSERT INTO public.sgc_municipio VALUES (234, 'Valdez', 18);
INSERT INTO public.sgc_municipio VALUES (235, 'Andres E Blanco', 18);
INSERT INTO public.sgc_municipio VALUES (236, 'Libertador', 18);
INSERT INTO public.sgc_municipio VALUES (237, 'Andres Mata', 18);
INSERT INTO public.sgc_municipio VALUES (238, 'Bolivar', 18);
INSERT INTO public.sgc_municipio VALUES (239, 'Cruz S Acosta', 18);
INSERT INTO public.sgc_municipio VALUES (240, 'Ayacucho', 19);
INSERT INTO public.sgc_municipio VALUES (241, 'Bolivar', 19);
INSERT INTO public.sgc_municipio VALUES (242, 'Independencia', 19);
INSERT INTO public.sgc_municipio VALUES (243, 'Cardenas', 19);
INSERT INTO public.sgc_municipio VALUES (244, 'Jauregui', 19);
INSERT INTO public.sgc_municipio VALUES (245, 'Junin', 19);
INSERT INTO public.sgc_municipio VALUES (246, 'Lobatera', 19);
INSERT INTO public.sgc_municipio VALUES (247, 'San Cristobal', 19);
INSERT INTO public.sgc_municipio VALUES (248, 'Uribante', 19);
INSERT INTO public.sgc_municipio VALUES (249, 'Cordoba', 19);
INSERT INTO public.sgc_municipio VALUES (250, 'Garcia De Hevia', 19);
INSERT INTO public.sgc_municipio VALUES (251, 'Guasimos', 19);
INSERT INTO public.sgc_municipio VALUES (252, 'Michelena', 19);
INSERT INTO public.sgc_municipio VALUES (253, 'Libertador', 19);
INSERT INTO public.sgc_municipio VALUES (254, 'Panamericano', 19);
INSERT INTO public.sgc_municipio VALUES (255, 'Pedro Maria Ureña', 19);
INSERT INTO public.sgc_municipio VALUES (256, 'Sucre', 19);
INSERT INTO public.sgc_municipio VALUES (257, 'Andres Bello', 19);
INSERT INTO public.sgc_municipio VALUES (258, 'Fernandez Feo', 19);
INSERT INTO public.sgc_municipio VALUES (259, 'Libertad', 19);
INSERT INTO public.sgc_municipio VALUES (260, 'Samuel Maldonado', 19);
INSERT INTO public.sgc_municipio VALUES (261, 'Seboruco', 19);
INSERT INTO public.sgc_municipio VALUES (262, 'Antonio Romulo C', 19);
INSERT INTO public.sgc_municipio VALUES (263, 'Fco De Miranda', 19);
INSERT INTO public.sgc_municipio VALUES (264, 'Jose Maria Vargas', 19);
INSERT INTO public.sgc_municipio VALUES (265, 'Rafael Urdaneta', 19);
INSERT INTO public.sgc_municipio VALUES (266, 'Simon Rodriguez', 19);
INSERT INTO public.sgc_municipio VALUES (267, 'Torbes', 19);
INSERT INTO public.sgc_municipio VALUES (268, 'San Judas Tadeo', 19);
INSERT INTO public.sgc_municipio VALUES (269, 'Rafael Rangel', 20);
INSERT INTO public.sgc_municipio VALUES (270, 'Bocono', 20);
INSERT INTO public.sgc_municipio VALUES (271, 'Carache', 20);
INSERT INTO public.sgc_municipio VALUES (272, 'Escuque', 20);
INSERT INTO public.sgc_municipio VALUES (273, 'Trujillo', 20);
INSERT INTO public.sgc_municipio VALUES (274, 'Urdaneta', 20);
INSERT INTO public.sgc_municipio VALUES (275, 'Valera', 20);
INSERT INTO public.sgc_municipio VALUES (276, 'Candelaria', 20);
INSERT INTO public.sgc_municipio VALUES (277, 'Miranda', 20);
INSERT INTO public.sgc_municipio VALUES (278, 'Monte Carmelo', 20);
INSERT INTO public.sgc_municipio VALUES (279, 'Motatan', 20);
INSERT INTO public.sgc_municipio VALUES (280, 'Pampan', 20);
INSERT INTO public.sgc_municipio VALUES (281, 'S Rafael Carvajal', 20);
INSERT INTO public.sgc_municipio VALUES (282, 'Sucre', 20);
INSERT INTO public.sgc_municipio VALUES (283, 'Andres Bello', 20);
INSERT INTO public.sgc_municipio VALUES (284, 'Bolivar', 20);
INSERT INTO public.sgc_municipio VALUES (285, 'Jose F M Cañizal', 20);
INSERT INTO public.sgc_municipio VALUES (286, 'Juan V Campo Eli', 20);
INSERT INTO public.sgc_municipio VALUES (287, 'La Ceiba', 20);
INSERT INTO public.sgc_municipio VALUES (288, 'Pampanito', 20);
INSERT INTO public.sgc_municipio VALUES (289, 'Bolivar', 22);
INSERT INTO public.sgc_municipio VALUES (290, 'Bruzual', 22);
INSERT INTO public.sgc_municipio VALUES (291, 'Nirgua', 22);
INSERT INTO public.sgc_municipio VALUES (292, 'San Felipe', 22);
INSERT INTO public.sgc_municipio VALUES (293, 'Sucre', 22);
INSERT INTO public.sgc_municipio VALUES (294, 'Urachiche', 22);
INSERT INTO public.sgc_municipio VALUES (295, 'Peña', 22);
INSERT INTO public.sgc_municipio VALUES (296, 'Jose Antonio Paez', 22);
INSERT INTO public.sgc_municipio VALUES (297, 'La Trinidad', 22);
INSERT INTO public.sgc_municipio VALUES (298, 'Cocorote', 22);
INSERT INTO public.sgc_municipio VALUES (299, 'Independencia', 22);
INSERT INTO public.sgc_municipio VALUES (300, 'Aristides Bastid', 22);
INSERT INTO public.sgc_municipio VALUES (301, 'Manuel Monge', 22);
INSERT INTO public.sgc_municipio VALUES (302, 'Veroes', 22);
INSERT INTO public.sgc_municipio VALUES (303, 'Baralt', 23);
INSERT INTO public.sgc_municipio VALUES (304, 'Santa Rita', 23);
INSERT INTO public.sgc_municipio VALUES (305, 'Colon', 23);
INSERT INTO public.sgc_municipio VALUES (306, 'Mara', 23);
INSERT INTO public.sgc_municipio VALUES (307, 'Maracaibo', 23);
INSERT INTO public.sgc_municipio VALUES (308, 'Miranda', 23);
INSERT INTO public.sgc_municipio VALUES (309, 'Paez', 23);
INSERT INTO public.sgc_municipio VALUES (310, 'Machiques De P', 23);
INSERT INTO public.sgc_municipio VALUES (311, 'Sucre', 23);
INSERT INTO public.sgc_municipio VALUES (312, 'La Cañada De U.', 23);
INSERT INTO public.sgc_municipio VALUES (313, 'Lagunillas', 23);
INSERT INTO public.sgc_municipio VALUES (314, 'Catatumbo', 23);
INSERT INTO public.sgc_municipio VALUES (315, 'M/Rosario De Perija', 23);
INSERT INTO public.sgc_municipio VALUES (316, 'Cabimas', 23);
INSERT INTO public.sgc_municipio VALUES (317, 'Valmore Rodriguez', 23);
INSERT INTO public.sgc_municipio VALUES (318, 'Jesus E Lossada', 23);
INSERT INTO public.sgc_municipio VALUES (319, 'Almirante P', 23);
INSERT INTO public.sgc_municipio VALUES (320, 'San Francisco', 23);
INSERT INTO public.sgc_municipio VALUES (321, 'Jesus M Semprun', 23);
INSERT INTO public.sgc_municipio VALUES (322, 'Francisco J Pulg', 23);
INSERT INTO public.sgc_municipio VALUES (323, 'Simon Bolivar', 23);
INSERT INTO public.sgc_municipio VALUES (324, 'Atures', 1);
INSERT INTO public.sgc_municipio VALUES (325, 'Atabapo', 1);
INSERT INTO public.sgc_municipio VALUES (326, 'Maroa', 1);
INSERT INTO public.sgc_municipio VALUES (327, 'Rio Negro', 1);
INSERT INTO public.sgc_municipio VALUES (328, 'Autana', 1);
INSERT INTO public.sgc_municipio VALUES (329, 'Manapiare', 1);
INSERT INTO public.sgc_municipio VALUES (330, 'Alto Orinoco', 1);
INSERT INTO public.sgc_municipio VALUES (331, 'Tucupita', 9);
INSERT INTO public.sgc_municipio VALUES (332, 'Pedernales', 9);
INSERT INTO public.sgc_municipio VALUES (333, 'Antonio Diaz', 9);
INSERT INTO public.sgc_municipio VALUES (334, 'Casacoima', 9);
INSERT INTO public.sgc_municipio VALUES (335, 'Vargas', 21);


--
-- TOC entry 3628 (class 0 OID 56065)
-- Dependencies: 219
-- Data for Name: sgc_oficinas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_oficinas VALUES (1, 'Sala Situacional');
INSERT INTO public.sgc_oficinas VALUES (2, 'Coordinacion Regional');


--
-- TOC entry 3630 (class 0 OID 56069)
-- Dependencies: 221
-- Data for Name: sgc_paises; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_paises VALUES (1, 'Venezuela');


--
-- TOC entry 3632 (class 0 OID 56073)
-- Dependencies: 223
-- Data for Name: sgc_parroquias; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_parroquias VALUES (1, 'Altagracia', 1);
INSERT INTO public.sgc_parroquias VALUES (2, 'Candelaria', 1);
INSERT INTO public.sgc_parroquias VALUES (3, 'Catedral', 1);
INSERT INTO public.sgc_parroquias VALUES (4, 'La Pastora', 1);
INSERT INTO public.sgc_parroquias VALUES (5, 'San Agustin', 1);
INSERT INTO public.sgc_parroquias VALUES (6, 'San Jose', 1);
INSERT INTO public.sgc_parroquias VALUES (7, 'San Juan', 1);
INSERT INTO public.sgc_parroquias VALUES (8, 'Santa Rosalia', 1);
INSERT INTO public.sgc_parroquias VALUES (9, 'Santa Teresa', 1);
INSERT INTO public.sgc_parroquias VALUES (10, 'Sucre', 1);
INSERT INTO public.sgc_parroquias VALUES (11, '23 De Enero', 1);
INSERT INTO public.sgc_parroquias VALUES (12, 'Antimano', 1);
INSERT INTO public.sgc_parroquias VALUES (13, 'El Recreo', 1);
INSERT INTO public.sgc_parroquias VALUES (14, 'El Valle', 1);
INSERT INTO public.sgc_parroquias VALUES (15, 'La Vega', 1);
INSERT INTO public.sgc_parroquias VALUES (16, 'Macarao', 1);
INSERT INTO public.sgc_parroquias VALUES (17, 'Caricuao', 1);
INSERT INTO public.sgc_parroquias VALUES (18, 'El Junquito', 1);
INSERT INTO public.sgc_parroquias VALUES (19, 'Coche', 1);
INSERT INTO public.sgc_parroquias VALUES (20, 'San Pedro', 1);
INSERT INTO public.sgc_parroquias VALUES (21, 'San Bernardino', 1);
INSERT INTO public.sgc_parroquias VALUES (22, 'El Paraiso', 1);
INSERT INTO public.sgc_parroquias VALUES (23, 'Anaco', 2);
INSERT INTO public.sgc_parroquias VALUES (24, 'San Joaquin', 2);
INSERT INTO public.sgc_parroquias VALUES (25, 'Cm. Aragua De Barcelona', 3);
INSERT INTO public.sgc_parroquias VALUES (26, 'Cachipo', 3);
INSERT INTO public.sgc_parroquias VALUES (27, 'El Carmen', 4);
INSERT INTO public.sgc_parroquias VALUES (28, 'San Cristobal', 4);
INSERT INTO public.sgc_parroquias VALUES (29, 'Bergantin', 4);
INSERT INTO public.sgc_parroquias VALUES (30, 'Caigua', 4);
INSERT INTO public.sgc_parroquias VALUES (31, 'El Pilar', 4);
INSERT INTO public.sgc_parroquias VALUES (32, 'Naricual', 4);
INSERT INTO public.sgc_parroquias VALUES (33, 'Cm. Clarines', 5);
INSERT INTO public.sgc_parroquias VALUES (34, 'Guanape', 5);
INSERT INTO public.sgc_parroquias VALUES (35, 'Sabana De Uchire', 5);
INSERT INTO public.sgc_parroquias VALUES (36, 'Cm. Onoto', 6);
INSERT INTO public.sgc_parroquias VALUES (37, 'San Pablo', 6);
INSERT INTO public.sgc_parroquias VALUES (38, 'Cm. Cantaura', 7);
INSERT INTO public.sgc_parroquias VALUES (39, 'Libertador', 7);
INSERT INTO public.sgc_parroquias VALUES (40, 'Santa Rosa', 7);
INSERT INTO public.sgc_parroquias VALUES (41, 'Urica', 7);
INSERT INTO public.sgc_parroquias VALUES (42, 'Cm. Soledad', 8);
INSERT INTO public.sgc_parroquias VALUES (43, 'Mamo', 8);
INSERT INTO public.sgc_parroquias VALUES (44, 'Cm. San Mateo', 9);
INSERT INTO public.sgc_parroquias VALUES (45, 'El Carito', 9);
INSERT INTO public.sgc_parroquias VALUES (46, 'Santa Ines', 9);
INSERT INTO public.sgc_parroquias VALUES (47, 'Cm. Pariaguan', 10);
INSERT INTO public.sgc_parroquias VALUES (48, 'Atapirire', 10);
INSERT INTO public.sgc_parroquias VALUES (49, 'Boca Del Pao', 10);
INSERT INTO public.sgc_parroquias VALUES (50, 'El Pao', 10);
INSERT INTO public.sgc_parroquias VALUES (51, 'Cm. Mapire', 11);
INSERT INTO public.sgc_parroquias VALUES (52, 'Piar', 11);
INSERT INTO public.sgc_parroquias VALUES (53, 'Sn Diego De Cabrutica', 11);
INSERT INTO public.sgc_parroquias VALUES (54, 'Santa Clara', 11);
INSERT INTO public.sgc_parroquias VALUES (55, 'Uverito', 11);
INSERT INTO public.sgc_parroquias VALUES (56, 'Zuata', 11);
INSERT INTO public.sgc_parroquias VALUES (57, 'Cm. Puerto Piritu', 12);
INSERT INTO public.sgc_parroquias VALUES (58, 'San Miguel', 12);
INSERT INTO public.sgc_parroquias VALUES (59, 'Sucre', 12);
INSERT INTO public.sgc_parroquias VALUES (60, 'Cm. El Tigre', 13);
INSERT INTO public.sgc_parroquias VALUES (61, 'Pozuelos', 14);
INSERT INTO public.sgc_parroquias VALUES (62, 'Cm Pto. La Cruz', 14);
INSERT INTO public.sgc_parroquias VALUES (63, 'Cm. San Jose De Guanipa', 15);
INSERT INTO public.sgc_parroquias VALUES (64, 'Guanta', 16);
INSERT INTO public.sgc_parroquias VALUES (65, 'Chorreron', 16);
INSERT INTO public.sgc_parroquias VALUES (66, 'Piritu', 17);
INSERT INTO public.sgc_parroquias VALUES (67, 'San Francisco', 17);
INSERT INTO public.sgc_parroquias VALUES (68, 'Lecherias', 18);
INSERT INTO public.sgc_parroquias VALUES (69, 'El Morro', 18);
INSERT INTO public.sgc_parroquias VALUES (70, 'Valle Guanape', 19);
INSERT INTO public.sgc_parroquias VALUES (71, 'Santa Barbara', 19);
INSERT INTO public.sgc_parroquias VALUES (72, 'Santa Ana', 20);
INSERT INTO public.sgc_parroquias VALUES (73, 'Pueblo Nuevo', 20);
INSERT INTO public.sgc_parroquias VALUES (74, 'El Chaparro', 21);
INSERT INTO public.sgc_parroquias VALUES (75, 'Tomas Alfaro Calatrava', 21);
INSERT INTO public.sgc_parroquias VALUES (76, 'Boca Uchire', 22);
INSERT INTO public.sgc_parroquias VALUES (77, 'Boca De Chavez', 22);
INSERT INTO public.sgc_parroquias VALUES (78, 'Achaguas', 23);
INSERT INTO public.sgc_parroquias VALUES (79, 'Apurito', 23);
INSERT INTO public.sgc_parroquias VALUES (80, 'El Yagual', 23);
INSERT INTO public.sgc_parroquias VALUES (81, 'Guachara', 23);
INSERT INTO public.sgc_parroquias VALUES (82, 'Mucuritas', 23);
INSERT INTO public.sgc_parroquias VALUES (83, 'Queseras Del Medio', 23);
INSERT INTO public.sgc_parroquias VALUES (84, 'Bruzual', 24);
INSERT INTO public.sgc_parroquias VALUES (85, 'Mantecal', 24);
INSERT INTO public.sgc_parroquias VALUES (86, 'Quintero', 24);
INSERT INTO public.sgc_parroquias VALUES (87, 'San Vicente', 24);
INSERT INTO public.sgc_parroquias VALUES (88, 'Rincon Hondo', 24);
INSERT INTO public.sgc_parroquias VALUES (89, 'Guasdualito', 25);
INSERT INTO public.sgc_parroquias VALUES (90, 'Aramendi', 25);
INSERT INTO public.sgc_parroquias VALUES (91, 'El Amparo', 25);
INSERT INTO public.sgc_parroquias VALUES (92, 'San Camilo', 25);
INSERT INTO public.sgc_parroquias VALUES (93, 'Urdaneta', 25);
INSERT INTO public.sgc_parroquias VALUES (94, 'San Juan De Payara', 26);
INSERT INTO public.sgc_parroquias VALUES (95, 'Codazzi', 26);
INSERT INTO public.sgc_parroquias VALUES (96, 'Cunaviche', 26);
INSERT INTO public.sgc_parroquias VALUES (97, 'Elorza', 27);
INSERT INTO public.sgc_parroquias VALUES (98, 'La Trinidad', 27);
INSERT INTO public.sgc_parroquias VALUES (99, 'San Fernando', 28);
INSERT INTO public.sgc_parroquias VALUES (100, 'Peñalver', 28);
INSERT INTO public.sgc_parroquias VALUES (101, 'El Recreo', 28);
INSERT INTO public.sgc_parroquias VALUES (102, 'Sn Rafael De Atamaica', 28);
INSERT INTO public.sgc_parroquias VALUES (103, 'Biruaca', 29);
INSERT INTO public.sgc_parroquias VALUES (104, 'Cm. Las Delicias', 30);
INSERT INTO public.sgc_parroquias VALUES (105, 'Choroni', 30);
INSERT INTO public.sgc_parroquias VALUES (106, 'Madre Ma De San Jose', 30);
INSERT INTO public.sgc_parroquias VALUES (107, 'Joaquin Crespo', 30);
INSERT INTO public.sgc_parroquias VALUES (108, 'Pedro Jose Ovalles', 30);
INSERT INTO public.sgc_parroquias VALUES (109, 'Jose Casanova Godoy', 30);
INSERT INTO public.sgc_parroquias VALUES (110, 'Andres Eloy Blanco', 30);
INSERT INTO public.sgc_parroquias VALUES (111, 'Los Tacariguas', 30);
INSERT INTO public.sgc_parroquias VALUES (112, 'Cm. Turmero', 31);
INSERT INTO public.sgc_parroquias VALUES (113, 'Saman De Guere', 31);
INSERT INTO public.sgc_parroquias VALUES (114, 'Alfredo Pacheco M', 31);
INSERT INTO public.sgc_parroquias VALUES (115, 'Chuao', 31);
INSERT INTO public.sgc_parroquias VALUES (116, 'Arevalo Aponte', 31);
INSERT INTO public.sgc_parroquias VALUES (117, 'Cm. La Victoria', 32);
INSERT INTO public.sgc_parroquias VALUES (118, 'Zuata', 32);
INSERT INTO public.sgc_parroquias VALUES (119, 'Pao De Zarate', 32);
INSERT INTO public.sgc_parroquias VALUES (120, 'Castor Nieves Rios', 32);
INSERT INTO public.sgc_parroquias VALUES (121, 'Las Guacamayas', 32);
INSERT INTO public.sgc_parroquias VALUES (122, 'Cm. San Casimiro', 33);
INSERT INTO public.sgc_parroquias VALUES (123, 'Valle Morin', 33);
INSERT INTO public.sgc_parroquias VALUES (124, 'Guiripa', 33);
INSERT INTO public.sgc_parroquias VALUES (125, 'Ollas De Caramacate', 33);
INSERT INTO public.sgc_parroquias VALUES (126, 'Cm. San Sebastian', 34);
INSERT INTO public.sgc_parroquias VALUES (127, 'Cm. Cagua', 35);
INSERT INTO public.sgc_parroquias VALUES (128, 'Bella Vista', 35);
INSERT INTO public.sgc_parroquias VALUES (129, 'Cm. Barbacoas', 36);
INSERT INTO public.sgc_parroquias VALUES (130, 'San Francisco De Cara', 36);
INSERT INTO public.sgc_parroquias VALUES (131, 'Taguay', 36);
INSERT INTO public.sgc_parroquias VALUES (132, 'Las Peñitas', 36);
INSERT INTO public.sgc_parroquias VALUES (133, 'Cm. Villa De Cura', 37);
INSERT INTO public.sgc_parroquias VALUES (134, 'Magdaleno', 37);
INSERT INTO public.sgc_parroquias VALUES (135, 'San Francisco De Asis', 37);
INSERT INTO public.sgc_parroquias VALUES (136, 'Valles De Tucutunemo', 37);
INSERT INTO public.sgc_parroquias VALUES (137, 'Pq Augusto Mijares', 37);
INSERT INTO public.sgc_parroquias VALUES (138, 'Cm. Palo Negro', 38);
INSERT INTO public.sgc_parroquias VALUES (139, 'San Martin De Porres', 38);
INSERT INTO public.sgc_parroquias VALUES (140, 'Cm. Santa Cruz', 39);
INSERT INTO public.sgc_parroquias VALUES (141, 'Cm. San Mateo', 40);
INSERT INTO public.sgc_parroquias VALUES (142, 'Cm. Las Tejerias', 41);
INSERT INTO public.sgc_parroquias VALUES (143, 'Tiara', 41);
INSERT INTO public.sgc_parroquias VALUES (144, 'Cm. El Limon', 42);
INSERT INTO public.sgc_parroquias VALUES (145, 'Ca A De Azucar', 42);
INSERT INTO public.sgc_parroquias VALUES (146, 'Cm. Colonia Tovar', 43);
INSERT INTO public.sgc_parroquias VALUES (147, 'Cm. Camatagua', 44);
INSERT INTO public.sgc_parroquias VALUES (148, 'Carmen De Cura', 44);
INSERT INTO public.sgc_parroquias VALUES (149, 'Cm. El Consejo', 45);
INSERT INTO public.sgc_parroquias VALUES (150, 'Cm. Santa Rita', 46);
INSERT INTO public.sgc_parroquias VALUES (151, 'Francisco De Miranda', 46);
INSERT INTO public.sgc_parroquias VALUES (152, 'Mons Feliciano G', 46);
INSERT INTO public.sgc_parroquias VALUES (153, 'Ocumare De La Costa', 47);
INSERT INTO public.sgc_parroquias VALUES (154, 'Arismendi', 48);
INSERT INTO public.sgc_parroquias VALUES (155, 'Guadarrama', 48);
INSERT INTO public.sgc_parroquias VALUES (156, 'La Union', 48);
INSERT INTO public.sgc_parroquias VALUES (157, 'San Antonio', 48);
INSERT INTO public.sgc_parroquias VALUES (158, 'Alfredo A Larriva', 49);
INSERT INTO public.sgc_parroquias VALUES (159, 'Barinas', 49);
INSERT INTO public.sgc_parroquias VALUES (160, 'San Silvestre', 49);
INSERT INTO public.sgc_parroquias VALUES (161, 'Santa Ines', 49);
INSERT INTO public.sgc_parroquias VALUES (162, 'Santa Lucia', 49);
INSERT INTO public.sgc_parroquias VALUES (163, 'Torunos', 49);
INSERT INTO public.sgc_parroquias VALUES (164, 'El Carmen', 49);
INSERT INTO public.sgc_parroquias VALUES (165, 'Romulo Betancourt', 49);
INSERT INTO public.sgc_parroquias VALUES (166, 'Corazon De Jesus', 49);
INSERT INTO public.sgc_parroquias VALUES (167, 'Ramon I Mendez', 49);
INSERT INTO public.sgc_parroquias VALUES (168, 'Alto Barinas', 49);
INSERT INTO public.sgc_parroquias VALUES (169, 'Manuel P Fajardo', 49);
INSERT INTO public.sgc_parroquias VALUES (170, 'Juan A Rodriguez D', 49);
INSERT INTO public.sgc_parroquias VALUES (171, 'Dominga Ortiz P', 49);
INSERT INTO public.sgc_parroquias VALUES (172, 'Altamira', 50);
INSERT INTO public.sgc_parroquias VALUES (173, 'Barinitas', 50);
INSERT INTO public.sgc_parroquias VALUES (174, 'Calderas', 50);
INSERT INTO public.sgc_parroquias VALUES (175, 'Santa Barbara', 51);
INSERT INTO public.sgc_parroquias VALUES (176, 'Jose Ignacio Del Pumar', 51);
INSERT INTO public.sgc_parroquias VALUES (177, 'Ramon Ignacio Mendez', 51);
INSERT INTO public.sgc_parroquias VALUES (178, 'Pedro Briceño Mendez', 51);
INSERT INTO public.sgc_parroquias VALUES (179, 'El Real', 52);
INSERT INTO public.sgc_parroquias VALUES (180, 'La Luz', 52);
INSERT INTO public.sgc_parroquias VALUES (181, 'Obispos', 52);
INSERT INTO public.sgc_parroquias VALUES (182, 'Los Guasimitos', 52);
INSERT INTO public.sgc_parroquias VALUES (183, 'Ciudad Bolivia', 53);
INSERT INTO public.sgc_parroquias VALUES (184, 'Ignacio Briceño', 53);
INSERT INTO public.sgc_parroquias VALUES (185, 'Paez', 53);
INSERT INTO public.sgc_parroquias VALUES (186, 'Jose Felix Ribas', 53);
INSERT INTO public.sgc_parroquias VALUES (187, 'Dolores', 54);
INSERT INTO public.sgc_parroquias VALUES (188, 'Libertad', 54);
INSERT INTO public.sgc_parroquias VALUES (189, 'Palacio Fajardo', 54);
INSERT INTO public.sgc_parroquias VALUES (190, 'Santa Rosa', 54);
INSERT INTO public.sgc_parroquias VALUES (191, 'Ciudad De Nutrias', 55);
INSERT INTO public.sgc_parroquias VALUES (192, 'El Regalo', 55);
INSERT INTO public.sgc_parroquias VALUES (193, 'Puerto De Nutrias', 55);
INSERT INTO public.sgc_parroquias VALUES (194, 'Santa Catalina', 55);
INSERT INTO public.sgc_parroquias VALUES (195, 'Rodriguez Dominguez', 56);
INSERT INTO public.sgc_parroquias VALUES (196, 'Sabaneta', 56);
INSERT INTO public.sgc_parroquias VALUES (197, 'Ticoporo', 57);
INSERT INTO public.sgc_parroquias VALUES (198, 'Nicolas Pulido', 57);
INSERT INTO public.sgc_parroquias VALUES (199, 'Andres Bello', 57);
INSERT INTO public.sgc_parroquias VALUES (200, 'Barrancas', 58);
INSERT INTO public.sgc_parroquias VALUES (201, 'El Socorro', 58);
INSERT INTO public.sgc_parroquias VALUES (202, 'Masparrito', 58);
INSERT INTO public.sgc_parroquias VALUES (203, 'El Canton', 59);
INSERT INTO public.sgc_parroquias VALUES (204, 'Santa Cruz De Guacas', 59);
INSERT INTO public.sgc_parroquias VALUES (205, 'Puerto Vivas', 59);
INSERT INTO public.sgc_parroquias VALUES (206, 'Simon Bolivar', 60);
INSERT INTO public.sgc_parroquias VALUES (207, 'Once De Abril', 60);
INSERT INTO public.sgc_parroquias VALUES (208, 'Vista Al Sol', 60);
INSERT INTO public.sgc_parroquias VALUES (209, 'Chirica', 60);
INSERT INTO public.sgc_parroquias VALUES (210, 'Dalla Costa', 60);
INSERT INTO public.sgc_parroquias VALUES (211, 'Cachamay', 60);
INSERT INTO public.sgc_parroquias VALUES (212, 'Universidad', 60);
INSERT INTO public.sgc_parroquias VALUES (213, 'Unare', 60);
INSERT INTO public.sgc_parroquias VALUES (214, 'Yocoima', 60);
INSERT INTO public.sgc_parroquias VALUES (215, 'Pozo Verde', 60);
INSERT INTO public.sgc_parroquias VALUES (216, 'Cm. Caicara Del Orinoco', 61);
INSERT INTO public.sgc_parroquias VALUES (217, 'Ascension Farreras', 61);
INSERT INTO public.sgc_parroquias VALUES (218, 'Altagracia', 61);
INSERT INTO public.sgc_parroquias VALUES (219, 'La Urbana', 61);
INSERT INTO public.sgc_parroquias VALUES (220, 'Guaniamo', 61);
INSERT INTO public.sgc_parroquias VALUES (221, 'Pijiguaos', 61);
INSERT INTO public.sgc_parroquias VALUES (222, 'Catedral', 62);
INSERT INTO public.sgc_parroquias VALUES (223, 'Agua Salada', 62);
INSERT INTO public.sgc_parroquias VALUES (224, 'La Sabanita', 62);
INSERT INTO public.sgc_parroquias VALUES (225, 'Vista Hermosa', 62);
INSERT INTO public.sgc_parroquias VALUES (226, 'Marhuanta', 62);
INSERT INTO public.sgc_parroquias VALUES (227, 'Jose Antonio Paez', 62);
INSERT INTO public.sgc_parroquias VALUES (228, 'Orinoco', 62);
INSERT INTO public.sgc_parroquias VALUES (229, 'Panapana', 62);
INSERT INTO public.sgc_parroquias VALUES (230, 'Zea', 62);
INSERT INTO public.sgc_parroquias VALUES (231, 'Cm. Upata', 63);
INSERT INTO public.sgc_parroquias VALUES (232, 'Andres Eloy Blanco', 63);
INSERT INTO public.sgc_parroquias VALUES (233, 'Pedro Cova', 63);
INSERT INTO public.sgc_parroquias VALUES (234, 'Cm. Guasipati', 64);
INSERT INTO public.sgc_parroquias VALUES (235, 'Salom', 64);
INSERT INTO public.sgc_parroquias VALUES (236, 'Cm. Maripa', 65);
INSERT INTO public.sgc_parroquias VALUES (237, 'Aripao', 65);
INSERT INTO public.sgc_parroquias VALUES (238, 'Las Majadas', 65);
INSERT INTO public.sgc_parroquias VALUES (239, 'Moitaco', 65);
INSERT INTO public.sgc_parroquias VALUES (240, 'Guarataro', 65);
INSERT INTO public.sgc_parroquias VALUES (241, 'Cm. Tumeremo', 66);
INSERT INTO public.sgc_parroquias VALUES (242, 'Dalla Costa', 66);
INSERT INTO public.sgc_parroquias VALUES (243, 'San Isidro', 66);
INSERT INTO public.sgc_parroquias VALUES (244, 'Cm. Ciudad Piar', 67);
INSERT INTO public.sgc_parroquias VALUES (245, 'San Francisco', 67);
INSERT INTO public.sgc_parroquias VALUES (246, 'Barceloneta', 67);
INSERT INTO public.sgc_parroquias VALUES (247, 'Santa Barbara', 67);
INSERT INTO public.sgc_parroquias VALUES (248, 'Cm. Santa Elena De Uairen', 68);
INSERT INTO public.sgc_parroquias VALUES (249, 'Ikabaru', 68);
INSERT INTO public.sgc_parroquias VALUES (250, 'Cm. El Callao', 69);
INSERT INTO public.sgc_parroquias VALUES (251, 'Cm. El Palmar', 70);
INSERT INTO public.sgc_parroquias VALUES (252, 'Bejuma', 71);
INSERT INTO public.sgc_parroquias VALUES (253, 'Canoabo', 71);
INSERT INTO public.sgc_parroquias VALUES (254, 'Simon Bolivar', 71);
INSERT INTO public.sgc_parroquias VALUES (255, 'Guigue', 72);
INSERT INTO public.sgc_parroquias VALUES (256, 'Belen', 72);
INSERT INTO public.sgc_parroquias VALUES (257, 'Tacarigua', 72);
INSERT INTO public.sgc_parroquias VALUES (258, 'Mariara', 73);
INSERT INTO public.sgc_parroquias VALUES (259, 'Aguas Calientes', 73);
INSERT INTO public.sgc_parroquias VALUES (260, 'Guacara', 74);
INSERT INTO public.sgc_parroquias VALUES (261, 'Ciudad Alianza', 74);
INSERT INTO public.sgc_parroquias VALUES (262, 'Yagua', 74);
INSERT INTO public.sgc_parroquias VALUES (263, 'Montalban', 75);
INSERT INTO public.sgc_parroquias VALUES (264, 'Moron', 76);
INSERT INTO public.sgc_parroquias VALUES (265, 'Urama', 76);
INSERT INTO public.sgc_parroquias VALUES (266, 'Democracia', 77);
INSERT INTO public.sgc_parroquias VALUES (267, 'Fraternidad', 77);
INSERT INTO public.sgc_parroquias VALUES (268, 'Goaigoaza', 77);
INSERT INTO public.sgc_parroquias VALUES (269, 'Juan Jose Flores', 77);
INSERT INTO public.sgc_parroquias VALUES (270, 'Bartolome Salom', 77);
INSERT INTO public.sgc_parroquias VALUES (271, 'Union', 77);
INSERT INTO public.sgc_parroquias VALUES (272, 'Borburata', 77);
INSERT INTO public.sgc_parroquias VALUES (273, 'Patanemo', 77);
INSERT INTO public.sgc_parroquias VALUES (274, 'San Joaquin', 78);
INSERT INTO public.sgc_parroquias VALUES (275, 'Candelaria', 79);
INSERT INTO public.sgc_parroquias VALUES (276, 'Catedral', 79);
INSERT INTO public.sgc_parroquias VALUES (277, 'El Socorro', 79);
INSERT INTO public.sgc_parroquias VALUES (278, 'Miguel Peña', 79);
INSERT INTO public.sgc_parroquias VALUES (279, 'San Blas', 79);
INSERT INTO public.sgc_parroquias VALUES (280, 'San Jose', 79);
INSERT INTO public.sgc_parroquias VALUES (281, 'Santa Rosa', 79);
INSERT INTO public.sgc_parroquias VALUES (282, 'Rafael Urdaneta', 79);
INSERT INTO public.sgc_parroquias VALUES (283, 'Negro Primero', 79);
INSERT INTO public.sgc_parroquias VALUES (284, 'Miranda', 80);
INSERT INTO public.sgc_parroquias VALUES (285, 'U Los Guayos', 81);
INSERT INTO public.sgc_parroquias VALUES (286, 'Naguanagua', 82);
INSERT INTO public.sgc_parroquias VALUES (287, 'Urb San Diego', 83);
INSERT INTO public.sgc_parroquias VALUES (288, 'U Tocuyito', 84);
INSERT INTO public.sgc_parroquias VALUES (289, 'U Independencia', 84);
INSERT INTO public.sgc_parroquias VALUES (290, 'Cojedes', 85);
INSERT INTO public.sgc_parroquias VALUES (291, 'Juan De Mata Suarez', 85);
INSERT INTO public.sgc_parroquias VALUES (292, 'Tinaquillo', 86);
INSERT INTO public.sgc_parroquias VALUES (293, 'El Baul', 87);
INSERT INTO public.sgc_parroquias VALUES (294, 'Sucre', 87);
INSERT INTO public.sgc_parroquias VALUES (295, 'El Pao', 88);
INSERT INTO public.sgc_parroquias VALUES (296, 'Libertad De Cojedes', 89);
INSERT INTO public.sgc_parroquias VALUES (297, 'El Amparo', 89);
INSERT INTO public.sgc_parroquias VALUES (298, 'San Carlos De Austria', 90);
INSERT INTO public.sgc_parroquias VALUES (299, 'Juan Angel Bravo', 90);
INSERT INTO public.sgc_parroquias VALUES (300, 'Manuel Manrique', 90);
INSERT INTO public.sgc_parroquias VALUES (301, 'Grl/Jefe Jose L Silva', 91);
INSERT INTO public.sgc_parroquias VALUES (302, 'Macapo', 92);
INSERT INTO public.sgc_parroquias VALUES (303, 'La Aguadita', 92);
INSERT INTO public.sgc_parroquias VALUES (304, 'Romulo Gallegos', 93);
INSERT INTO public.sgc_parroquias VALUES (305, 'San Juan De Los Cayos', 94);
INSERT INTO public.sgc_parroquias VALUES (306, 'Capadare', 94);
INSERT INTO public.sgc_parroquias VALUES (307, 'La Pastora', 94);
INSERT INTO public.sgc_parroquias VALUES (308, 'Libertador', 94);
INSERT INTO public.sgc_parroquias VALUES (309, 'San Luis', 95);
INSERT INTO public.sgc_parroquias VALUES (310, 'Aracua', 95);
INSERT INTO public.sgc_parroquias VALUES (311, 'La Peña', 95);
INSERT INTO public.sgc_parroquias VALUES (312, 'Capatarida', 96);
INSERT INTO public.sgc_parroquias VALUES (313, 'Borojo', 96);
INSERT INTO public.sgc_parroquias VALUES (314, 'Seque', 96);
INSERT INTO public.sgc_parroquias VALUES (315, 'Zazarida', 96);
INSERT INTO public.sgc_parroquias VALUES (316, 'Bariro', 96);
INSERT INTO public.sgc_parroquias VALUES (317, 'Guajiro', 96);
INSERT INTO public.sgc_parroquias VALUES (318, 'Norte', 97);
INSERT INTO public.sgc_parroquias VALUES (319, 'Carirubana', 97);
INSERT INTO public.sgc_parroquias VALUES (320, 'Punta Cardon', 97);
INSERT INTO public.sgc_parroquias VALUES (321, 'Santa Ana', 97);
INSERT INTO public.sgc_parroquias VALUES (322, 'La Vela De Coro', 98);
INSERT INTO public.sgc_parroquias VALUES (323, 'Acurigua', 98);
INSERT INTO public.sgc_parroquias VALUES (324, 'Guaibacoa', 98);
INSERT INTO public.sgc_parroquias VALUES (325, 'Macoruca', 98);
INSERT INTO public.sgc_parroquias VALUES (326, 'Las Calderas', 98);
INSERT INTO public.sgc_parroquias VALUES (327, 'Pedregal', 99);
INSERT INTO public.sgc_parroquias VALUES (328, 'Agua Clara', 99);
INSERT INTO public.sgc_parroquias VALUES (329, 'Avaria', 99);
INSERT INTO public.sgc_parroquias VALUES (330, 'Piedra Grande', 99);
INSERT INTO public.sgc_parroquias VALUES (331, 'Purureche', 99);
INSERT INTO public.sgc_parroquias VALUES (332, 'Pueblo Nuevo', 100);
INSERT INTO public.sgc_parroquias VALUES (333, 'Adicora', 100);
INSERT INTO public.sgc_parroquias VALUES (334, 'Baraived', 100);
INSERT INTO public.sgc_parroquias VALUES (335, 'Buena Vista', 100);
INSERT INTO public.sgc_parroquias VALUES (336, 'Jadacaquiva', 100);
INSERT INTO public.sgc_parroquias VALUES (337, 'Moruy', 100);
INSERT INTO public.sgc_parroquias VALUES (338, 'El Vinculo', 100);
INSERT INTO public.sgc_parroquias VALUES (339, 'El Hato', 100);
INSERT INTO public.sgc_parroquias VALUES (340, 'Adaure', 100);
INSERT INTO public.sgc_parroquias VALUES (341, 'Churuguara', 101);
INSERT INTO public.sgc_parroquias VALUES (342, 'Agua Larga', 101);
INSERT INTO public.sgc_parroquias VALUES (343, 'Independencia', 101);
INSERT INTO public.sgc_parroquias VALUES (344, 'Maparari', 101);
INSERT INTO public.sgc_parroquias VALUES (345, 'El Pauji', 101);
INSERT INTO public.sgc_parroquias VALUES (346, 'Mene De Mauroa', 102);
INSERT INTO public.sgc_parroquias VALUES (347, 'Casigua', 102);
INSERT INTO public.sgc_parroquias VALUES (348, 'San Felix', 102);
INSERT INTO public.sgc_parroquias VALUES (349, 'San Antonio', 103);
INSERT INTO public.sgc_parroquias VALUES (350, 'San Gabriel', 103);
INSERT INTO public.sgc_parroquias VALUES (351, 'Santa Ana', 103);
INSERT INTO public.sgc_parroquias VALUES (352, 'Guzman Guillermo', 103);
INSERT INTO public.sgc_parroquias VALUES (353, 'Mitare', 103);
INSERT INTO public.sgc_parroquias VALUES (354, 'Sabaneta', 103);
INSERT INTO public.sgc_parroquias VALUES (355, 'Rio Seco', 103);
INSERT INTO public.sgc_parroquias VALUES (356, 'Cabure', 104);
INSERT INTO public.sgc_parroquias VALUES (357, 'Curimagua', 104);
INSERT INTO public.sgc_parroquias VALUES (358, 'Colina', 104);
INSERT INTO public.sgc_parroquias VALUES (359, 'Tucacas', 105);
INSERT INTO public.sgc_parroquias VALUES (360, 'Boca De Aroa', 105);
INSERT INTO public.sgc_parroquias VALUES (361, 'Puerto Cumarebo', 106);
INSERT INTO public.sgc_parroquias VALUES (362, 'La Cienaga', 106);
INSERT INTO public.sgc_parroquias VALUES (363, 'La Soledad', 106);
INSERT INTO public.sgc_parroquias VALUES (364, 'Pueblo Cumarebo', 106);
INSERT INTO public.sgc_parroquias VALUES (365, 'Zazarida', 106);
INSERT INTO public.sgc_parroquias VALUES (366, 'Cm. Dabajuro', 107);
INSERT INTO public.sgc_parroquias VALUES (367, 'Chichiriviche', 108);
INSERT INTO public.sgc_parroquias VALUES (368, 'Boca De Tocuyo', 108);
INSERT INTO public.sgc_parroquias VALUES (369, 'Tocuyo De La Costa', 108);
INSERT INTO public.sgc_parroquias VALUES (370, 'Los Taques', 109);
INSERT INTO public.sgc_parroquias VALUES (371, 'Judibana', 109);
INSERT INTO public.sgc_parroquias VALUES (372, 'Piritu', 110);
INSERT INTO public.sgc_parroquias VALUES (373, 'San Jose De La Costa', 110);
INSERT INTO public.sgc_parroquias VALUES (374, 'Sta.Cruz De Bucaral', 111);
INSERT INTO public.sgc_parroquias VALUES (375, 'El Charal', 111);
INSERT INTO public.sgc_parroquias VALUES (376, 'Las Vegas Del Tuy', 111);
INSERT INTO public.sgc_parroquias VALUES (377, 'Cm. Mirimire', 112);
INSERT INTO public.sgc_parroquias VALUES (378, 'Jacura', 113);
INSERT INTO public.sgc_parroquias VALUES (379, 'Agua Linda', 113);
INSERT INTO public.sgc_parroquias VALUES (380, 'Araurima', 113);
INSERT INTO public.sgc_parroquias VALUES (381, 'Cm. Yaracal', 114);
INSERT INTO public.sgc_parroquias VALUES (382, 'Cm. Palma Sola', 115);
INSERT INTO public.sgc_parroquias VALUES (383, 'Sucre', 116);
INSERT INTO public.sgc_parroquias VALUES (384, 'Pecaya', 116);
INSERT INTO public.sgc_parroquias VALUES (385, 'Urumaco', 117);
INSERT INTO public.sgc_parroquias VALUES (386, 'Bruzual', 117);
INSERT INTO public.sgc_parroquias VALUES (387, 'Cm. Tocopero', 118);
INSERT INTO public.sgc_parroquias VALUES (388, 'Valle De La Pascua', 119);
INSERT INTO public.sgc_parroquias VALUES (389, 'Espino', 119);
INSERT INTO public.sgc_parroquias VALUES (390, 'El Sombrero', 120);
INSERT INTO public.sgc_parroquias VALUES (391, 'Sosa', 120);
INSERT INTO public.sgc_parroquias VALUES (392, 'Calabozo', 121);
INSERT INTO public.sgc_parroquias VALUES (393, 'El Calvario', 121);
INSERT INTO public.sgc_parroquias VALUES (394, 'El Rastro', 121);
INSERT INTO public.sgc_parroquias VALUES (395, 'Guardatinajas', 121);
INSERT INTO public.sgc_parroquias VALUES (396, 'Altagracia De Orituco', 122);
INSERT INTO public.sgc_parroquias VALUES (397, 'Lezama', 122);
INSERT INTO public.sgc_parroquias VALUES (398, 'Libertad De Orituco', 122);
INSERT INTO public.sgc_parroquias VALUES (399, 'San Fco De Macaira', 122);
INSERT INTO public.sgc_parroquias VALUES (400, 'San Rafael De Orituco', 122);
INSERT INTO public.sgc_parroquias VALUES (401, 'Soublette', 122);
INSERT INTO public.sgc_parroquias VALUES (402, 'Paso Real De Macaira', 122);
INSERT INTO public.sgc_parroquias VALUES (403, 'Tucupido', 123);
INSERT INTO public.sgc_parroquias VALUES (404, 'San Rafael De Laya', 123);
INSERT INTO public.sgc_parroquias VALUES (405, 'San Juan De Los Morros', 124);
INSERT INTO public.sgc_parroquias VALUES (406, 'Parapara', 124);
INSERT INTO public.sgc_parroquias VALUES (407, 'Cantagallo', 124);
INSERT INTO public.sgc_parroquias VALUES (408, 'Zaraza', 125);
INSERT INTO public.sgc_parroquias VALUES (409, 'San Jose De Unare', 125);
INSERT INTO public.sgc_parroquias VALUES (410, 'Camaguan', 126);
INSERT INTO public.sgc_parroquias VALUES (411, 'Puerto Miranda', 126);
INSERT INTO public.sgc_parroquias VALUES (412, 'Uverito', 126);
INSERT INTO public.sgc_parroquias VALUES (413, 'San Jose De Guaribe', 127);
INSERT INTO public.sgc_parroquias VALUES (414, 'Las Mercedes', 128);
INSERT INTO public.sgc_parroquias VALUES (415, 'Sta Rita De Manapire', 128);
INSERT INTO public.sgc_parroquias VALUES (416, 'Cabruta', 128);
INSERT INTO public.sgc_parroquias VALUES (417, 'El Socorro', 129);
INSERT INTO public.sgc_parroquias VALUES (418, 'Ortiz', 130);
INSERT INTO public.sgc_parroquias VALUES (419, 'San Fco. De Tiznados', 130);
INSERT INTO public.sgc_parroquias VALUES (420, 'San Jose De Tiznados', 130);
INSERT INTO public.sgc_parroquias VALUES (421, 'S Lorenzo De Tiznados', 130);
INSERT INTO public.sgc_parroquias VALUES (422, 'Santa Maria De Ipire', 131);
INSERT INTO public.sgc_parroquias VALUES (423, 'Altamira', 131);
INSERT INTO public.sgc_parroquias VALUES (424, 'Chaguaramas', 132);
INSERT INTO public.sgc_parroquias VALUES (425, 'Guayabal', 133);
INSERT INTO public.sgc_parroquias VALUES (426, 'Cazorla', 133);
INSERT INTO public.sgc_parroquias VALUES (427, 'Freitez', 134);
INSERT INTO public.sgc_parroquias VALUES (428, 'Jose Maria Blanco', 134);
INSERT INTO public.sgc_parroquias VALUES (429, 'Catedral', 135);
INSERT INTO public.sgc_parroquias VALUES (430, 'La Concepcion', 135);
INSERT INTO public.sgc_parroquias VALUES (431, 'Santa Rosa', 135);
INSERT INTO public.sgc_parroquias VALUES (432, 'Union', 135);
INSERT INTO public.sgc_parroquias VALUES (433, 'El Cuji', 135);
INSERT INTO public.sgc_parroquias VALUES (434, 'Tamaca', 135);
INSERT INTO public.sgc_parroquias VALUES (435, 'Juan De Villegas', 135);
INSERT INTO public.sgc_parroquias VALUES (436, 'Aguedo F. Alvarado', 135);
INSERT INTO public.sgc_parroquias VALUES (437, 'Buena Vista', 135);
INSERT INTO public.sgc_parroquias VALUES (438, 'Juarez', 135);
INSERT INTO public.sgc_parroquias VALUES (439, 'Juan B Rodriguez', 136);
INSERT INTO public.sgc_parroquias VALUES (440, 'Diego De Lozada', 136);
INSERT INTO public.sgc_parroquias VALUES (441, 'San Miguel', 136);
INSERT INTO public.sgc_parroquias VALUES (442, 'Cuara', 136);
INSERT INTO public.sgc_parroquias VALUES (443, 'Paraiso De San Jose', 136);
INSERT INTO public.sgc_parroquias VALUES (444, 'Tintorero', 136);
INSERT INTO public.sgc_parroquias VALUES (445, 'Jose Bernardo Dorante', 136);
INSERT INTO public.sgc_parroquias VALUES (446, 'Crnel. Mariano Peraza', 136);
INSERT INTO public.sgc_parroquias VALUES (447, 'Bolivar', 137);
INSERT INTO public.sgc_parroquias VALUES (448, 'Anzoategui', 137);
INSERT INTO public.sgc_parroquias VALUES (449, 'Guarico', 137);
INSERT INTO public.sgc_parroquias VALUES (450, 'Humocaro Alto', 137);
INSERT INTO public.sgc_parroquias VALUES (451, 'Humocaro Bajo', 137);
INSERT INTO public.sgc_parroquias VALUES (452, 'Moran', 137);
INSERT INTO public.sgc_parroquias VALUES (453, 'Hilario Luna Y Luna', 137);
INSERT INTO public.sgc_parroquias VALUES (454, 'La Candelaria', 137);
INSERT INTO public.sgc_parroquias VALUES (455, 'Cabudare', 138);
INSERT INTO public.sgc_parroquias VALUES (456, 'Jose G. Bastidas', 138);
INSERT INTO public.sgc_parroquias VALUES (457, 'Agua Viva', 138);
INSERT INTO public.sgc_parroquias VALUES (458, 'Trinidad Samuel', 139);
INSERT INTO public.sgc_parroquias VALUES (459, 'Antonio Diaz', 139);
INSERT INTO public.sgc_parroquias VALUES (460, 'Camacaro', 139);
INSERT INTO public.sgc_parroquias VALUES (461, 'Castañeda', 139);
INSERT INTO public.sgc_parroquias VALUES (462, 'Chiquinquira', 139);
INSERT INTO public.sgc_parroquias VALUES (463, 'Espinoza Los Monteros', 139);
INSERT INTO public.sgc_parroquias VALUES (464, 'Lara', 139);
INSERT INTO public.sgc_parroquias VALUES (465, 'Manuel Morillo', 139);
INSERT INTO public.sgc_parroquias VALUES (466, 'Montes De Oca', 139);
INSERT INTO public.sgc_parroquias VALUES (467, 'Torres', 139);
INSERT INTO public.sgc_parroquias VALUES (468, 'El Blanco', 139);
INSERT INTO public.sgc_parroquias VALUES (469, 'Monta A Verde', 139);
INSERT INTO public.sgc_parroquias VALUES (470, 'Heriberto Arroyo', 139);
INSERT INTO public.sgc_parroquias VALUES (471, 'Las Mercedes', 139);
INSERT INTO public.sgc_parroquias VALUES (472, 'Cecilio Zubillaga', 139);
INSERT INTO public.sgc_parroquias VALUES (473, 'Reyes Vargas', 139);
INSERT INTO public.sgc_parroquias VALUES (474, 'Altagracia', 139);
INSERT INTO public.sgc_parroquias VALUES (475, 'Siquisique', 140);
INSERT INTO public.sgc_parroquias VALUES (476, 'San Miguel', 140);
INSERT INTO public.sgc_parroquias VALUES (477, 'Xaguas', 140);
INSERT INTO public.sgc_parroquias VALUES (478, 'Moroturo', 140);
INSERT INTO public.sgc_parroquias VALUES (479, 'Pio Tamayo', 141);
INSERT INTO public.sgc_parroquias VALUES (480, 'Yacambu', 141);
INSERT INTO public.sgc_parroquias VALUES (481, 'Qbda. Honda De Guache', 141);
INSERT INTO public.sgc_parroquias VALUES (482, 'Sarare', 142);
INSERT INTO public.sgc_parroquias VALUES (483, 'Gustavo Vegas Leon', 142);
INSERT INTO public.sgc_parroquias VALUES (484, 'Buria', 142);
INSERT INTO public.sgc_parroquias VALUES (485, 'Gabriel Picon G.', 143);
INSERT INTO public.sgc_parroquias VALUES (486, 'Hector Amable Mora', 143);
INSERT INTO public.sgc_parroquias VALUES (487, 'Jose Nucete Sardi', 143);
INSERT INTO public.sgc_parroquias VALUES (488, 'Pulido Mendez', 143);
INSERT INTO public.sgc_parroquias VALUES (489, 'Pte. Romulo Gallegos', 143);
INSERT INTO public.sgc_parroquias VALUES (490, 'Presidente Betancourt', 143);
INSERT INTO public.sgc_parroquias VALUES (491, 'Presidente Paez', 143);
INSERT INTO public.sgc_parroquias VALUES (492, 'Cm. La Azulita', 144);
INSERT INTO public.sgc_parroquias VALUES (493, 'Cm. Canagua', 145);
INSERT INTO public.sgc_parroquias VALUES (494, 'Capuri', 145);
INSERT INTO public.sgc_parroquias VALUES (495, 'Chacanta', 145);
INSERT INTO public.sgc_parroquias VALUES (496, 'El Molino', 145);
INSERT INTO public.sgc_parroquias VALUES (497, 'Guaimaral', 145);
INSERT INTO public.sgc_parroquias VALUES (498, 'Mucutuy', 145);
INSERT INTO public.sgc_parroquias VALUES (499, 'Mucuchachi', 145);
INSERT INTO public.sgc_parroquias VALUES (500, 'Acequias', 146);
INSERT INTO public.sgc_parroquias VALUES (501, 'Jaji', 146);
INSERT INTO public.sgc_parroquias VALUES (502, 'La Mesa', 146);
INSERT INTO public.sgc_parroquias VALUES (503, 'San Jose', 146);
INSERT INTO public.sgc_parroquias VALUES (504, 'Montalban', 146);
INSERT INTO public.sgc_parroquias VALUES (505, 'Matriz', 146);
INSERT INTO public.sgc_parroquias VALUES (506, 'Fernandez Peña', 146);
INSERT INTO public.sgc_parroquias VALUES (507, 'Cm. Guaraque', 147);
INSERT INTO public.sgc_parroquias VALUES (508, 'Mesa De Quintero', 147);
INSERT INTO public.sgc_parroquias VALUES (509, 'Rio Negro', 147);
INSERT INTO public.sgc_parroquias VALUES (510, 'Cm. Arapuey', 148);
INSERT INTO public.sgc_parroquias VALUES (511, 'Palmira', 148);
INSERT INTO public.sgc_parroquias VALUES (512, 'Cm. Torondoy', 149);
INSERT INTO public.sgc_parroquias VALUES (513, 'San Cristobal De T', 149);
INSERT INTO public.sgc_parroquias VALUES (514, 'Arias', 150);
INSERT INTO public.sgc_parroquias VALUES (515, 'Sagrario', 150);
INSERT INTO public.sgc_parroquias VALUES (516, 'Milla', 150);
INSERT INTO public.sgc_parroquias VALUES (517, 'El Llano', 150);
INSERT INTO public.sgc_parroquias VALUES (518, 'Juan Rodriguez Suarez', 150);
INSERT INTO public.sgc_parroquias VALUES (519, 'Jacinto Plaza', 150);
INSERT INTO public.sgc_parroquias VALUES (520, 'Domingo Peña', 150);
INSERT INTO public.sgc_parroquias VALUES (521, 'Gonzalo Picon Febres', 150);
INSERT INTO public.sgc_parroquias VALUES (522, 'Osuna Rodriguez', 150);
INSERT INTO public.sgc_parroquias VALUES (523, 'Lasso De La Vega', 150);
INSERT INTO public.sgc_parroquias VALUES (524, 'Caracciolo Parra P', 150);
INSERT INTO public.sgc_parroquias VALUES (525, 'Mariano Picon Salas', 150);
INSERT INTO public.sgc_parroquias VALUES (526, 'Antonio Spinetti Dini', 150);
INSERT INTO public.sgc_parroquias VALUES (527, 'El Morro', 150);
INSERT INTO public.sgc_parroquias VALUES (528, 'Los Nevados', 150);
INSERT INTO public.sgc_parroquias VALUES (529, 'Cm. Tabay', 151);
INSERT INTO public.sgc_parroquias VALUES (530, 'Cm. Timotes', 152);
INSERT INTO public.sgc_parroquias VALUES (531, 'Andres Eloy Blanco', 152);
INSERT INTO public.sgc_parroquias VALUES (532, 'Piñango', 152);
INSERT INTO public.sgc_parroquias VALUES (533, 'La Venta', 152);
INSERT INTO public.sgc_parroquias VALUES (534, 'Cm. Sta Cruz De Mora', 153);
INSERT INTO public.sgc_parroquias VALUES (535, 'Mesa Bolivar', 153);
INSERT INTO public.sgc_parroquias VALUES (536, 'Mesa De Las Palmas', 153);
INSERT INTO public.sgc_parroquias VALUES (537, 'Cm. Sta Elena De Arenales', 154);
INSERT INTO public.sgc_parroquias VALUES (538, 'Eloy Paredes', 154);
INSERT INTO public.sgc_parroquias VALUES (539, 'Pq R De Alcazar', 154);
INSERT INTO public.sgc_parroquias VALUES (540, 'Cm. Tucani', 155);
INSERT INTO public.sgc_parroquias VALUES (541, 'Florencio Ramirez', 155);
INSERT INTO public.sgc_parroquias VALUES (542, 'Cm. Santo Domingo', 156);
INSERT INTO public.sgc_parroquias VALUES (543, 'Las Piedras', 156);
INSERT INTO public.sgc_parroquias VALUES (544, 'Cm. Pueblo Llano', 157);
INSERT INTO public.sgc_parroquias VALUES (545, 'Cm. Mucuchies', 158);
INSERT INTO public.sgc_parroquias VALUES (546, 'Mucuruba', 158);
INSERT INTO public.sgc_parroquias VALUES (547, 'San Rafael', 158);
INSERT INTO public.sgc_parroquias VALUES (548, 'Cacute', 158);
INSERT INTO public.sgc_parroquias VALUES (549, 'La Toma', 158);
INSERT INTO public.sgc_parroquias VALUES (550, 'Cm. Bailadores', 159);
INSERT INTO public.sgc_parroquias VALUES (551, 'Geronimo Maldonado', 159);
INSERT INTO public.sgc_parroquias VALUES (552, 'Cm. Lagunillas', 160);
INSERT INTO public.sgc_parroquias VALUES (553, 'Chiguara', 160);
INSERT INTO public.sgc_parroquias VALUES (554, 'Estanques', 160);
INSERT INTO public.sgc_parroquias VALUES (555, 'San Juan', 160);
INSERT INTO public.sgc_parroquias VALUES (556, 'Pueblo Nuevo Del Sur', 160);
INSERT INTO public.sgc_parroquias VALUES (557, 'La Trampa', 160);
INSERT INTO public.sgc_parroquias VALUES (558, 'El Llano', 161);
INSERT INTO public.sgc_parroquias VALUES (559, 'Tovar', 161);
INSERT INTO public.sgc_parroquias VALUES (560, 'El Amparo', 161);
INSERT INTO public.sgc_parroquias VALUES (561, 'San Francisco', 161);
INSERT INTO public.sgc_parroquias VALUES (562, 'Cm. Nueva Bolivia', 162);
INSERT INTO public.sgc_parroquias VALUES (563, 'Independencia', 162);
INSERT INTO public.sgc_parroquias VALUES (564, 'Maria C Palacios', 162);
INSERT INTO public.sgc_parroquias VALUES (565, 'Santa Apolonia', 162);
INSERT INTO public.sgc_parroquias VALUES (566, 'Cm. Sta Maria De Caparo', 163);
INSERT INTO public.sgc_parroquias VALUES (567, 'Cm. Aricagua', 164);
INSERT INTO public.sgc_parroquias VALUES (568, 'San Antonio', 164);
INSERT INTO public.sgc_parroquias VALUES (569, 'Cm. Zea', 165);
INSERT INTO public.sgc_parroquias VALUES (570, 'Caño El Tigre', 165);
INSERT INTO public.sgc_parroquias VALUES (571, 'Caucagua', 166);
INSERT INTO public.sgc_parroquias VALUES (572, 'Araguita', 166);
INSERT INTO public.sgc_parroquias VALUES (573, 'Arevalo Gonzalez', 166);
INSERT INTO public.sgc_parroquias VALUES (574, 'Capaya', 166);
INSERT INTO public.sgc_parroquias VALUES (575, 'Panaquire', 166);
INSERT INTO public.sgc_parroquias VALUES (576, 'Ribas', 166);
INSERT INTO public.sgc_parroquias VALUES (577, 'El Cafe', 166);
INSERT INTO public.sgc_parroquias VALUES (578, 'Marizapa', 166);
INSERT INTO public.sgc_parroquias VALUES (579, 'Higuerote', 167);
INSERT INTO public.sgc_parroquias VALUES (580, 'Curiepe', 167);
INSERT INTO public.sgc_parroquias VALUES (581, 'Tacarigua', 167);
INSERT INTO public.sgc_parroquias VALUES (582, 'Los Teques', 168);
INSERT INTO public.sgc_parroquias VALUES (583, 'Cecilio Acosta', 168);
INSERT INTO public.sgc_parroquias VALUES (584, 'Paracotos', 168);
INSERT INTO public.sgc_parroquias VALUES (585, 'San Pedro', 168);
INSERT INTO public.sgc_parroquias VALUES (586, 'Tacata', 168);
INSERT INTO public.sgc_parroquias VALUES (587, 'El Jarillo', 168);
INSERT INTO public.sgc_parroquias VALUES (588, 'Altagracia De La M', 168);
INSERT INTO public.sgc_parroquias VALUES (589, 'Sta Teresa Del Tuy', 169);
INSERT INTO public.sgc_parroquias VALUES (590, 'El Cartanal', 169);
INSERT INTO public.sgc_parroquias VALUES (591, 'Ocumare Del Tuy', 170);
INSERT INTO public.sgc_parroquias VALUES (592, 'La Democracia', 170);
INSERT INTO public.sgc_parroquias VALUES (593, 'Santa Barbara', 170);
INSERT INTO public.sgc_parroquias VALUES (594, 'Rio Chico', 171);
INSERT INTO public.sgc_parroquias VALUES (595, 'El Guapo', 171);
INSERT INTO public.sgc_parroquias VALUES (596, 'Tacarigua De La Laguna', 171);
INSERT INTO public.sgc_parroquias VALUES (597, 'Paparo', 171);
INSERT INTO public.sgc_parroquias VALUES (598, 'Sn Fernando Del Guapo', 171);
INSERT INTO public.sgc_parroquias VALUES (599, 'Santa Lucia', 172);
INSERT INTO public.sgc_parroquias VALUES (600, 'Guarenas', 173);
INSERT INTO public.sgc_parroquias VALUES (601, 'Petare', 174);
INSERT INTO public.sgc_parroquias VALUES (602, 'Leoncio Martinez', 174);
INSERT INTO public.sgc_parroquias VALUES (603, 'Caucaguita', 174);
INSERT INTO public.sgc_parroquias VALUES (604, 'Filas De Mariches', 174);
INSERT INTO public.sgc_parroquias VALUES (605, 'La Dolorita', 174);
INSERT INTO public.sgc_parroquias VALUES (606, 'Cua', 175);
INSERT INTO public.sgc_parroquias VALUES (607, 'Nueva Cua', 175);
INSERT INTO public.sgc_parroquias VALUES (608, 'Guatire', 176);
INSERT INTO public.sgc_parroquias VALUES (609, 'Bolivar', 176);
INSERT INTO public.sgc_parroquias VALUES (610, 'Charallave', 177);
INSERT INTO public.sgc_parroquias VALUES (611, 'Las Brisas', 177);
INSERT INTO public.sgc_parroquias VALUES (612, 'San Antonio Los Altos', 178);
INSERT INTO public.sgc_parroquias VALUES (613, 'San Jose De Barlovento', 179);
INSERT INTO public.sgc_parroquias VALUES (614, 'Cumbo', 179);
INSERT INTO public.sgc_parroquias VALUES (615, 'San Fco De Yare', 180);
INSERT INTO public.sgc_parroquias VALUES (616, 'S Antonio De Yare', 180);
INSERT INTO public.sgc_parroquias VALUES (617, 'Baruta', 181);
INSERT INTO public.sgc_parroquias VALUES (618, 'El Cafetal', 181);
INSERT INTO public.sgc_parroquias VALUES (619, 'Las Minas De Baruta', 181);
INSERT INTO public.sgc_parroquias VALUES (620, 'Carrizal', 182);
INSERT INTO public.sgc_parroquias VALUES (621, 'Chacao', 183);
INSERT INTO public.sgc_parroquias VALUES (622, 'El Hatillo', 184);
INSERT INTO public.sgc_parroquias VALUES (623, 'Mamporal', 185);
INSERT INTO public.sgc_parroquias VALUES (624, 'Cupira', 186);
INSERT INTO public.sgc_parroquias VALUES (625, 'Machurucuto', 186);
INSERT INTO public.sgc_parroquias VALUES (626, 'Cm. San Antonio', 187);
INSERT INTO public.sgc_parroquias VALUES (627, 'San Francisco', 187);
INSERT INTO public.sgc_parroquias VALUES (628, 'Cm. Caripito', 188);
INSERT INTO public.sgc_parroquias VALUES (629, 'Cm. Caripe', 189);
INSERT INTO public.sgc_parroquias VALUES (630, 'Teresen', 189);
INSERT INTO public.sgc_parroquias VALUES (631, 'El Guacharo', 189);
INSERT INTO public.sgc_parroquias VALUES (632, 'San Agustin', 189);
INSERT INTO public.sgc_parroquias VALUES (633, 'La Guanota', 189);
INSERT INTO public.sgc_parroquias VALUES (634, 'Sabana De Piedra', 189);
INSERT INTO public.sgc_parroquias VALUES (635, 'Cm. Caicara', 190);
INSERT INTO public.sgc_parroquias VALUES (636, 'Areo', 190);
INSERT INTO public.sgc_parroquias VALUES (637, 'San Felix', 190);
INSERT INTO public.sgc_parroquias VALUES (638, 'Viento Fresco', 190);
INSERT INTO public.sgc_parroquias VALUES (639, 'Cm. Punta De Mata', 191);
INSERT INTO public.sgc_parroquias VALUES (640, 'El Tejero', 191);
INSERT INTO public.sgc_parroquias VALUES (641, 'Cm. Temblador', 192);
INSERT INTO public.sgc_parroquias VALUES (642, 'Tabasca', 192);
INSERT INTO public.sgc_parroquias VALUES (643, 'Las Alhuacas', 192);
INSERT INTO public.sgc_parroquias VALUES (644, 'Chaguaramas', 192);
INSERT INTO public.sgc_parroquias VALUES (645, 'El Furrial', 193);
INSERT INTO public.sgc_parroquias VALUES (646, 'Jusepin', 193);
INSERT INTO public.sgc_parroquias VALUES (647, 'El Corozo', 193);
INSERT INTO public.sgc_parroquias VALUES (648, 'San Vicente', 193);
INSERT INTO public.sgc_parroquias VALUES (649, 'La Pica', 193);
INSERT INTO public.sgc_parroquias VALUES (650, 'Alto De Los Godos', 193);
INSERT INTO public.sgc_parroquias VALUES (651, 'Boqueron', 193);
INSERT INTO public.sgc_parroquias VALUES (652, 'Las Cocuizas', 193);
INSERT INTO public.sgc_parroquias VALUES (653, 'Santa Cruz', 193);
INSERT INTO public.sgc_parroquias VALUES (654, 'San Simon', 193);
INSERT INTO public.sgc_parroquias VALUES (655, 'Cm. Aragua', 194);
INSERT INTO public.sgc_parroquias VALUES (656, 'Chaguaramal', 194);
INSERT INTO public.sgc_parroquias VALUES (657, 'Guanaguana', 194);
INSERT INTO public.sgc_parroquias VALUES (658, 'Aparicio', 194);
INSERT INTO public.sgc_parroquias VALUES (659, 'Taguaya', 194);
INSERT INTO public.sgc_parroquias VALUES (660, 'El Pinto', 194);
INSERT INTO public.sgc_parroquias VALUES (661, 'La Toscana', 194);
INSERT INTO public.sgc_parroquias VALUES (662, 'Cm. Quiriquire', 195);
INSERT INTO public.sgc_parroquias VALUES (663, 'Cachipo', 195);
INSERT INTO public.sgc_parroquias VALUES (664, 'Cm. Barrancas', 196);
INSERT INTO public.sgc_parroquias VALUES (665, 'Los Barrancos De Fajardo', 196);
INSERT INTO public.sgc_parroquias VALUES (666, 'Cm. Aguasay', 197);
INSERT INTO public.sgc_parroquias VALUES (667, 'Cm. Santa Barbara', 198);
INSERT INTO public.sgc_parroquias VALUES (668, 'Cm. Uracoa', 199);
INSERT INTO public.sgc_parroquias VALUES (669, 'Cm. La Asuncion', 200);
INSERT INTO public.sgc_parroquias VALUES (670, 'Cm. San Juan Bautista', 201);
INSERT INTO public.sgc_parroquias VALUES (671, 'Zabala', 201);
INSERT INTO public.sgc_parroquias VALUES (672, 'Cm. Santa Ana', 202);
INSERT INTO public.sgc_parroquias VALUES (673, 'Guevara', 202);
INSERT INTO public.sgc_parroquias VALUES (674, 'Matasiete', 202);
INSERT INTO public.sgc_parroquias VALUES (675, 'Bolivar', 202);
INSERT INTO public.sgc_parroquias VALUES (676, 'Sucre', 202);
INSERT INTO public.sgc_parroquias VALUES (677, 'Cm. Pampatar', 203);
INSERT INTO public.sgc_parroquias VALUES (678, 'Aguirre', 203);
INSERT INTO public.sgc_parroquias VALUES (679, 'Cm. Juan Griego', 204);
INSERT INTO public.sgc_parroquias VALUES (680, 'Adrian', 204);
INSERT INTO public.sgc_parroquias VALUES (681, 'Cm. Porlamar', 205);
INSERT INTO public.sgc_parroquias VALUES (682, 'Cm. Boca Del Rio', 206);
INSERT INTO public.sgc_parroquias VALUES (683, 'San Francisco', 206);
INSERT INTO public.sgc_parroquias VALUES (684, 'Cm. San Pedro De Coche', 207);
INSERT INTO public.sgc_parroquias VALUES (685, 'Vicente Fuentes', 207);
INSERT INTO public.sgc_parroquias VALUES (686, 'Cm. Punta De Piedras', 208);
INSERT INTO public.sgc_parroquias VALUES (687, 'Los Barales', 208);
INSERT INTO public.sgc_parroquias VALUES (688, 'Cm.La Plaza De Paraguachi', 209);
INSERT INTO public.sgc_parroquias VALUES (689, 'Cm. Valle Esp Santo', 210);
INSERT INTO public.sgc_parroquias VALUES (690, 'Francisco Fajardo', 210);
INSERT INTO public.sgc_parroquias VALUES (691, 'Cm. Araure', 211);
INSERT INTO public.sgc_parroquias VALUES (692, 'Rio Acarigua', 211);
INSERT INTO public.sgc_parroquias VALUES (693, 'Cm. Piritu', 212);
INSERT INTO public.sgc_parroquias VALUES (694, 'Uveral', 212);
INSERT INTO public.sgc_parroquias VALUES (695, 'Cm. Guanare', 213);
INSERT INTO public.sgc_parroquias VALUES (696, 'Cordoba', 213);
INSERT INTO public.sgc_parroquias VALUES (697, 'San Juan Guanaguanare', 213);
INSERT INTO public.sgc_parroquias VALUES (698, 'Virgen De La Coromoto', 213);
INSERT INTO public.sgc_parroquias VALUES (699, 'San Jose De La Montaña', 213);
INSERT INTO public.sgc_parroquias VALUES (700, 'Cm. Guanarito', 214);
INSERT INTO public.sgc_parroquias VALUES (701, 'Trinidad De La Capilla', 214);
INSERT INTO public.sgc_parroquias VALUES (702, 'Divina Pastora', 214);
INSERT INTO public.sgc_parroquias VALUES (703, 'Cm. Ospino', 215);
INSERT INTO public.sgc_parroquias VALUES (704, 'Aparicion', 215);
INSERT INTO public.sgc_parroquias VALUES (705, 'La Estacion', 215);
INSERT INTO public.sgc_parroquias VALUES (706, 'Cm. Acarigua', 216);
INSERT INTO public.sgc_parroquias VALUES (707, 'Payara', 216);
INSERT INTO public.sgc_parroquias VALUES (708, 'Pimpinela', 216);
INSERT INTO public.sgc_parroquias VALUES (709, 'Ramon Peraza', 216);
INSERT INTO public.sgc_parroquias VALUES (710, 'Cm. Biscucuy', 217);
INSERT INTO public.sgc_parroquias VALUES (711, 'Concepcion', 217);
INSERT INTO public.sgc_parroquias VALUES (712, 'San Rafael Palo Alzado', 217);
INSERT INTO public.sgc_parroquias VALUES (713, 'Uvencio A Velasquez', 217);
INSERT INTO public.sgc_parroquias VALUES (714, 'San Jose De Saguaz', 217);
INSERT INTO public.sgc_parroquias VALUES (715, 'Villa Rosa', 217);
INSERT INTO public.sgc_parroquias VALUES (716, 'Cm. Villa Bruzual', 218);
INSERT INTO public.sgc_parroquias VALUES (717, 'Canelones', 218);
INSERT INTO public.sgc_parroquias VALUES (718, 'Santa Cruz', 218);
INSERT INTO public.sgc_parroquias VALUES (719, 'San Isidro Labrador', 218);
INSERT INTO public.sgc_parroquias VALUES (720, 'Cm. Chabasquen', 219);
INSERT INTO public.sgc_parroquias VALUES (721, 'Peña Blanca', 219);
INSERT INTO public.sgc_parroquias VALUES (722, 'Cm. Agua Blanca', 220);
INSERT INTO public.sgc_parroquias VALUES (723, 'Cm. Papelon', 221);
INSERT INTO public.sgc_parroquias VALUES (724, 'Caño Delgadito', 221);
INSERT INTO public.sgc_parroquias VALUES (725, 'Cm. Boconoito', 222);
INSERT INTO public.sgc_parroquias VALUES (726, 'Antolin Tovar Aquino', 222);
INSERT INTO public.sgc_parroquias VALUES (727, 'Cm. San Rafael De Onoto', 223);
INSERT INTO public.sgc_parroquias VALUES (728, 'Santa Fe', 223);
INSERT INTO public.sgc_parroquias VALUES (729, 'Thermo Morles', 223);
INSERT INTO public.sgc_parroquias VALUES (730, 'Cm. El Playon', 224);
INSERT INTO public.sgc_parroquias VALUES (731, 'Florida', 224);
INSERT INTO public.sgc_parroquias VALUES (732, 'Rio Caribe', 225);
INSERT INTO public.sgc_parroquias VALUES (733, 'San Juan Galdonas', 225);
INSERT INTO public.sgc_parroquias VALUES (734, 'Puerto Santo', 225);
INSERT INTO public.sgc_parroquias VALUES (735, 'El Morro De Pto Santo', 225);
INSERT INTO public.sgc_parroquias VALUES (736, 'Antonio Jose De Sucre', 225);
INSERT INTO public.sgc_parroquias VALUES (737, 'El Pilar', 226);
INSERT INTO public.sgc_parroquias VALUES (738, 'El Rincon', 226);
INSERT INTO public.sgc_parroquias VALUES (739, 'Guaraunos', 226);
INSERT INTO public.sgc_parroquias VALUES (740, 'Tunapuicito', 226);
INSERT INTO public.sgc_parroquias VALUES (741, 'Union', 226);
INSERT INTO public.sgc_parroquias VALUES (742, 'Gral Fco. A Vasquez', 226);
INSERT INTO public.sgc_parroquias VALUES (743, 'Santa Catalina', 227);
INSERT INTO public.sgc_parroquias VALUES (744, 'Santa Rosa', 227);
INSERT INTO public.sgc_parroquias VALUES (745, 'Santa Teresa', 227);
INSERT INTO public.sgc_parroquias VALUES (746, 'Bolivar', 227);
INSERT INTO public.sgc_parroquias VALUES (747, 'Macarapana', 227);
INSERT INTO public.sgc_parroquias VALUES (748, 'Yaguaraparo', 228);
INSERT INTO public.sgc_parroquias VALUES (749, 'Libertad', 228);
INSERT INTO public.sgc_parroquias VALUES (750, 'Paujil', 228);
INSERT INTO public.sgc_parroquias VALUES (751, 'Irapa', 229);
INSERT INTO public.sgc_parroquias VALUES (752, 'Campo Claro', 229);
INSERT INTO public.sgc_parroquias VALUES (753, 'Soro', 229);
INSERT INTO public.sgc_parroquias VALUES (754, 'San Antonio De Irapa', 229);
INSERT INTO public.sgc_parroquias VALUES (755, 'Marabal', 229);
INSERT INTO public.sgc_parroquias VALUES (756, 'Cm. San Ant Del Golfo', 230);
INSERT INTO public.sgc_parroquias VALUES (757, 'Cumanacoa', 231);
INSERT INTO public.sgc_parroquias VALUES (758, 'Arenas', 231);
INSERT INTO public.sgc_parroquias VALUES (759, 'Aricagua', 231);
INSERT INTO public.sgc_parroquias VALUES (760, 'Cocollar', 231);
INSERT INTO public.sgc_parroquias VALUES (761, 'San Fernando', 231);
INSERT INTO public.sgc_parroquias VALUES (762, 'San Lorenzo', 231);
INSERT INTO public.sgc_parroquias VALUES (763, 'Cariaco', 232);
INSERT INTO public.sgc_parroquias VALUES (764, 'Catuaro', 232);
INSERT INTO public.sgc_parroquias VALUES (765, 'Rendon', 232);
INSERT INTO public.sgc_parroquias VALUES (766, 'Santa Cruz', 232);
INSERT INTO public.sgc_parroquias VALUES (767, 'Santa Maria', 232);
INSERT INTO public.sgc_parroquias VALUES (768, 'Altagracia', 233);
INSERT INTO public.sgc_parroquias VALUES (769, 'Ayacucho', 233);
INSERT INTO public.sgc_parroquias VALUES (770, 'Santa Ines', 233);
INSERT INTO public.sgc_parroquias VALUES (771, 'Valentin Valiente', 233);
INSERT INTO public.sgc_parroquias VALUES (772, 'San Juan', 233);
INSERT INTO public.sgc_parroquias VALUES (773, 'Gran Mariscal', 233);
INSERT INTO public.sgc_parroquias VALUES (774, 'Raul Leoni', 233);
INSERT INTO public.sgc_parroquias VALUES (775, 'Guiria', 234);
INSERT INTO public.sgc_parroquias VALUES (776, 'Cristobal Colon', 234);
INSERT INTO public.sgc_parroquias VALUES (777, 'Punta De Piedra', 234);
INSERT INTO public.sgc_parroquias VALUES (778, 'Bideau', 234);
INSERT INTO public.sgc_parroquias VALUES (779, 'Mariño', 235);
INSERT INTO public.sgc_parroquias VALUES (780, 'Romulo Gallegos', 235);
INSERT INTO public.sgc_parroquias VALUES (781, 'Tunapuy', 236);
INSERT INTO public.sgc_parroquias VALUES (782, 'Campo Elias', 236);
INSERT INTO public.sgc_parroquias VALUES (783, 'San Jose De Areocuar', 237);
INSERT INTO public.sgc_parroquias VALUES (784, 'Tavera Acosta', 237);
INSERT INTO public.sgc_parroquias VALUES (785, 'Cm. Mariguitar', 238);
INSERT INTO public.sgc_parroquias VALUES (786, 'Araya', 239);
INSERT INTO public.sgc_parroquias VALUES (787, 'Manicuare', 239);
INSERT INTO public.sgc_parroquias VALUES (788, 'Chacopata', 239);
INSERT INTO public.sgc_parroquias VALUES (789, 'Cm. Colon', 240);
INSERT INTO public.sgc_parroquias VALUES (790, 'Rivas Berti', 240);
INSERT INTO public.sgc_parroquias VALUES (791, 'San Pedro Del Rio', 240);
INSERT INTO public.sgc_parroquias VALUES (792, 'Cm. San Ant Del Tachira', 241);
INSERT INTO public.sgc_parroquias VALUES (793, 'Palotal', 241);
INSERT INTO public.sgc_parroquias VALUES (794, 'Juan Vicente Gomez', 241);
INSERT INTO public.sgc_parroquias VALUES (795, 'Isaias Medina Angarit', 241);
INSERT INTO public.sgc_parroquias VALUES (796, 'Cm. Capacho Nuevo', 242);
INSERT INTO public.sgc_parroquias VALUES (797, 'Juan German Roscio', 242);
INSERT INTO public.sgc_parroquias VALUES (798, 'Roman Cardenas', 242);
INSERT INTO public.sgc_parroquias VALUES (799, 'Cm. Tariba', 243);
INSERT INTO public.sgc_parroquias VALUES (800, 'La Florida', 243);
INSERT INTO public.sgc_parroquias VALUES (801, 'Amenodoro Rangel Lamu', 243);
INSERT INTO public.sgc_parroquias VALUES (802, 'Cm. La Grita', 244);
INSERT INTO public.sgc_parroquias VALUES (803, 'Emilio C. Guerrero', 244);
INSERT INTO public.sgc_parroquias VALUES (804, 'Mons. Miguel A Salas', 244);
INSERT INTO public.sgc_parroquias VALUES (805, 'Cm. Rubio', 245);
INSERT INTO public.sgc_parroquias VALUES (806, 'Bramon', 245);
INSERT INTO public.sgc_parroquias VALUES (807, 'La Petrolea', 245);
INSERT INTO public.sgc_parroquias VALUES (808, 'Quinimari', 245);
INSERT INTO public.sgc_parroquias VALUES (809, 'Cm. Lobatera', 246);
INSERT INTO public.sgc_parroquias VALUES (810, 'Constitucion', 246);
INSERT INTO public.sgc_parroquias VALUES (811, 'La Concordia', 247);
INSERT INTO public.sgc_parroquias VALUES (812, 'Pedro Maria Morantes', 247);
INSERT INTO public.sgc_parroquias VALUES (813, 'Sn Juan Bautista', 247);
INSERT INTO public.sgc_parroquias VALUES (814, 'San Sebastian', 247);
INSERT INTO public.sgc_parroquias VALUES (815, 'Dr. Fco. Romero Lobo', 247);
INSERT INTO public.sgc_parroquias VALUES (816, 'Cm. Pregonero', 248);
INSERT INTO public.sgc_parroquias VALUES (817, 'Cardenas', 248);
INSERT INTO public.sgc_parroquias VALUES (818, 'Potosi', 248);
INSERT INTO public.sgc_parroquias VALUES (819, 'Juan Pablo Peñaloza', 248);
INSERT INTO public.sgc_parroquias VALUES (820, 'Cm. Sta. Ana  Del Tachira', 249);
INSERT INTO public.sgc_parroquias VALUES (821, 'Cm. La Fria', 250);
INSERT INTO public.sgc_parroquias VALUES (822, 'Boca De Grita', 250);
INSERT INTO public.sgc_parroquias VALUES (823, 'Jose Antonio Paez', 250);
INSERT INTO public.sgc_parroquias VALUES (824, 'Cm. Palmira', 251);
INSERT INTO public.sgc_parroquias VALUES (825, 'Cm. Michelena', 252);
INSERT INTO public.sgc_parroquias VALUES (826, 'Cm. Abejales', 253);
INSERT INTO public.sgc_parroquias VALUES (827, 'San Joaquin De Navay', 253);
INSERT INTO public.sgc_parroquias VALUES (828, 'Doradas', 253);
INSERT INTO public.sgc_parroquias VALUES (829, 'Emeterio Ochoa', 253);
INSERT INTO public.sgc_parroquias VALUES (830, 'Cm. Coloncito', 254);
INSERT INTO public.sgc_parroquias VALUES (831, 'La Palmita', 254);
INSERT INTO public.sgc_parroquias VALUES (832, 'Cm. Ureña', 255);
INSERT INTO public.sgc_parroquias VALUES (833, 'Nueva Arcadia', 255);
INSERT INTO public.sgc_parroquias VALUES (834, 'Cm. Queniquea', 256);
INSERT INTO public.sgc_parroquias VALUES (835, 'San Pablo', 256);
INSERT INTO public.sgc_parroquias VALUES (836, 'Eleazar Lopez Contrera', 256);
INSERT INTO public.sgc_parroquias VALUES (837, 'Cm. Cordero', 257);
INSERT INTO public.sgc_parroquias VALUES (838, 'Cm.San Rafael Del Pinal', 258);
INSERT INTO public.sgc_parroquias VALUES (839, 'Santo Domingo', 258);
INSERT INTO public.sgc_parroquias VALUES (840, 'Alberto Adriani', 258);
INSERT INTO public.sgc_parroquias VALUES (841, 'Cm. Capacho Viejo', 259);
INSERT INTO public.sgc_parroquias VALUES (842, 'Cipriano Castro', 259);
INSERT INTO public.sgc_parroquias VALUES (843, 'Manuel Felipe Rugeles', 259);
INSERT INTO public.sgc_parroquias VALUES (844, 'Cm. La Tendida', 260);
INSERT INTO public.sgc_parroquias VALUES (845, 'Bocono', 260);
INSERT INTO public.sgc_parroquias VALUES (846, 'Hernandez', 260);
INSERT INTO public.sgc_parroquias VALUES (847, 'Cm. Seboruco', 261);
INSERT INTO public.sgc_parroquias VALUES (848, 'Cm. Las Mesas', 262);
INSERT INTO public.sgc_parroquias VALUES (849, 'Cm. San Jose De Bolivar', 263);
INSERT INTO public.sgc_parroquias VALUES (850, 'Cm. El Cobre', 264);
INSERT INTO public.sgc_parroquias VALUES (851, 'Cm. Delicias', 265);
INSERT INTO public.sgc_parroquias VALUES (852, 'Cm. San Simon', 266);
INSERT INTO public.sgc_parroquias VALUES (853, 'Cm. San Josecito', 267);
INSERT INTO public.sgc_parroquias VALUES (854, 'Cm. Umuquena', 268);
INSERT INTO public.sgc_parroquias VALUES (855, 'Betijoque', 269);
INSERT INTO public.sgc_parroquias VALUES (856, 'Jose G Hernandez', 269);
INSERT INTO public.sgc_parroquias VALUES (857, 'La Pueblita', 269);
INSERT INTO public.sgc_parroquias VALUES (858, 'El Cedro', 269);
INSERT INTO public.sgc_parroquias VALUES (859, 'Bocono', 270);
INSERT INTO public.sgc_parroquias VALUES (860, 'El Carmen', 270);
INSERT INTO public.sgc_parroquias VALUES (861, 'Mosquey', 270);
INSERT INTO public.sgc_parroquias VALUES (862, 'Ayacucho', 270);
INSERT INTO public.sgc_parroquias VALUES (863, 'Burbusay', 270);
INSERT INTO public.sgc_parroquias VALUES (864, 'General Rivas', 270);
INSERT INTO public.sgc_parroquias VALUES (865, 'Monseñor Jauregui', 270);
INSERT INTO public.sgc_parroquias VALUES (866, 'Rafael Rangel', 270);
INSERT INTO public.sgc_parroquias VALUES (867, 'San Jose', 270);
INSERT INTO public.sgc_parroquias VALUES (868, 'San Miguel', 270);
INSERT INTO public.sgc_parroquias VALUES (869, 'Guaramacal', 270);
INSERT INTO public.sgc_parroquias VALUES (870, 'La Vega De Guaramacal', 270);
INSERT INTO public.sgc_parroquias VALUES (871, 'Carache', 271);
INSERT INTO public.sgc_parroquias VALUES (872, 'La Concepcion', 271);
INSERT INTO public.sgc_parroquias VALUES (873, 'Cuicas', 271);
INSERT INTO public.sgc_parroquias VALUES (874, 'Panamericana', 271);
INSERT INTO public.sgc_parroquias VALUES (875, 'Santa Cruz', 271);
INSERT INTO public.sgc_parroquias VALUES (876, 'Escuque', 272);
INSERT INTO public.sgc_parroquias VALUES (877, 'Sabana Libre', 272);
INSERT INTO public.sgc_parroquias VALUES (878, 'La Union', 272);
INSERT INTO public.sgc_parroquias VALUES (879, 'Santa Rita', 272);
INSERT INTO public.sgc_parroquias VALUES (880, 'Cristobal Mendoza', 273);
INSERT INTO public.sgc_parroquias VALUES (881, 'Chiquinquira', 273);
INSERT INTO public.sgc_parroquias VALUES (882, 'Matriz', 273);
INSERT INTO public.sgc_parroquias VALUES (883, 'Monseñor Carrillo', 273);
INSERT INTO public.sgc_parroquias VALUES (884, 'Cruz Carrillo', 273);
INSERT INTO public.sgc_parroquias VALUES (885, 'Andres Linares', 273);
INSERT INTO public.sgc_parroquias VALUES (886, 'Tres Esquinas', 273);
INSERT INTO public.sgc_parroquias VALUES (887, 'La Quebrada', 274);
INSERT INTO public.sgc_parroquias VALUES (888, 'Jajo', 274);
INSERT INTO public.sgc_parroquias VALUES (889, 'La Mesa', 274);
INSERT INTO public.sgc_parroquias VALUES (890, 'Santiago', 274);
INSERT INTO public.sgc_parroquias VALUES (891, 'Cabimbu', 274);
INSERT INTO public.sgc_parroquias VALUES (892, 'Tuñame', 274);
INSERT INTO public.sgc_parroquias VALUES (893, 'Mercedes Diaz', 275);
INSERT INTO public.sgc_parroquias VALUES (894, 'Juan Ignacio Montilla', 275);
INSERT INTO public.sgc_parroquias VALUES (895, 'La Beatriz', 275);
INSERT INTO public.sgc_parroquias VALUES (896, 'Mendoza', 275);
INSERT INTO public.sgc_parroquias VALUES (897, 'La Puerta', 275);
INSERT INTO public.sgc_parroquias VALUES (898, 'San Luis', 275);
INSERT INTO public.sgc_parroquias VALUES (899, 'Chejende', 276);
INSERT INTO public.sgc_parroquias VALUES (900, 'Carrillo', 276);
INSERT INTO public.sgc_parroquias VALUES (901, 'Cegarra', 276);
INSERT INTO public.sgc_parroquias VALUES (902, 'Bolivia', 276);
INSERT INTO public.sgc_parroquias VALUES (903, 'Manuel Salvador Ulloa', 276);
INSERT INTO public.sgc_parroquias VALUES (904, 'San Jose', 276);
INSERT INTO public.sgc_parroquias VALUES (905, 'Arnoldo Gabaldon', 276);
INSERT INTO public.sgc_parroquias VALUES (906, 'El Dividive', 277);
INSERT INTO public.sgc_parroquias VALUES (907, 'Agua Caliente', 277);
INSERT INTO public.sgc_parroquias VALUES (908, 'El Cenizo', 277);
INSERT INTO public.sgc_parroquias VALUES (909, 'Agua Santa', 277);
INSERT INTO public.sgc_parroquias VALUES (910, 'Valerita', 277);
INSERT INTO public.sgc_parroquias VALUES (911, 'Monte Carmelo', 278);
INSERT INTO public.sgc_parroquias VALUES (912, 'Buena Vista', 278);
INSERT INTO public.sgc_parroquias VALUES (913, 'Sta Maria Del Horcon', 278);
INSERT INTO public.sgc_parroquias VALUES (914, 'Motatan', 279);
INSERT INTO public.sgc_parroquias VALUES (915, 'El Baño', 279);
INSERT INTO public.sgc_parroquias VALUES (916, 'Jalisco', 279);
INSERT INTO public.sgc_parroquias VALUES (917, 'Pampan', 280);
INSERT INTO public.sgc_parroquias VALUES (918, 'Santa Ana', 280);
INSERT INTO public.sgc_parroquias VALUES (919, 'La Paz', 280);
INSERT INTO public.sgc_parroquias VALUES (920, 'Flor De Patria', 280);
INSERT INTO public.sgc_parroquias VALUES (921, 'Carvajal', 281);
INSERT INTO public.sgc_parroquias VALUES (922, 'Antonio N Briceño', 281);
INSERT INTO public.sgc_parroquias VALUES (923, 'Campo Alegre', 281);
INSERT INTO public.sgc_parroquias VALUES (924, 'Jose Leonardo Suarez', 281);
INSERT INTO public.sgc_parroquias VALUES (925, 'Sabana De Mendoza', 282);
INSERT INTO public.sgc_parroquias VALUES (926, 'Junin', 282);
INSERT INTO public.sgc_parroquias VALUES (927, 'Valmore Rodriguez', 282);
INSERT INTO public.sgc_parroquias VALUES (928, 'El Paraiso', 282);
INSERT INTO public.sgc_parroquias VALUES (929, 'Santa Isabel', 283);
INSERT INTO public.sgc_parroquias VALUES (930, 'Araguaney', 283);
INSERT INTO public.sgc_parroquias VALUES (931, 'El Jaguito', 283);
INSERT INTO public.sgc_parroquias VALUES (932, 'La Esperanza', 283);
INSERT INTO public.sgc_parroquias VALUES (933, 'Sabana Grande', 284);
INSERT INTO public.sgc_parroquias VALUES (934, 'Cheregue', 284);
INSERT INTO public.sgc_parroquias VALUES (935, 'Granados', 284);
INSERT INTO public.sgc_parroquias VALUES (936, 'El Socorro', 285);
INSERT INTO public.sgc_parroquias VALUES (937, 'Los Caprichos', 285);
INSERT INTO public.sgc_parroquias VALUES (938, 'Antonio Jose De Sucre', 285);
INSERT INTO public.sgc_parroquias VALUES (939, 'Campo Elias', 286);
INSERT INTO public.sgc_parroquias VALUES (940, 'Arnoldo Gabaldon', 286);
INSERT INTO public.sgc_parroquias VALUES (941, 'Santa Apolonia', 287);
INSERT INTO public.sgc_parroquias VALUES (942, 'La Ceiba', 287);
INSERT INTO public.sgc_parroquias VALUES (943, 'El Progreso', 287);
INSERT INTO public.sgc_parroquias VALUES (944, 'Tres De Febrero', 287);
INSERT INTO public.sgc_parroquias VALUES (945, 'Pampanito', 288);
INSERT INTO public.sgc_parroquias VALUES (946, 'Pampanito Ii', 288);
INSERT INTO public.sgc_parroquias VALUES (947, 'La Concepcion', 288);
INSERT INTO public.sgc_parroquias VALUES (948, 'Cm. Aroa', 289);
INSERT INTO public.sgc_parroquias VALUES (949, 'Cm. Chivacoa', 290);
INSERT INTO public.sgc_parroquias VALUES (950, 'Campo Elias', 290);
INSERT INTO public.sgc_parroquias VALUES (951, 'Cm. Nirgua', 291);
INSERT INTO public.sgc_parroquias VALUES (952, 'Salom', 291);
INSERT INTO public.sgc_parroquias VALUES (953, 'Temerla', 291);
INSERT INTO public.sgc_parroquias VALUES (954, 'Cm. San Felipe', 292);
INSERT INTO public.sgc_parroquias VALUES (955, 'Albarico', 292);
INSERT INTO public.sgc_parroquias VALUES (956, 'San Javier', 292);
INSERT INTO public.sgc_parroquias VALUES (957, 'Cm. Guama', 293);
INSERT INTO public.sgc_parroquias VALUES (958, 'Cm. Urachiche', 294);
INSERT INTO public.sgc_parroquias VALUES (959, 'Cm. Yaritagua', 295);
INSERT INTO public.sgc_parroquias VALUES (960, 'San Andres', 295);
INSERT INTO public.sgc_parroquias VALUES (961, 'Cm. Sabana De Parra', 296);
INSERT INTO public.sgc_parroquias VALUES (962, 'Cm. Boraure', 297);
INSERT INTO public.sgc_parroquias VALUES (963, 'Cm. Cocorote', 298);
INSERT INTO public.sgc_parroquias VALUES (964, 'Cm. Independencia', 299);
INSERT INTO public.sgc_parroquias VALUES (965, 'Cm. San Pablo', 300);
INSERT INTO public.sgc_parroquias VALUES (966, 'Cm. Yumare', 301);
INSERT INTO public.sgc_parroquias VALUES (967, 'Cm. Farriar', 302);
INSERT INTO public.sgc_parroquias VALUES (968, 'El Guayabo', 302);
INSERT INTO public.sgc_parroquias VALUES (969, 'General Urdaneta', 303);
INSERT INTO public.sgc_parroquias VALUES (970, 'Libertador', 303);
INSERT INTO public.sgc_parroquias VALUES (971, 'Manuel Guanipa Matos', 303);
INSERT INTO public.sgc_parroquias VALUES (972, 'Marcelino Briceño', 303);
INSERT INTO public.sgc_parroquias VALUES (973, 'San Timoteo', 303);
INSERT INTO public.sgc_parroquias VALUES (974, 'Pueblo Nuevo', 303);
INSERT INTO public.sgc_parroquias VALUES (975, 'Pedro Lucas Urribarri', 304);
INSERT INTO public.sgc_parroquias VALUES (976, 'Santa Rita', 304);
INSERT INTO public.sgc_parroquias VALUES (977, 'Jose Cenovio Urribarr', 304);
INSERT INTO public.sgc_parroquias VALUES (978, 'El Mene', 304);
INSERT INTO public.sgc_parroquias VALUES (979, 'Santa Cruz Del Zulia', 305);
INSERT INTO public.sgc_parroquias VALUES (980, 'Urribarri', 305);
INSERT INTO public.sgc_parroquias VALUES (981, 'Moralito', 305);
INSERT INTO public.sgc_parroquias VALUES (982, 'San Carlos Del Zulia', 305);
INSERT INTO public.sgc_parroquias VALUES (983, 'Santa Barbara', 305);
INSERT INTO public.sgc_parroquias VALUES (984, 'Luis De Vicente', 306);
INSERT INTO public.sgc_parroquias VALUES (985, 'Ricaurte', 306);
INSERT INTO public.sgc_parroquias VALUES (986, 'Mons.Marcos Sergio G', 306);
INSERT INTO public.sgc_parroquias VALUES (987, 'San Rafael', 306);
INSERT INTO public.sgc_parroquias VALUES (988, 'Las Parcelas', 306);
INSERT INTO public.sgc_parroquias VALUES (989, 'Tamare', 306);
INSERT INTO public.sgc_parroquias VALUES (990, 'La Sierrita', 306);
INSERT INTO public.sgc_parroquias VALUES (991, 'Bolivar', 307);
INSERT INTO public.sgc_parroquias VALUES (992, 'Coquivacoa', 307);
INSERT INTO public.sgc_parroquias VALUES (993, 'Cristo De Aranza', 307);
INSERT INTO public.sgc_parroquias VALUES (994, 'Chiquinquira', 307);
INSERT INTO public.sgc_parroquias VALUES (995, 'Santa Lucia', 307);
INSERT INTO public.sgc_parroquias VALUES (996, 'Olegario Villalobos', 307);
INSERT INTO public.sgc_parroquias VALUES (997, 'Juana De Avila', 307);
INSERT INTO public.sgc_parroquias VALUES (998, 'Caracciolo Parra Perez', 307);
INSERT INTO public.sgc_parroquias VALUES (999, 'Idelfonzo Vasquez', 307);
INSERT INTO public.sgc_parroquias VALUES (1000, 'Cacique Mara', 307);
INSERT INTO public.sgc_parroquias VALUES (1001, 'Cecilio Acosta', 307);
INSERT INTO public.sgc_parroquias VALUES (1002, 'Raul Leoni', 307);
INSERT INTO public.sgc_parroquias VALUES (1003, 'Francisco Eugenio B', 307);
INSERT INTO public.sgc_parroquias VALUES (1004, 'Manuel Dagnino', 307);
INSERT INTO public.sgc_parroquias VALUES (1005, 'Luis Hurtado Higuera', 307);
INSERT INTO public.sgc_parroquias VALUES (1006, 'Venancio Pulgar', 307);
INSERT INTO public.sgc_parroquias VALUES (1007, 'Antonio Borjas Romero', 307);
INSERT INTO public.sgc_parroquias VALUES (1008, 'San Isidro', 307);
INSERT INTO public.sgc_parroquias VALUES (1009, 'Faria', 308);
INSERT INTO public.sgc_parroquias VALUES (1010, 'San Antonio', 308);
INSERT INTO public.sgc_parroquias VALUES (1011, 'Ana Maria Campos', 308);
INSERT INTO public.sgc_parroquias VALUES (1012, 'San Jose', 308);
INSERT INTO public.sgc_parroquias VALUES (1013, 'Altagracia', 308);
INSERT INTO public.sgc_parroquias VALUES (1014, 'Goajira', 309);
INSERT INTO public.sgc_parroquias VALUES (1015, 'Elias Sanchez Rubio', 309);
INSERT INTO public.sgc_parroquias VALUES (1016, 'Sinamaica', 309);
INSERT INTO public.sgc_parroquias VALUES (1017, 'Alta Guajira', 309);
INSERT INTO public.sgc_parroquias VALUES (1018, 'San Jose De Perija', 310);
INSERT INTO public.sgc_parroquias VALUES (1019, 'Bartolome De Las Casas', 310);
INSERT INTO public.sgc_parroquias VALUES (1020, 'Libertad', 310);
INSERT INTO public.sgc_parroquias VALUES (1021, 'Rio Negro', 310);
INSERT INTO public.sgc_parroquias VALUES (1022, 'Gibraltar', 311);
INSERT INTO public.sgc_parroquias VALUES (1023, 'Heras', 311);
INSERT INTO public.sgc_parroquias VALUES (1024, 'M.Arturo Celestino A', 311);
INSERT INTO public.sgc_parroquias VALUES (1025, 'Romulo Gallegos', 311);
INSERT INTO public.sgc_parroquias VALUES (1026, 'Bobures', 311);
INSERT INTO public.sgc_parroquias VALUES (1027, 'El Batey', 311);
INSERT INTO public.sgc_parroquias VALUES (1028, 'Andres Bello (KM 48)', 312);
INSERT INTO public.sgc_parroquias VALUES (1029, 'Potreritos', 312);
INSERT INTO public.sgc_parroquias VALUES (1030, 'El Carmelo', 312);
INSERT INTO public.sgc_parroquias VALUES (1031, 'Chiquinquira', 312);
INSERT INTO public.sgc_parroquias VALUES (1032, 'Concepcion', 312);
INSERT INTO public.sgc_parroquias VALUES (1033, 'Eleazar Lopez C', 313);
INSERT INTO public.sgc_parroquias VALUES (1034, 'Alonso De Ojeda', 313);
INSERT INTO public.sgc_parroquias VALUES (1035, 'Venezuela', 313);
INSERT INTO public.sgc_parroquias VALUES (1036, 'Campo Lara', 313);
INSERT INTO public.sgc_parroquias VALUES (1037, 'Libertad', 313);
INSERT INTO public.sgc_parroquias VALUES (1038, 'Udon Perez', 314);
INSERT INTO public.sgc_parroquias VALUES (1039, 'Encontrados', 314);
INSERT INTO public.sgc_parroquias VALUES (1040, 'Donaldo Garcia', 315);
INSERT INTO public.sgc_parroquias VALUES (1041, 'Sixto Zambrano', 315);
INSERT INTO public.sgc_parroquias VALUES (1042, 'El Rosario', 315);
INSERT INTO public.sgc_parroquias VALUES (1043, 'Ambrosio', 316);
INSERT INTO public.sgc_parroquias VALUES (1044, 'German Rios Linares', 316);
INSERT INTO public.sgc_parroquias VALUES (1045, 'Jorge Hernandez', 316);
INSERT INTO public.sgc_parroquias VALUES (1046, 'La Rosa', 316);
INSERT INTO public.sgc_parroquias VALUES (1047, 'Punta Gorda', 316);
INSERT INTO public.sgc_parroquias VALUES (1048, 'Carmen Herrera', 316);
INSERT INTO public.sgc_parroquias VALUES (1049, 'San Benito', 316);
INSERT INTO public.sgc_parroquias VALUES (1050, 'Romulo Betancourt', 316);
INSERT INTO public.sgc_parroquias VALUES (1051, 'Aristides Calvani', 316);
INSERT INTO public.sgc_parroquias VALUES (1052, 'Raul Cuenca', 317);
INSERT INTO public.sgc_parroquias VALUES (1053, 'La Victoria', 317);
INSERT INTO public.sgc_parroquias VALUES (1054, 'Rafael Urdaneta', 317);
INSERT INTO public.sgc_parroquias VALUES (1055, 'Jose Ramon Yepez', 318);
INSERT INTO public.sgc_parroquias VALUES (1056, 'La Concepcion', 318);
INSERT INTO public.sgc_parroquias VALUES (1057, 'San Jose', 318);
INSERT INTO public.sgc_parroquias VALUES (1058, 'Mariano Parra Leon', 318);
INSERT INTO public.sgc_parroquias VALUES (1059, 'Monagas', 319);
INSERT INTO public.sgc_parroquias VALUES (1060, 'Isla De Toas', 319);
INSERT INTO public.sgc_parroquias VALUES (1061, 'Marcial Hernandez', 320);
INSERT INTO public.sgc_parroquias VALUES (1062, 'Francisco Ochoa', 320);
INSERT INTO public.sgc_parroquias VALUES (1063, 'San Francisco', 320);
INSERT INTO public.sgc_parroquias VALUES (1064, 'El Bajo', 320);
INSERT INTO public.sgc_parroquias VALUES (1065, 'Domitila Flores', 320);
INSERT INTO public.sgc_parroquias VALUES (1066, 'Los Cortijos', 320);
INSERT INTO public.sgc_parroquias VALUES (1067, 'Bari', 321);
INSERT INTO public.sgc_parroquias VALUES (1068, 'Jesus M Semprun', 321);
INSERT INTO public.sgc_parroquias VALUES (1069, 'Simon Rodriguez', 322);
INSERT INTO public.sgc_parroquias VALUES (1070, 'Carlos Quevedo', 322);
INSERT INTO public.sgc_parroquias VALUES (1071, 'Francisco J Pulgar', 322);
INSERT INTO public.sgc_parroquias VALUES (1072, 'Rafael Maria Baralt', 323);
INSERT INTO public.sgc_parroquias VALUES (1073, 'Manuel Manrique', 323);
INSERT INTO public.sgc_parroquias VALUES (1074, 'Rafael Urdaneta', 323);
INSERT INTO public.sgc_parroquias VALUES (1075, 'Fernando Giron Tovar', 324);
INSERT INTO public.sgc_parroquias VALUES (1076, 'Luis Alberto Gomez', 324);
INSERT INTO public.sgc_parroquias VALUES (1077, 'Parhueña', 324);
INSERT INTO public.sgc_parroquias VALUES (1078, 'Platanillal', 324);
INSERT INTO public.sgc_parroquias VALUES (1079, 'Cm. San Fernando De Ataba', 325);
INSERT INTO public.sgc_parroquias VALUES (1080, 'Ucata', 325);
INSERT INTO public.sgc_parroquias VALUES (1081, 'Yapacana', 325);
INSERT INTO public.sgc_parroquias VALUES (1082, 'Caname', 325);
INSERT INTO public.sgc_parroquias VALUES (1083, 'Cm. Maroa', 326);
INSERT INTO public.sgc_parroquias VALUES (1084, 'Victorino', 326);
INSERT INTO public.sgc_parroquias VALUES (1085, 'Comunidad', 326);
INSERT INTO public.sgc_parroquias VALUES (1086, 'Cm. San Carlos De Rio Neg', 327);
INSERT INTO public.sgc_parroquias VALUES (1087, 'Solano', 327);
INSERT INTO public.sgc_parroquias VALUES (1088, 'Cocuy', 327);
INSERT INTO public.sgc_parroquias VALUES (1089, 'Cm. Isla De Raton', 328);
INSERT INTO public.sgc_parroquias VALUES (1090, 'Samariapo', 328);
INSERT INTO public.sgc_parroquias VALUES (1091, 'Sipapo', 328);
INSERT INTO public.sgc_parroquias VALUES (1092, 'Munduapo', 328);
INSERT INTO public.sgc_parroquias VALUES (1093, 'Guayapo', 328);
INSERT INTO public.sgc_parroquias VALUES (1094, 'Cm. San Juan De Manapiare', 329);
INSERT INTO public.sgc_parroquias VALUES (1095, 'Alto Ventuari', 329);
INSERT INTO public.sgc_parroquias VALUES (1096, 'Medio Ventuari', 329);
INSERT INTO public.sgc_parroquias VALUES (1097, 'Bajo Ventuari', 329);
INSERT INTO public.sgc_parroquias VALUES (1098, 'Cm. La Esmeralda', 330);
INSERT INTO public.sgc_parroquias VALUES (1099, 'Huachamacare', 330);
INSERT INTO public.sgc_parroquias VALUES (1100, 'Marawaka', 330);
INSERT INTO public.sgc_parroquias VALUES (1101, 'Mavaca', 330);
INSERT INTO public.sgc_parroquias VALUES (1102, 'Sierra Parima', 330);
INSERT INTO public.sgc_parroquias VALUES (1103, 'San Jose', 331);
INSERT INTO public.sgc_parroquias VALUES (1104, 'Virgen Del Valle', 331);
INSERT INTO public.sgc_parroquias VALUES (1105, 'San Rafael', 331);
INSERT INTO public.sgc_parroquias VALUES (1106, 'Jose Vidal Marcano', 331);
INSERT INTO public.sgc_parroquias VALUES (1107, 'Leonardo Ruiz Pineda', 331);
INSERT INTO public.sgc_parroquias VALUES (1108, 'Mons. Argimiro Garcia', 331);
INSERT INTO public.sgc_parroquias VALUES (1109, 'Mcl.Antonio J De Sucre', 331);
INSERT INTO public.sgc_parroquias VALUES (1110, 'Juan Millan', 331);
INSERT INTO public.sgc_parroquias VALUES (1111, 'Pedernales', 332);
INSERT INTO public.sgc_parroquias VALUES (1112, 'Luis B Prieto Figuero', 332);
INSERT INTO public.sgc_parroquias VALUES (1113, 'Curiapo', 333);
INSERT INTO public.sgc_parroquias VALUES (1114, 'Santos De Abelgas', 333);
INSERT INTO public.sgc_parroquias VALUES (1115, 'Manuel Renaud', 333);
INSERT INTO public.sgc_parroquias VALUES (1116, 'Padre Barral', 333);
INSERT INTO public.sgc_parroquias VALUES (1117, 'Aniceto Lugo', 333);
INSERT INTO public.sgc_parroquias VALUES (1118, 'Almirante Luis Brion', 333);
INSERT INTO public.sgc_parroquias VALUES (1119, 'Imataca', 334);
INSERT INTO public.sgc_parroquias VALUES (1120, 'Romulo Gallegos', 334);
INSERT INTO public.sgc_parroquias VALUES (1121, 'Juan Bautista Arismen', 334);
INSERT INTO public.sgc_parroquias VALUES (1122, 'Manuel Piar', 334);
INSERT INTO public.sgc_parroquias VALUES (1123, '5 De Julio', 334);
INSERT INTO public.sgc_parroquias VALUES (1124, 'Caraballeda', 335);
INSERT INTO public.sgc_parroquias VALUES (1125, 'Carayaca', 335);
INSERT INTO public.sgc_parroquias VALUES (1126, 'Caruao', 335);
INSERT INTO public.sgc_parroquias VALUES (1127, 'Catia La Mar', 335);
INSERT INTO public.sgc_parroquias VALUES (1128, 'La Guaira', 335);
INSERT INTO public.sgc_parroquias VALUES (1129, 'Macuto', 335);
INSERT INTO public.sgc_parroquias VALUES (1130, 'Maiquetia', 335);
INSERT INTO public.sgc_parroquias VALUES (1131, 'Naiguata', 335);
INSERT INTO public.sgc_parroquias VALUES (1132, 'El Junko', 335);
INSERT INTO public.sgc_parroquias VALUES (1133, 'Pq Raul Leoni', 335);
INSERT INTO public.sgc_parroquias VALUES (1134, 'Pq Carlos Soublette', 335);


--
-- TOC entry 3634 (class 0 OID 56077)
-- Dependencies: 225
-- Data for Name: sgc_red_social; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_red_social VALUES (1, 'Whatsapp', false);
INSERT INTO public.sgc_red_social VALUES (2, 'Personal', false);
INSERT INTO public.sgc_red_social VALUES (3, 'Portal web ', false);
INSERT INTO public.sgc_red_social VALUES (4, 'Correo Electrónico', false);
INSERT INTO public.sgc_red_social VALUES (6, 'Llamada Telefónica', false);


--
-- TOC entry 3636 (class 0 OID 56082)
-- Dependencies: 227
-- Data for Name: sgc_registro_cgr; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3638 (class 0 OID 56087)
-- Dependencies: 229
-- Data for Name: sgc_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_roles VALUES (1, 'Director');
INSERT INTO public.sgc_roles VALUES (2, 'Analista');
INSERT INTO public.sgc_roles VALUES (3, 'Supervisor');
INSERT INTO public.sgc_roles VALUES (4, 'Coordinador');
INSERT INTO public.sgc_roles VALUES (5, 'Administrador');


--
-- TOC entry 3665 (class 0 OID 74897)
-- Dependencies: 256
-- Data for Name: sgc_seguimiento_caso; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3649 (class 0 OID 74659)
-- Dependencies: 240
-- Data for Name: sgc_tipo_beneficiarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_tipo_beneficiarios VALUES (1, 'Activos', true);
INSERT INTO public.sgc_tipo_beneficiarios VALUES (2, 'Jubilados', false);
INSERT INTO public.sgc_tipo_beneficiarios VALUES (3, 'Familiares', false);
INSERT INTO public.sgc_tipo_beneficiarios VALUES (4, 'Externos', false);
INSERT INTO public.sgc_tipo_beneficiarios VALUES (5, 'Institución', false);
INSERT INTO public.sgc_tipo_beneficiarios VALUES (6, 'Entes Adscritos', false);
INSERT INTO public.sgc_tipo_beneficiarios VALUES (7, 'Consejo Comunales', false);
INSERT INTO public.sgc_tipo_beneficiarios VALUES (8, 'Comuna', false);


--
-- TOC entry 3640 (class 0 OID 56107)
-- Dependencies: 231
-- Data for Name: sgc_tipoatencion_usu; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_tipoatencion_usu VALUES (2, 'Sugerencia', false);
INSERT INTO public.sgc_tipoatencion_usu VALUES (3, 'Queja', false);
INSERT INTO public.sgc_tipoatencion_usu VALUES (4, 'Reclamo', false);
INSERT INTO public.sgc_tipoatencion_usu VALUES (7, 'Ayuda Económica', false);
INSERT INTO public.sgc_tipoatencion_usu VALUES (5, 'Denuncia', false);
INSERT INTO public.sgc_tipoatencion_usu VALUES (1, 'Asesoría', true);
INSERT INTO public.sgc_tipoatencion_usu VALUES (6, 'Solicitud (donacion)', false);


--
-- TOC entry 3651 (class 0 OID 74689)
-- Dependencies: 242
-- Data for Name: sgc_tipoatenciondetalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_tipoatenciondetalle VALUES (3, 'Insumos Medicos', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (1, 'Servicios Funebres', 7, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (2, 'Servicios Médicos', 7, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (4, 'Apoyo Técnico', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (5, 'Tecnología', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (6, 'Linea Blanca', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (7, 'Juguetes', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (8, 'Calzado Plan Z', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (9, 'Adecuaciones Viviendas', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (10, 'Alimentos', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (11, 'Planc', 6, false);
INSERT INTO public.sgc_tipoatenciondetalle VALUES (12, 'prueba freddy', 6, false);


--
-- TOC entry 3642 (class 0 OID 56121)
-- Dependencies: 233
-- Data for Name: sgc_usuario_operador; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_usuario_operador VALUES (1, 'ADMIN', 'ADMIN', '$2y$10$sdBk.kGGbPTr4hrs0IX5BuTW7J.KpaQx/U.KhdXn/3yel2JYsZqXG', 'admin@sapi.gob.ve', 5, false, 'DIRECTOR');


--
-- TOC entry 3644 (class 0 OID 56128)
-- Dependencies: 235
-- Data for Name: sgc_usuario_token; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sgc_usuario_token VALUES (1, 18, '34a698892c5aea5d5683176ce5256b8c');


--
-- TOC entry 3646 (class 0 OID 56134)
-- Dependencies: 237
-- Data for Name: sta_usuarios_visitas; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3699 (class 0 OID 0)
-- Dependencies: 245
-- Name: sgc_auditoria_sistema_audi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_auditoria_sistema_audi_id_seq', 1, false);


--
-- TOC entry 3700 (class 0 OID 0)
-- Dependencies: 249
-- Name: sgc_casos_denuncias_denu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_casos_denuncias_denu_id_seq', 1, false);


--
-- TOC entry 3701 (class 0 OID 0)
-- Dependencies: 247
-- Name: sgc_casos_idcaso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_casos_idcaso_seq', 1, false);


--
-- TOC entry 3702 (class 0 OID 0)
-- Dependencies: 251
-- Name: sgc_casos_remitidos_casos_re_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_casos_remitidos_casos_re_id_seq', 1, false);


--
-- TOC entry 3703 (class 0 OID 0)
-- Dependencies: 257
-- Name: sgc_direcciones_administrativas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_direcciones_administrativas_id_seq', 1, false);


--
-- TOC entry 3704 (class 0 OID 0)
-- Dependencies: 253
-- Name: sgc_documentos_casos_docu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_documentos_casos_docu_id_seq', 1, false);


--
-- TOC entry 3705 (class 0 OID 0)
-- Dependencies: 210
-- Name: sgc_ente_asdcrito_ente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_ente_asdcrito_ente_id_seq', 2, true);


--
-- TOC entry 3706 (class 0 OID 0)
-- Dependencies: 212
-- Name: sgc_estados_estadoid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_estados_estadoid_seq', 1, false);


--
-- TOC entry 3707 (class 0 OID 0)
-- Dependencies: 243
-- Name: sgc_estatus_idest_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_estatus_idest_seq', 5, true);


--
-- TOC entry 3708 (class 0 OID 0)
-- Dependencies: 214
-- Name: sgc_estatus_llamadas_idestllam_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_estatus_llamadas_idestllam_seq', 1, false);


--
-- TOC entry 3709 (class 0 OID 0)
-- Dependencies: 216
-- Name: sgc_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_migrations_id_seq', 17, true);


--
-- TOC entry 3710 (class 0 OID 0)
-- Dependencies: 218
-- Name: sgc_municipio_municipioid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_municipio_municipioid_seq', 1, false);


--
-- TOC entry 3711 (class 0 OID 0)
-- Dependencies: 220
-- Name: sgc_oficinas_idofi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_oficinas_idofi_seq', 1, false);


--
-- TOC entry 3712 (class 0 OID 0)
-- Dependencies: 222
-- Name: sgc_paises_paisid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_paises_paisid_seq', 1, false);


--
-- TOC entry 3713 (class 0 OID 0)
-- Dependencies: 224
-- Name: sgc_parroquias_parroquiaid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_parroquias_parroquiaid_seq', 1, false);


--
-- TOC entry 3714 (class 0 OID 0)
-- Dependencies: 226
-- Name: sgc_red_social_idrrss_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_red_social_idrrss_seq', 6, true);


--
-- TOC entry 3715 (class 0 OID 0)
-- Dependencies: 228
-- Name: sgc_registro_cgr_id_cgr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_registro_cgr_id_cgr_seq', 1, false);


--
-- TOC entry 3716 (class 0 OID 0)
-- Dependencies: 230
-- Name: sgc_roles_idrol_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_roles_idrol_seq', 8, true);


--
-- TOC entry 3717 (class 0 OID 0)
-- Dependencies: 255
-- Name: sgc_seguimiento_caso_idsegcas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_seguimiento_caso_idsegcas_seq', 1, false);


--
-- TOC entry 3718 (class 0 OID 0)
-- Dependencies: 239
-- Name: sgc_tipo_beneficiarios_tipo_beneficiario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_tipo_beneficiarios_tipo_beneficiario_id_seq', 8, true);


--
-- TOC entry 3719 (class 0 OID 0)
-- Dependencies: 241
-- Name: sgc_tipoatencioindetalle_tipo_atend_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_tipoatencioindetalle_tipo_atend_id_seq', 14, true);


--
-- TOC entry 3720 (class 0 OID 0)
-- Dependencies: 232
-- Name: sgc_tipoatencion_usu_tipo_anten_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_tipoatencion_usu_tipo_anten_id_seq', 6, true);


--
-- TOC entry 3721 (class 0 OID 0)
-- Dependencies: 234
-- Name: sgc_usuario_operador_idusuopr_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_usuario_operador_idusuopr_seq', 1, true);


--
-- TOC entry 3722 (class 0 OID 0)
-- Dependencies: 236
-- Name: sgc_usuario_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sgc_usuario_token_id_seq', 1, true);


--
-- TOC entry 3723 (class 0 OID 0)
-- Dependencies: 238
-- Name: sta_usuarios_visitas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sta_usuarios_visitas_id_seq', 1, false);


--
-- TOC entry 3453 (class 2606 OID 74820)
-- Name: sgc_auditoria_sistema auditoria_de_sistema_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_auditoria_sistema
    ADD CONSTRAINT auditoria_de_sistema_pkey PRIMARY KEY (audi_id);


--
-- TOC entry 3455 (class 2606 OID 74832)
-- Name: sgc_casos pk_sgc_casos; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos
    ADD CONSTRAINT pk_sgc_casos PRIMARY KEY (idcaso);


--
-- TOC entry 3419 (class 2606 OID 56170)
-- Name: sgc_estados pk_sgc_estados; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_estados
    ADD CONSTRAINT pk_sgc_estados PRIMARY KEY (estadoid);


--
-- TOC entry 3451 (class 2606 OID 74741)
-- Name: sgc_estatus pk_sgc_estatus; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_estatus
    ADD CONSTRAINT pk_sgc_estatus PRIMARY KEY (idest);


--
-- TOC entry 3421 (class 2606 OID 56174)
-- Name: sgc_estatus_llamadas pk_sgc_estatus_llamadas; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_estatus_llamadas
    ADD CONSTRAINT pk_sgc_estatus_llamadas PRIMARY KEY (idestllam);


--
-- TOC entry 3423 (class 2606 OID 56176)
-- Name: sgc_migrations pk_sgc_migrations; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_migrations
    ADD CONSTRAINT pk_sgc_migrations PRIMARY KEY (id);


--
-- TOC entry 3425 (class 2606 OID 56178)
-- Name: sgc_municipio pk_sgc_municipio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_municipio
    ADD CONSTRAINT pk_sgc_municipio PRIMARY KEY (municipioid);


--
-- TOC entry 3427 (class 2606 OID 56180)
-- Name: sgc_oficinas pk_sgc_oficinas; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_oficinas
    ADD CONSTRAINT pk_sgc_oficinas PRIMARY KEY (idofi);


--
-- TOC entry 3429 (class 2606 OID 56182)
-- Name: sgc_paises pk_sgc_paises; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_paises
    ADD CONSTRAINT pk_sgc_paises PRIMARY KEY (paisid);


--
-- TOC entry 3431 (class 2606 OID 56184)
-- Name: sgc_parroquias pk_sgc_parroquias; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_parroquias
    ADD CONSTRAINT pk_sgc_parroquias PRIMARY KEY (parroquiaid);


--
-- TOC entry 3433 (class 2606 OID 56186)
-- Name: sgc_red_social pk_sgc_red_social; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_red_social
    ADD CONSTRAINT pk_sgc_red_social PRIMARY KEY (red_s_id);


--
-- TOC entry 3437 (class 2606 OID 56188)
-- Name: sgc_roles pk_sgc_roles; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_roles
    ADD CONSTRAINT pk_sgc_roles PRIMARY KEY (idrol);


--
-- TOC entry 3463 (class 2606 OID 74905)
-- Name: sgc_seguimiento_caso pk_sgc_seguimiento_caso; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_seguimiento_caso
    ADD CONSTRAINT pk_sgc_seguimiento_caso PRIMARY KEY (idsegcas);


--
-- TOC entry 3441 (class 2606 OID 56196)
-- Name: sgc_usuario_operador pk_sgc_usuario_operador; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_usuario_operador
    ADD CONSTRAINT pk_sgc_usuario_operador PRIMARY KEY (idusuopr);


--
-- TOC entry 3457 (class 2606 OID 74876)
-- Name: sgc_casos_denuncias sgc_casos_denuncias_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos_denuncias
    ADD CONSTRAINT sgc_casos_denuncias_pkey PRIMARY KEY (denu_id);


--
-- TOC entry 3459 (class 2606 OID 74886)
-- Name: sgc_casos_remitidos sgc_casos_remitidos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos_remitidos
    ADD CONSTRAINT sgc_casos_remitidos_pkey PRIMARY KEY (casos_re_id);


--
-- TOC entry 3461 (class 2606 OID 74895)
-- Name: sgc_documentos_casos sgc_documentos_casos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_documentos_casos
    ADD CONSTRAINT sgc_documentos_casos_pkey PRIMARY KEY (docu_id);


--
-- TOC entry 3417 (class 2606 OID 56204)
-- Name: sgc_ente_asdcrito sgc_ente_asdcrito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_ente_asdcrito
    ADD CONSTRAINT sgc_ente_asdcrito_pkey PRIMARY KEY (ente_id);


--
-- TOC entry 3435 (class 2606 OID 56206)
-- Name: sgc_registro_cgr sgc_registro_cgr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_registro_cgr
    ADD CONSTRAINT sgc_registro_cgr_pkey PRIMARY KEY (id_cgr);


--
-- TOC entry 3447 (class 2606 OID 74666)
-- Name: sgc_tipo_beneficiarios sgc_tipo_beneficiarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_tipo_beneficiarios
    ADD CONSTRAINT sgc_tipo_beneficiarios_pkey PRIMARY KEY (tipo_beneficiario_id);


--
-- TOC entry 3449 (class 2606 OID 74697)
-- Name: sgc_tipoatenciondetalle sgc_tipoatencioindetalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_tipoatenciondetalle
    ADD CONSTRAINT sgc_tipoatencioindetalle_pkey PRIMARY KEY (tipo_atend_id);


--
-- TOC entry 3439 (class 2606 OID 56208)
-- Name: sgc_tipoatencion_usu sgc_tipoatencion_usu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_tipoatencion_usu
    ADD CONSTRAINT sgc_tipoatencion_usu_pkey PRIMARY KEY (tipo_aten_id);


--
-- TOC entry 3443 (class 2606 OID 56212)
-- Name: sgc_usuario_token sgc_usuario_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_usuario_token
    ADD CONSTRAINT sgc_usuario_token_pkey PRIMARY KEY (id_usuario);


--
-- TOC entry 3445 (class 2606 OID 56214)
-- Name: sta_usuarios_visitas sta_usuarios_visitas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sta_usuarios_visitas
    ADD CONSTRAINT sta_usuarios_visitas_pkey PRIMARY KEY (id);


--
-- TOC entry 3465 (class 2606 OID 74931)
-- Name: sgc_direcciones_administrativas ubicacion_administrativa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_direcciones_administrativas
    ADD CONSTRAINT ubicacion_administrativa_pkey PRIMARY KEY (id);


--
-- TOC entry 3470 (class 2606 OID 74833)
-- Name: sgc_casos sgc_casos_estadoid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos
    ADD CONSTRAINT sgc_casos_estadoid_foreign FOREIGN KEY (estadoid) REFERENCES public.sgc_estados(estadoid);


--
-- TOC entry 3471 (class 2606 OID 74838)
-- Name: sgc_casos sgc_casos_idest_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos
    ADD CONSTRAINT sgc_casos_idest_foreign FOREIGN KEY (idest) REFERENCES public.sgc_estatus(idest);


--
-- TOC entry 3472 (class 2606 OID 74843)
-- Name: sgc_casos sgc_casos_idrrss_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos
    ADD CONSTRAINT sgc_casos_idrrss_foreign FOREIGN KEY (idrrss) REFERENCES public.sgc_red_social(red_s_id);


--
-- TOC entry 3473 (class 2606 OID 74848)
-- Name: sgc_casos sgc_casos_idusuopr_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos
    ADD CONSTRAINT sgc_casos_idusuopr_foreign FOREIGN KEY (idusuopr) REFERENCES public.sgc_usuario_operador(idusuopr);


--
-- TOC entry 3474 (class 2606 OID 74853)
-- Name: sgc_casos sgc_casos_municipioid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos
    ADD CONSTRAINT sgc_casos_municipioid_foreign FOREIGN KEY (municipioid) REFERENCES public.sgc_municipio(municipioid);


--
-- TOC entry 3475 (class 2606 OID 74858)
-- Name: sgc_casos sgc_casos_parroquiaid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_casos
    ADD CONSTRAINT sgc_casos_parroquiaid_foreign FOREIGN KEY (parroquiaid) REFERENCES public.sgc_parroquias(parroquiaid);


--
-- TOC entry 3466 (class 2606 OID 56247)
-- Name: sgc_estados sgc_estados_paisid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_estados
    ADD CONSTRAINT sgc_estados_paisid_foreign FOREIGN KEY (paisid) REFERENCES public.sgc_paises(paisid);


--
-- TOC entry 3467 (class 2606 OID 56252)
-- Name: sgc_municipio sgc_municipio_estadoid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_municipio
    ADD CONSTRAINT sgc_municipio_estadoid_foreign FOREIGN KEY (estadoid) REFERENCES public.sgc_estados(estadoid);


--
-- TOC entry 3468 (class 2606 OID 56257)
-- Name: sgc_parroquias sgc_parroquias_municipioid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_parroquias
    ADD CONSTRAINT sgc_parroquias_municipioid_foreign FOREIGN KEY (municipioid) REFERENCES public.sgc_municipio(municipioid);


--
-- TOC entry 3476 (class 2606 OID 74906)
-- Name: sgc_seguimiento_caso sgc_seguimiento_caso_idcaso_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_seguimiento_caso
    ADD CONSTRAINT sgc_seguimiento_caso_idcaso_foreign FOREIGN KEY (idcaso) REFERENCES public.sgc_casos(idcaso);


--
-- TOC entry 3477 (class 2606 OID 74911)
-- Name: sgc_seguimiento_caso sgc_seguimiento_caso_idestllam_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_seguimiento_caso
    ADD CONSTRAINT sgc_seguimiento_caso_idestllam_foreign FOREIGN KEY (idestllam) REFERENCES public.sgc_estatus_llamadas(idestllam);


--
-- TOC entry 3478 (class 2606 OID 74916)
-- Name: sgc_seguimiento_caso sgc_seguimiento_caso_idusuopr_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_seguimiento_caso
    ADD CONSTRAINT sgc_seguimiento_caso_idusuopr_foreign FOREIGN KEY (idusuopr) REFERENCES public.sgc_usuario_operador(idusuopr);


--
-- TOC entry 3469 (class 2606 OID 56287)
-- Name: sgc_usuario_operador sgc_usuario_operador_idrol_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sgc_usuario_operador
    ADD CONSTRAINT sgc_usuario_operador_idrol_foreign FOREIGN KEY (idrol) REFERENCES public.sgc_roles(idrol);


--
-- TOC entry 3673 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2024-08-25 20:01:58 -04

--
-- PostgreSQL database dump complete
--

