/*
 *Este es el document ready
 */
$(function() {
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let edad_min = $('#edad_min').val();
    let edad_max = $('#edad_max').val();
    let tipo_atencion = $('#tipo-atencion-usu').val();
    let via_atencion = $('#via-atencion').val();
    let tipo_beneficiario = $('#t-beneficiario').val();
    let atencion_cuidadano = $('#office').val();
    let sexo = $('#sexo').val();
    if (desde == '' && hasta == '') {

        desde = 'null'
        hasta = 'null'
    }

    if (edad_min == '' && edad_max == '') {

        edad_min = 'null'
        edad_max = 'null'
    }
    listar_reportes(desde, hasta,tipo_atencion, sexo,edad_min,edad_max);
    llenar_Tipo_Atencion(Event);
    llenar_via_atencion(Event);
    llenar_Tipo_Beneficiarios(Event);
    llenar_Estados(Event);
});

function listar_reportes(desde = null, hasta = null, tipo_atencion = null, sexo = null, via_atencion = null, tipo_beneficiario = 0,atencion_cuidadano = 0,estatus=0,detalle_atencion=0,tipo_de_persona=0,id_estado=0,edad_min=null,edad_max=null,nombre_atencion=null, nombresexo=null, nombre_via_atencion=null, nombre_tipo_beneficiario=null,nombre_aten_cuidadano=null,nombre_estatus=null,nombre_detalle_atencion=null,nombre_tipo_de_persona=null,nombre_estado=null) {

   
    // Convertir la fecha
    var fechaOriginal = desde;
    var dataFormatada_desde = moment(fechaOriginal).format("DD-MM-YYYY");
    var fechaOriginal2 = hasta;
    var dataFormatada_hasta = moment(fechaOriginal2).format("DD-MM-YYYY");
    var encabezado = '';
// Ancho máximo permitido en el PDF (en puntos)
const maxWidth = 800;
// Longitud del texto
const textLength = encabezado.length;
// Calcular el ancho del texto
const textWidth = textLength * 5; // Aproximadamente 5 puntos por carácter
// Agregar salto de línea si el texto se acerca al borde
if (textWidth > maxWidth) {
  const words = encabezado.split(' ');
  const lines = [];
  let currentLine = '';
  for (const word of words) {
    if (currentLine.length + word.length + 1 > maxWidth / 5) {
      lines.push(currentLine);
      currentLine = word;
    } else {
      if (currentLine) {
        currentLine += ' ';
      }
      currentLine += word;
   }

  }
  lines.push(currentLine);
  encabezado = lines.join('\n');
}



    if (dataFormatada_desde != 'Invalid date' && dataFormatada_hasta != 'Invalid date') {
        encabezado = encabezado + 'Desde:' + ' ' + dataFormatada_desde + ' ' + ' ' + 'hasta' + ' ' + ' ' + dataFormatada_hasta + ' ' + ' ';
    }
   
    if (tipo_atencion != null &&tipo_atencion!='' ) {
        encabezado = encabezado + 'Tipo de Atencion:' + ' ' + nombre_atencion + ' ';
    }

    if (nombre_detalle_atencion != null  &&nombre_detalle_atencion!=''&& nombre_detalle_atencion!='Seleccione') {
        encabezado = encabezado + 'Detalle de Atencion:' + ' ' + nombre_detalle_atencion + ' ';
    }
    
    
    if (nombre_tipo_de_persona != null && nombre_tipo_de_persona!=''&& nombre_tipo_de_persona!='Seleccione') {
        encabezado = encabezado + 'Tipo de Persona:' + ' ' + nombre_tipo_de_persona + ' ';
    }
    
    if (nombre_estado != null&& nombre_estado!=''&& nombre_estado!='Seleccione') {
        encabezado = encabezado + 'Estado:' + ' ' + nombre_estado + ' ';
    }
    
    if (sexo != null&& sexo!='') {
        encabezado = encabezado + 'Sexo:' + ' ' + nombresexo + ' ';
    }

    if (via_atencion !=null&&via_atencion !='null') {
        encabezado = encabezado + 'Via de atencion:' + ' ' + nombre_via_atencion + ' ';
    }
    if (edad_min !='null'&& edad_max!='null'&& edad_min !=null&& edad_max!=null ) {
        encabezado = encabezado + 'Edad:' + ' '+'Entre'+' '+edad_min+' '+'y'+' '+edad_max+' ';
    }
   
    if (tipo_beneficiario !='null' && tipo_beneficiario !=null) {
        encabezado = encabezado + 'Tipo beneficiario :' + ' ' + nombre_tipo_beneficiario + ' ';
    }
    if (atencion_cuidadano != null && atencion_cuidadano != 0) {
        encabezado = encabezado + 'Atencion Cuidadano :' + ' ' + nombre_aten_cuidadano+ ' ';
    }

    if (estatus != null && estatus != 0) {
        encabezado = encabezado + 'Estatus :' + ' ' + nombre_estatus+ ' ';
    }

    let ruta_imagen = rootpath;
    var table = $('#table_casos').DataTable({
        responsive: true,
        dom: "Bfrtip",
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'
                },
            },
            buttons: [{
                    //definimos estilos del boton de pd
                    extend: "pdf",
                    text: 'PDF',
                    className: 'btn-xs btn-dark',
                    orientation: 'landscape',
                    pageSize: 'LETTER',
                    header: true,
                    footer: true,
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6],
                    },
                    alignment: 'center',

                    customize: function(doc) {
                        
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'
                        }
                        doc.styles['td:nth-child(2)'] = {
                                width: '130px',
                                'max-width': '130px'
                            },
                            doc.styles.tableHeader = {
                                fillColor: '#4c8aa0',
                                color: 'white',
                                alignment: 'center'
                            },
                            // Create a header
                            doc.pageMargins = [100, 120, 0, 70];
                        doc['header'] = (function(page, pages) {
                            doc.styles.title = {
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                                pageBreak: 'auto',
                            }
                            return {
                                columns: [
 
                                    {
                                        margin: [10, 3, 40, 40],
                                        image: ruta_imagen,
                                        width: 780,
                                        height: 46,
                                        pageBreak: 'auto'

                                    },
                                    {
                                        margin: [-800, 50, -25, 0],
                                        color: '#4c8aa0',
                                        fontSize: '18',
                                        pageBreak: 'auto',
                                        alignment: 'center',
                                        text: 'Consolidado de Casos',
                                        fontSize: 18,
                                    },
                                    {

                                        
                        
                                        margin: [-750, 80, -25, 0],
                                        text: encabezado,
                        
                                       
                        
                                    },
                                ],
                            }
                        });
                        // Create a footer
                        doc['footer'] = (function(page, pages) {
                            return {
                                columns: [{
                                    alignment: 'center',
                                    text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }],
                            }
                        });

                    },
                },

                {
                    //definimos estilos del boton de excel
                    extend: "excel",
                    text: 'Excel',
                    className: 'btn-xs btn-dark',
                    title: 'Consolidado de Casos',

                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6],
                    },
                    excelStyles: {
                        "template": [
                            "blue_medium",
                            "header_blue",
                            "title_medium"
                        ]
                    },

                }
            ]
        },
        "order": [
            [0, "desc"]
        ],
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],
        "ordering": true,
        "info": true,
        "autoWidth": true,
        //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
        "ajax": {
            "url": "/reporte_consolidado/" + desde + '/' + hasta + '/' + tipo_atencion + '/' + sexo + '/' + via_atencion + '/' + tipo_beneficiario+ '/' +atencion_cuidadano+'/'+estatus+'/'+detalle_atencion+'/'+tipo_de_persona+'/'+id_estado+'/'+edad_min+'/'+edad_max,
            "type": "GET",
            dataSrc: ''
        },
        "columns": [
            //{data:'cedula_beneficiario'},
            { data: 'cedula' },
            { data: 'tipo_beneficiario_nombre' },
            { data: 'nombre' },
            { data: 'casotel' },
            { data: 'tipo_aten_nombre' },
            { data: 'casofec' },
            { data: 'estnom' },
            { data: 'user_name' },

        ],

        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }, ]

        },
    });
}

//FUNCION PARA LLENAR EL COMBO DE LAS REDES SOCIALES
function llenar_via_atencion(e, id) {
    e.preventDefault;
    url = "/listar_Red_Social";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#via-atencion").empty();
                $("#via-atencion").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#via-atencion").append(
                            "<option value=" +
                            item.red_s_id +
                            ">" +
                            item.red_s_nom +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $("#via-atencion").append(
                                "<option value=" +
                                item.red_s_id +
                                " selected>" +
                                item.red_s_nom +
                                "</option>"
                            );
                        } else {
                            $("#via-atencion").append(
                                "<option value=" +
                                item.red_s_id +
                                ">" +
                                item.red_s_nom +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            //alert(xhr.status);
           //alert(errorThrown);
        },
    });
}
//FUNCION PARA LLENAR EL COMBO TIPO DE PROPIEDAD INTELECTUAL
function llenar_Propiedad_Intelectual(e, id) {
    e.preventDefault;
    url = "/Listar_Propiedad_Intelectual";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#tipo-pi").empty();
                $("#tipo-pi").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#tipo-pi").append(
                            "<option value=" +
                            item.tipo_prop_id +
                            ">" +
                            item.tipo_prop_nombre +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $("#tipo-pi").append(
                                "<option value=" +
                                item.tipo_prop_id +
                                " selected>" +
                                item.tipo_prop_nombre +
                                "</option>"
                            );
                        } else {
                            $("#tipo-pi").append(
                                "<option value=" +
                                item.tipo_prop_id +
                                ">" +
                                item.tipo_prop_nombre +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            //alert(xhr.status);
           //alert(errorThrown);
        },
    });
}





 //FUNCION PARA LLENAR EL COMBO TIPO DE BENEFICIARIOS
 function llenar_Tipo_Beneficiarios(e, id) {
    e.preventDefault;
    url = "/Listar_Tipo_Beneficiarios_filtro";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
             $("#t-beneficiario").empty();
                $("#t-beneficiario").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#t-beneficiario").append(
                            "<option value=" +
                            item.tipo_beneficiario_id+
                            ">" +
                            item.tipo_beneficiario_nombre +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id=== ente_adscrito_id) {
                            $("#t-beneficiario").append(
                                "<option value=" +
                                item.tipo_beneficiario_id+
                                " selected>" +
                                item.tipo_beneficiario_nombre +
                                "</option>"
                            );
                        } else {
                            $("#t-beneficiario").append(
                                "<option value=" +
                                item.tipo_beneficiario_id+
                                ">" +
                                item.tipo_beneficiario_nombre +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            
        },
    });
}





 //FUNCION PARA LLENAR EL COMBO TIPO DE ATENCION USUARIO
 function llenar_Tipo_Atencion(e, id) {
    e.preventDefault;
    url = "/Listar_Tipo_Atencion_filtro";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#tipo-atencion-usu").empty();
                $("#tipo-atencion-usu").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#tipo-atencion-usu").append(
                            "<option value=" +
                            item.tipo_aten_id +
                            ">" +
                            item.tipo_aten_nombre +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $("#tipo-atencion-usu").append(
                                "<option value=" +
                                item.tipo_aten_id +
                                " selected>" +
                                item.tipo_aten_nombre +
                                "</option>"
                            );
                        } else {
                            $("#tipo-atencion-usu").append(
                                "<option value=" +
                                item.tipo_aten_id +
                                ">" +
                                item.tipo_aten_nombre +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            
        },
    });
}


$("#tipo-atencion-usu").on('change', function(e)
{
    document.getElementById("detalles_atencion").disabled=false;
     $("#hijos_tipoatencion").val('NO');
    id_tipo_atencion=$('#tipo-atencion-usu option:selected').val(); 
    llenar_detalle_atencion(e,id_tipo_atencion)
      
}); 

function  llenar_detalle_atencion(e,id_tipo_atencion)
{

    e.preventDefault;
    
      url='/Listar_Detalle_Atencion_filtro';
       $.ajax
      ({
           url:url,
           method:'GET',
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
          
if(data.length>=1)
{
     $('#detalles_atencion').empty();
     $('#detalles_atencion').append('<option value=0  selected disabled>Seleccione</option>');   
     if(id_tipo_atencion===undefined)
    {
      
      
         $.each(data, function(i, item)
         {
           $(".detelle_atencion").hide();
              //console.log(data)
              $('#detalles_atencion').append('<option value='+item.tipo_atend_id+'>'+item.tipo_atend_nombre+'</option>');

         });
    }
    else
    {
       $(".detelle_atencion").hide();
       data=data.filter(dato=>dato.tipo_aten_id==id_tipo_atencion);
       //console.log(buscar);
          $.each(data, function(i, item)
          {
          
   
           $(".detelle_atencion").show();
           $("#hijos_tipoatencion").val('SI');
           $('#detalles_atencion').append('<option value='+item.tipo_atend_id+'>'+item.tipo_atend_nombre+'</option>');    
         });
    }
}      
},
error:function(xhr, status, errorThrown)
{
    alert(xhr.status);
    alert(errorThrown);
}
});
}

$(document).on('click', '.consultar', function(e) {
    e.preventDefault();
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let via_atencion = $('#via-atencion').val();
    let tipo_beneficiario = $('#t-beneficiario').val();
    let atencion_cuidadano = $('#office').val();
    let estatus = $('#estatus-caso').val();
    let tipo_atencion = $('#tipo-atencion-usu').val();
    let detalle_atencion = $('#detalles_atencion').val();
    let tipo_de_persona = $('#tipo-persona').val();
    let id_estado = $('#estado-caso').val();
    let sexo = $('#sexo').val();
    let nombre_atencion = $('#tipo-atencion-usu option:selected').text();
    let nombresexo = $('#sexo option:selected').text();
    let nombre_via_atencion = $('#via-atencion option:selected').text();
    let nombre_tipo_beneficiario = $('#t-beneficiario option:selected').text();
    let nombre_aten_cuidadano = $('#office option:selected').text();
    let nombre_estatus = $('#estatus-caso option:selected').text();
    let nombre_detalle_atencion = $('#detalles_atencion option:selected').text();
    let nombre_tipo_de_persona = $('#tipo-persona option:selected').text();
    let nombre_estado = $('#estado-caso option:selected').text();
 
    if (desde == '') {
        desde = 'null'
    }
    if (hasta == '') {
        hasta = 'null'
    }

    if (tipo_atencion == ''||tipo_atencion == 'Seleccione') {
        tipo_atencion = 'null'
    }
    if (via_atencion == ''||via_atencion == 'Seleccione') {
        via_atencion = 'null'
    }

    if (tipo_beneficiario == ''||tipo_beneficiario == 'Seleccione') {
        tipo_beneficiario = 'null'
    }











    let edad_min = $('#edad_min').val();
    let edad_max = $('#edad_max').val();
    
    if (edad_min == '' && edad_max == '') {
        edad_min = 'null';
        edad_max = 'null';
    } 
    
    if (edad_min >= '0' && edad_max == '') {
        alert('Debe indicar el campo "Edad Hasta"');
    } else if (edad_min == '' && edad_max != '') {
        alert('Debe indicar el campo "Edad Desde"');
    } else if (parseInt(edad_max) < parseInt(edad_min)) {
        alert('El campo "Edad Desde" es mayor al campo "Edad Hasta"');
    }else
    {
        $("#table_casos").dataTable().fnDestroy();
        listar_reportes(desde, hasta,tipo_atencion, sexo,via_atencion,tipo_beneficiario,atencion_cuidadano,estatus,detalle_atencion,tipo_de_persona,id_estado,edad_min,edad_max,nombre_atencion, nombresexo, nombre_via_atencion, nombre_tipo_beneficiario,nombre_aten_cuidadano,nombre_estatus,nombre_detalle_atencion,nombre_tipo_de_persona,nombre_estado);
    }
})
$(document).on('click', '.limpiar', function(e) {
    e.preventDefault();
    location.reload();

})

 //FUNCION PARA LLENAR EL COMBO ESTADOS
 function llenar_Estados(e, id) {
    e.preventDefault;
    url = "/llenar_Estados";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $("#estado-caso").empty();
                $("#estado-caso").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#estado-caso").append(
                            "<option value=" +
                            item.estadoid +
                            ">" +
                            item.estadonom +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $("#estado-caso").append(
                                "<option value=" +
                                item.estadoid +
                                " selected>" +
                                item.estadonom +
                                "</option>"
                            );
                        } else {
                            $("#estado-caso").append(
                                "<option value=" +
                                item.estadoid +
                                ">" +
                                item.estadonom +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
         
        },
    });
}