$(function() {

    // let tipo_atencion_usu = $("#tipo-atencion-usu").val();
 
     llenar_Tipo_Atencion(Event);
     llenar_Estados(Event);
     llenar_Red_social(Event);
     llenar_Entes_asdcritos(Event);
    // llenar_Tipo_Solicitud(Event);
   //  llenar_Tipo_Ayuda_Economica(Event);
     llenar_Tipo_Beneficiarios(Event);
 });
 








 //FUNCION PARA LLENAR EL COMBO TIPO DE BENEFICIARIOS
 function llenar_Tipo_Beneficiarios(e, id) {
    e.preventDefault;
    url = "/Listar_Tipo_Beneficiarios_filtro";
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
             $("#t-beneficiario").empty();
                $("#t-beneficiario").append(
                    "<option value=0  selected disabled>Seleccione</option>"
                );
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $("#t-beneficiario").append(
                            "<option value=" +
                            item.tipo_beneficiario_id+
                            ">" +
                            item.tipo_beneficiario_nombre +
                            "</option>"
                        );
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id=== ente_adscrito_id) {
                            $("#t-beneficiario").append(
                                "<option value=" +
                                item.tipo_beneficiario_id+
                                " selected>" +
                                item.tipo_beneficiario_nombre +
                                "</option>"
                            );
                        } else {
                            $("#t-beneficiario").append(
                                "<option value=" +
                                item.tipo_beneficiario_id+
                                ">" +
                                item.tipo_beneficiario_nombre +
                                "</option>"
                            );
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            
        },
    });
}



 //FUNCION PARA LLENAR EL COMBO ENTES ADSCRITOS
 function llenar_Entes_asdcritos(e, ente_adscrito_id) {
     e.preventDefault;
     url = "/Listar_Entes_asdcritos";
     $.ajax({
         url: url,
         method: "GET",
         dataType: "JSON",
         beforeSend: function(data) {},
         success: function(data) {
             if (data.length >= 1) {
              $("#ente-adscrito").empty();
                 $("#ente-adscrito").append(
                     "<option value=0  selected disabled>Seleccione</option>"
                 );
                 if (ente_adscrito_id === undefined) {
                     $.each(data, function(i, item) {
                         //console.log(data)
                         $("#ente-adscrito").append(
                             "<option value=" +
                             item.ente_id +
                             ">" +
                             item.ente_nombre +
                             "</option>"
                         );
                     });
                 } else {
                     $.each(data, function(i, item) {
                         if (item.ente_id === ente_adscrito_id) {
                             $("#ente-adscrito").append(
                                 "<option value=" +
                                 item.ente_id +
                                 " selected>" +
                                 item.ente_nombre +
                                 "</option>"
                             );
                         } else {
                             $("#ente-adscrito").append(
                                 "<option value=" +
                                 item.ente_id +
                                 ">" +
                                 item.ente_nombre +
                                 "</option>"
                             );
                         }
                     });
                 }
             }
         },
         error: function(xhr, status, errorThrown) {
        
         },
     });
 }
 
 //FUNCION PARA LLENAR EL COMBO DE LAS REDES SOCIALES
 function llenar_Red_social(e, id) {
     e.preventDefault;
     url = "/listar_Red_Social_filtro";
     $.ajax({
         url: url,
         method: "GET",
         dataType: "JSON",
         beforeSend: function(data) {},
         success: function(data) {
             if (data.length >= 1) {
                 $("#red-social").empty();
                 $("#red-social").append(
                     "<option value=0  selected disabled>Seleccione</option>"
                 );
                 if (id === undefined) {
                     $.each(data, function(i, item) {
                         //console.log(data)
                         $("#red-social").append(
                             "<option value=" +
                             item.red_s_id +
                             ">" +
                             item.red_s_nom +
                             "</option>"
                         );
                     });
                 } else {
                     $.each(data, function(i, item) {
                         if (item.id === id) {
                             $("#red-social").append(
                                 "<option value=" +
                                 item.red_s_id +
                                 " selected>" +
                                 item.red_s_nom +
                                 "</option>"
                             );
                         } else {
                             $("#red-social").append(
                                 "<option value=" +
                                 item.red_s_id +
                                 ">" +
                                 item.red_s_nom +
                                 "</option>"
                             );
                         }
                     });
                 }
             }
         },
         error: function(xhr, status, errorThrown) {
             
         },
     });
 }
 
 //FUNCION PARA LLENAR EL COMBO ESTADOS
 function llenar_Estados(e, id) {
     e.preventDefault;
     url = "/llenar_Estados";
     $.ajax({
         url: url,
         method: "GET",
         dataType: "JSON",
         beforeSend: function(data) {},
         success: function(data) {
             if (data.length >= 1) {
                 $("#estado-caso").empty();
                 $("#estado-caso").append(
                     "<option value=0  selected disabled>Seleccione</option>"
                 );
                 if (id === undefined) {
                     $.each(data, function(i, item) {
                         //console.log(data)
                         $("#estado-caso").append(
                             "<option value=" +
                             item.estadoid +
                             ">" +
                             item.estadonom +
                             "</option>"
                         );
                     });
                 } else {
                     $.each(data, function(i, item) {
                         if (item.id === id) {
                             $("#estado-caso").append(
                                 "<option value=" +
                                 item.estadoid +
                                 " selected>" +
                                 item.estadonom +
                                 "</option>"
                             );
                         } else {
                             $("#estado-caso").append(
                                 "<option value=" +
                                 item.estadoid +
                                 ">" +
                                 item.estadonom +
                                 "</option>"
                             );
                         }
                     });
                 }
             }
         },
         error: function(xhr, status, errorThrown) {
          
         },
     });
 }
 
 //FUNCION PARA LLENAR EL COMBO TIPO DE ATENCION USUARIO
 function llenar_Tipo_Atencion(e, id) {
     e.preventDefault;
     url = "/Listar_Tipo_Atencion_filtro";
     $.ajax({
         url: url,
         method: "GET",
         dataType: "JSON",
         beforeSend: function(data) {},
         success: function(data) {
             if (data.length >= 1) {
                 $("#tipo-atencion-usu").empty();
                 $("#tipo-atencion-usu").append(
                     "<option value=0  selected disabled>Seleccione</option>"
                 );
                 if (id === undefined) {
                     $.each(data, function(i, item) {
                         //console.log(data)
                         $("#tipo-atencion-usu").append(
                             "<option value=" +
                             item.tipo_aten_id +
                             ">" +
                             item.tipo_aten_nombre +
                             "</option>"
                         );
                     });
                 } else {
                     $.each(data, function(i, item) {
                         if (item.id === id) {
                             $("#tipo-atencion-usu").append(
                                 "<option value=" +
                                 item.tipo_aten_id +
                                 " selected>" +
                                 item.tipo_aten_nombre +
                                 "</option>"
                             );
                         } else {
                             $("#tipo-atencion-usu").append(
                                 "<option value=" +
                                 item.tipo_aten_id +
                                 ">" +
                                 item.tipo_aten_nombre +
                                 "</option>"
                             );
                         }
                     });
                 }
             }
         },
         error: function(xhr, status, errorThrown) {
             
         },
     });
 }
 //Evento que busca los municipios por estados
 $(document).on("click", "#estado-caso", (e) => {
     e.preventDefault();
 
     let datos = {
         id_estado: $("#estado-caso").val(),
     };
     $.ajax({
             url: "/municipios",
             method: "POST",
             dataType: "JSON",
             data: {
                 data: btoa(JSON.stringify(datos)),
             },
         })
         .then((response) => {
             $("#municipio-caso").html(response.data);
 
             let mun = $("#municipio-caso").val();
 
             if (mun != 0) {
                 let datos = {
                     id_municipio: $("#municipio-caso").val(),
                 };
                 $.ajax({
                         url: "/parroquias",
                         method: "POST",
                         dataType: "JSON",
                         data: {
                             data: btoa(JSON.stringify(datos)),
                         },
                     })
                     .then((response) => {
                         $("#parroquia-caso").html(response.data);
                     })
                     .catch((request) => {
                         Swal.fire("Error", response.JSONmessage, "Error");
                     });
             }
         })
         .catch((request) => {
             Swal.fire("Error", response.JSONmessage, "Error");
         });
 });
 //Evento que busca las parroquias por municipio
 $(document).on("click", "#municipio-caso", (e) => {
     e.preventDefault();
     let datos = {
         id_municipio: $("#municipio-caso").val(),
     };
     $.ajax({
             url: "/parroquias",
             method: "POST",
             dataType: "JSON",
             data: {
                 data: btoa(JSON.stringify(datos)),
             },
         })
         .then((response) => {
             $("#parroquia-caso").html(response.data);
         })
         .catch((request) => {
             Swal.fire("Error", response.JSONmessage, "Error");
         });
 });
 
 $("#red-social").on('change', function() {
     $("#red-social").removeClass('is-invalid');
 });
 
 $("#estado-caso").on('change', function() {
     $("#estado-caso").removeClass('is-invalid');
 });
 $("#tipo-pi").on('change', function() {
     $("#tipo-pi").removeClass('is-invalid');
 
 });

 $("#tipo-atencion-usu").on('change', function(e)
 {
   
     document.getElementById("detalles_atencion").disabled=false;
      $("#hijos_tipoatencion").val('NO');
     id_tipo_atencion=$('#tipo-atencion-usu option:selected').val(); 
    if (id_tipo_atencion == 5) 
    {

        $("#cgr").hide();
        $("#denuncias").show();
    }else
    {
        llenar_detalle_atencion(e,id_tipo_atencion)
        $("#cgr").hide();
        $("#denuncias").hide();
    }
    
 }); 


 function  llenar_detalle_atencion(e,id_tipo_atencion)
 {

     e.preventDefault;
     
       url='/Listar_Detalle_Atencion_filtro';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
           
 if(data.length>=1)
 {
      $('#detalles_atencion').empty();
      $('#detalles_atencion').append('<option value=0  selected disabled>Seleccione</option>');   
      if(id_tipo_atencion===undefined)
     {
       
       
          $.each(data, function(i, item)
          {
            $(".detelle_atencion").hide();
               //console.log(data)
               $('#detalles_atencion').append('<option value='+item.tipo_atend_id+'>'+item.tipo_atend_nombre+'</option>');

          });
     }
     else
     {
        $(".detelle_atencion").hide();
        data=data.filter(dato=>dato.tipo_aten_id==id_tipo_atencion);
        //console.log(buscar);
           $.each(data, function(i, item)
           {
           
    
            $(".detelle_atencion").show();
            $("#hijos_tipoatencion").val('SI');
            $('#detalles_atencion').append('<option value='+item.tipo_atend_id+'>'+item.tipo_atend_nombre+'</option>');    
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $("#requerimiento-usuario").on('change', function() {
     $("#requerimiento-usuario").removeClass('is-invalid');
 });



 //Evento de envio del formulario
 $(document).on("click", "#guardar", function(e) {
     e.preventDefault();
     //let tipo_prop_intelec = $("#tipo-pi").val();
     let tipo_atencion = $("#tipo-atencion-usu").val();
     let tipo_atend_id = $("#detalles_atencion").val();
     let requerimiento_user = $("#requerimiento-usuario").val();
     let red_social = $("#red-social").val();
     let estado = $("#estado-caso").val();
     let sexo = $("#sexo").val();
     requerimiento_user = requerimiento_user.trim();
     if (red_social == null) {
         $("#red-social").addClass('is-invalid');
 
         Swal.fire({
             icon: "success",
             type: 'error',
             html: '<strong>DEBE SELECCIONAR LA VIA DE ATENCION.</strong>',
 
             toast: true,
             position: "center",
             showConfirmButton: false,
             timer: 3500,
         });
     } else if (estado == null) {
         $("#red-social").removeClass('is-invalid');
         $("#estado-caso").addClass('is-invalid');
         Swal.fire({
             icon: "success",
             type: 'error',
             html: '<strong>EL CAMPO ESTADO ES OBLIGATORIO.</strong>',
             toast: true,
             position: "center",
             showConfirmButton: false,
             timer: 3500,
         });
     } 
     else if (tipo_atencion == null) {
         $("#tipo-pi").removeClass('is-invalid');
         $("#tipo-atencion-usu").addClass('is-invalid');
         Swal.fire({
             icon: "success",
             type: 'error',
             html: '<strong>EL USUARIO DEBE TENER ALGUN TIPO DE ATENCION</strong>',
             toast: true,
             position: "center",
             showConfirmButton: false,
             timer: 3500,
         })
     } else if (requerimiento_user == '') {
         $("#tipo-atencion-usu").removeClass('is-invalid');
         $("#requerimiento-usuario").addClass('is-invalid');
         $
         Swal.fire({
             icon: "success",
             type: 'error',
             html: '<strong>DEBE INDICAR LA DESCRIPCION DEL CASO .</strong>',
             toast: true,
             position: "center",
             showConfirmButton: false,
             timer: 3500,
         });
     } else {
         $("button[type=button]").attr('disabled', 'false');
         $("#red-social").removeClass('is-invalid');
         $("#estado-caso").removeClass('is-invalid');
        // $("#tipo-pi").removeClass('is-invalid');
         $("#tipo-atencion-usu").removeClass('is-invalid');
         $("#requerimiento-usuario").removeClass('is-invalid');
         //VERIFICO SI LA ATENCION ES ASESORIA PARA TOMAR EL VALOR DE LOS CAMPOS CORREPONDIENTES
         let tipo_atencion_usu = $("#tipo-atencion-usu").val();
         //VARIABLES PARA CGR
         let competencia_crg =2;
         let asume_crg= 2;
         let ente_adscrito
         let bandera_cgr = false;
         //VARIABLES PARA LA DEDUNCIA
         let option_personal
         let option_comunidad
         let option_terceros
         let bandera_denuncia = false;
         let fecha_hechos = $('#fecha-hechos').val();
         let denu_involucrados = $('#denu-involucrados').val();
         denu_involucrados = denu_involucrados.trim();
         let nombre_instancia = $('#nombre-instancia').val();
         let rif_instancia = $('#rif-instancia').val();
         let ente_financiador = $('#ente-financiador').val();
         let nombre_proyecto = $('#nombre-proyecto').val();
         let monto_aprovado = $('#monto-aprovado').val();
        if (tipo_atencion === '5')
        {
             if (document.getElementById('option-personal').checked) {
                 option_personal = true
             } else {
                 option_personal = false
             }
             if (document.getElementById('option-comunidad').checked) {
                 option_comunidad = true
             } else {
                 option_comunidad = false
             }
             if (document.getElementById('option-terceros').checked) {
                 option_terceros = true
             } else {
                 option_terceros = false
             }
 
             if (option_personal == false && option_comunidad == false && option_terceros == false) {
                 alert('Debe indicar a quien afecta el hecho');
             } else {
                 ente_adscrito = 0
                 if (fecha_hechos == '') {
                     alert('Debe selecciar la fecha en que ocurrieron los hechos');
 
                 } else if (denu_involucrados === '') {
                     $("#denu-involucrados").addClass('is-invalid');
                     alert('Este campo es requerido , por favor introduzca la informacion solicitada');
                 } else {
                     bandera_denuncia = true;
 
                     prop_intelectual = 1
                     let cedula= $("#cedula-persona").val()
                     if (cedula.charAt(0).match(/[a-zA-Z]/))
                     {
                         cedula = cedula.slice(1);
                     }
                     $("#denu-involucrados").removeClass('is-invalid');
                     let datos = {
                         "social_network": $("#red-social").val(),
                         "date-entry": $("#fecha-recibido").val(),
                         "person-name": $("#nombre-persona").val(),
                         "person-lastname": $("#apellido-persona").val(),
                         "person-id": cedula,
                         "nacionalidad": $("#tipo-persona").val(),
                         "telephone": $("#telefono").val(),
                         "country": $("#pais-caso").val(),
                         "state": $("#estado-caso").val(),
                         "county": $("#municipio-caso").val(),
                         "town": $("#parroquia-caso").val(),
                         "record-work": $("#num-tramite").val(),
                         "user-requirement": $("#requerimiento-usuario").val(),
                         "office": $("#office").val(),
                         "id_tipo_atencion": tipo_atencion,
                         "tipo_atend_id": tipo_atend_id,
                         "edad": $("#edad").val(),
                         "sexo": $("#sexo").val(),
                         "bandera_denuncia": bandera_denuncia,
                         "option_personal": option_personal,
                         "option_comunidad": option_comunidad,
                         "option_terceros": option_terceros,
                         "fecha_hechos": fecha_hechos,
                         "denu_involucrados": denu_involucrados,
                         "nombre_instancia": nombre_instancia,
                         "rif_instancia": rif_instancia,
                         "ente_financiador": ente_financiador,
                         "nombre_proyecto": nombre_proyecto,
                         "monto_aprovado": monto_aprovado,
                         "bandera_cgr": bandera_cgr,
                         "tipo_beneficiario": $("#t-beneficiario").val(),
                         "direccion": $("#office").val(),
                         "correo": $("#correo").val(),
                         "tipo_atend_id": tipo_atend_id,
                         "ente_adscrito": ente_adscrito,
                         "fecha_nacimiento": $("#fecha-nacimiento").val(),
                     }
                     $.ajax({
                         url: "/registrarCaso",
                         method: "POST",
                         dataType: "JSON",
                         data: {
                             "data": btoa(JSON.stringify(datos))
                         },
                         beforeSend: function() {
                             
                         },
                         success: function(respuesta) {
                             $("button[type=button]").attr('disabled', 'false');
                             if (respuesta.mensaje === 1) {
                                 Swal.fire({
                                     icon: "success",
                                     type: 'success',
                                     html: '<strong>Caso registrado exitosamente con el Nª' + ' ' + ' ' + respuesta.idcaso + '</strong>',
                                     toast: true,
                                     position: "center",
                                     showConfirmButton: false,
                                     //timer: 3500,
                                 });
                                 setTimeout(function() {
                                     window.location = "/casos";
                                 }, 1500);
                             } else if (respuesta.mensaje === 2) {
                                 Swal.fire({
                                     icon: "error",
                                     type: 'error',
                                     html: '<strong>Hubo un error en el registro del requerimiento del usuario .</strong>',
                                     toast: true,
                                     position: "center",
                                     showConfirmButton: false,
                                     //timer: 3000,
                                 });
                                 setTimeout(function() {
                                     window.location = "/casos";
                                 }, 1500);
                             }
                             else if (respuesta.mensaje === 7) {
                                Swal.fire({
                                    icon: "error",
                                    type: 'error',
                                    html: '<strong>Hubo un error en el registro del requerimiento del usuario .</strong>',
                                    toast: true,
                                    position: "center",
                                    showConfirmButton: false,
                                    //timer: 3000,
                                });
                                setTimeout(function() {
                                    window.location = "/casos";
                                }, 1500);
                            }
                         }
                         
                     });
                 }
 
             }
 
         } else {
             bandera_cgr = false;
             bandera_denuncia = false;
             valor_competencia = '';
             ente_adscrito = 0
             valor_asume = '';
             prop_intelectual = 1
             let cedula= $("#cedula-persona").val()
             if (cedula.charAt(0).match(/[a-zA-Z]/))
             {
                 cedula = cedula.slice(1);
             }
             let datos = {
                 "social_network": $("#red-social").val(),
                 "date-entry": $("#fecha-recibido").val(),
                 "person-name": $("#nombre-persona").val(),
                 "person-lastname": $("#apellido-persona").val(),
                 "person-id": cedula,
                 "nacionalidad": $("#tipo-persona").val(),
                 "telephone": $("#telefono").val(),
                 "country": $("#pais-caso").val(),
                 "state": $("#estado-caso").val(),
                 "county": $("#municipio-caso").val(),
                 "town": $("#parroquia-caso").val(),
                 "record-work": $("#num-tramite").val(),
                 //"pi-type":prop_intelectual,
                 "user-requirement": $("#requerimiento-usuario").val(),
                 "office": $("#office").val(),
                 "id_tipo_atencion": tipo_atencion,
                 "tipo_atend_id": tipo_atend_id,
                 "edad": $("#edad").val(),
                 "sexo": $("#sexo").val(),
                 "bandera_cgr": bandera_cgr,
                 "bandera_denuncia": bandera_denuncia,
                 "competencia_crg": competencia_crg,
                 "ente_adscrito": ente_adscrito,
                 "asume_crg": asume_crg,
                 "fecha_nacimiento": $("#fecha-nacimiento").val(),
                 "tipo_beneficiario": $("#t-beneficiario").val(),
                 "direccion": $("#office").val(),
                 "correo": $("#correo").val(),
             }
             $.ajax({
                 url: "/registrarCaso",
                 method: "POST",
                 dataType: "JSON",
                 data: {
                     "data": btoa(JSON.stringify(datos))
                 },
                 beforeSend: function() {
                     
                 },
                 success: function(respuesta) {
                     $("button[type=button]").attr('disabled', 'false');
                     if (respuesta.mensaje === 1) {
                         Swal.fire({
                             icon: "success",
                             type: 'success',
                             html: '<strong>Caso registrado exitosamente con el Nª' + ' ' + ' ' + respuesta.idcaso + '</strong>',
                             toast: true,
                             position: "center",
                             showConfirmButton: false,
                             //timer: 3500,
 
                         });
                         setTimeout(function() {
                             window.location = "/casos";
                         }, 1500);
                     } else if (respuesta.mensaje === 2) {
                         Swal.fire({
                             icon: "error",
                             type: 'error',
                             html: '<strong>Hubo un error en el registro del requerimiento del usuario .</strong>',
                             toast: true,
                             position: "center",
                             showConfirmButton: false,
                             //timer: 1500,
                         });
                         setTimeout(function() {
                             window.location = "/casos";
                         }, 1500);
                     }
                     else if (respuesta.mensaje === 7) {
                        Swal.fire({
                            icon: "error",
                            type: 'error',
                            html: '<strong>Hubo un error en el registro del requerimiento del usuario .</strong>',
                            toast: true,
                            position: "center",
                            showConfirmButton: false,
                            //timer: 3000,
                        });
                        setTimeout(function() {
                            window.location = "/casos";
                        }, 1500);
                    }
                 }
             });
         }
 
     }
 
 
 });

 $('#btn_buscar').on('click',function(e)
 {  
     e.preventDefault
     let cedula_normal = $("#cedula-existente").val().trim();
    let cedula_existente = $("#cedula-existente").val().trim();
    if (cedula_existente.charAt(0).match(/[a-zA-Z]/))
    {
        cedula_existente = cedula_existente.slice(1);
    }
      
    if (cedula_existente==''||cedula_existente==null) 
    {
        alert('Debe ingresar la cédula para los datos del Usuario');   
    }else
    {
        var url='/buscar_datos_usuarios';
        var data=
        {
            cedula_existente: cedula_existente,
        }
         $.ajax
         ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
             },
             success:function(data)
             {    

              
                if (data==0) 
                {
                   alert('La cedula no se encuentra registrada');
                   $("#cedula-persona").val(cedula_existente);
                }else
                {
                     // Accede al objeto data
                     const caso = data[0];
                     const nombre = caso.casonom ;
                     const apellido = caso.casoape ;
                     const cedula = cedula_normal;
                     const nacionalidad = caso.caso_nacionalidad;
                     const beneficiario = caso.tipo_beneficiario;
                     const genero = caso.sexo;
                     const telefono = caso.casotel;
                     const correo = caso.correo;
                     const estado = caso.estadoid;
                     const fecha_nacimiento = caso.fecha_nacimiento;
                     const edad = caso.edad;
                     const municipio = caso.municipioid;
                     const parroquia = caso.parroquiaid;
                     const tipo_atencion = caso.id_tipo_atencion;
                     // Asigna el nombre al valor del atributo value del input
                     const nombreInput = document.getElementById("nombre-persona");
                     nombreInput.value = nombre;
                     const edadInput = document.getElementById("edad");
                     edadInput.value = edad;
                     const fecha_nacimientoInput = document.getElementById("fecha-nacimiento");
                     fecha_nacimientoInput.value = fecha_nacimiento;
                     const apellidoInput = document.getElementById("apellido-persona");
                     apellidoInput.value = apellido;
                     const cedulaInput = document.getElementById("cedula-persona");
                     cedulaInput.value = cedula;
                     const telefonoInput = document.getElementById("telefono");
                     telefonoInput.value = telefono;
                     const correoInput = document.getElementById("correo");
                     correoInput.value = correo;
                //NACIONALIDAD
                const nacionalidadselect = document.getElementById("tipo-persona");
                const indicenacionalidad = Array.from(nacionalidadselect.options).findIndex(option => option.value === nacionalidad);
                nacionalidadselect.selectedIndex = indicenacionalidad;
                //BENEFICIARIO
                const beneficiarioselect = document.getElementById("t-beneficiario");
                const indicebeneficiario = Array.from(beneficiarioselect.options).findIndex(option => option.value === beneficiario);
                beneficiarioselect.selectedIndex = indicebeneficiario;
                 //SEXO
                 const sexoselect = document.getElementById("sexo");
                 const indicesexo = Array.from(sexoselect.options).findIndex(option => option.value === genero);
                 sexoselect.selectedIndex = indicesexo;
                 //ESTADO
                 const estadoselect = document.getElementById("estado-caso");
                 const indiceestado = Array.from(estadoselect.options).findIndex(option => option.value === estado);
                 estadoselect.selectedIndex = indiceestado;
                 //MUNICIPIO
                 let datos = {
                    id_estado: $("#estado-caso").val(),
                };
                $.ajax({
                        url: "/municipios",
                        method: "POST",
                        dataType: "JSON",
                        data: {
                            data: btoa(JSON.stringify(datos)),
                        },
                    })
                    .then((response) => {
                        $("#municipio-caso").html(response.data);
            
                        let mun = $("#municipio-caso").val();
            
                        if (mun != 0) {
                            let datos = {
                                id_municipio: $("#municipio-caso").val(),
                            };
                            $.ajax({
                                    url: "/parroquias",
                                    method: "POST",
                                    dataType: "JSON",
                                    data: {
                                        data: btoa(JSON.stringify(datos)),
                                    },
                                })
                                .then((response) => {
                                    $("#parroquia-caso").html(response.data);
                                })
                                .catch((request) => {
                                    Swal.fire("Error", response.JSONmessage, "Error");
                                });
                        }
                    })
                    .catch((request) => {
                        Swal.fire("Error", response.JSONmessage, "Error");
                    });
                // //PARROQUIA
                let datos2 = {
                    id_municipio: $("#municipio-caso").val(),
                };
                $.ajax({
                        url: "/parroquias",
                        method: "POST",
                        dataType: "JSON",
                        data: {
                            data: btoa(JSON.stringify(datos2)),
                        },
                    })
                    .then((response) => {
                        $("#parroquia-caso").html(response.data);
                    })
                    .catch((request) => {
                        Swal.fire("Error", response.JSONmessage, "Error");
                    });



               
 



                }
               
             },

            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
            }
        });
    }
         
}); 